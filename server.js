console.log("If module not found, install express globally `npm i express -g`!");
var port    = process.env.OPENSHIFT_NODEJS_PORT || process.env.VCAP_APP_PORT || process.env.PORT || process.argv[2] || 8585;
var express = require('express');
var Gun     = require('gun');
const http = require('http');

var app    = express();

// app.use(Gun.serve);
// function log(...args) {
//     console.log(...args.map(a=>JSON.stringify(a,null, 2)));
// }

//example of middleware user
let middleware = (req,res, next)=> {
    console.log("Request Intercepted", req);
    // log(req);
    // console.log(JSON.stringify(req,null,2));
    next()
}
app.use(middleware);

app.get('/gitAuth', (req,res)=>{

    let code = req.params.code || req.body.code;
    console.log("Getting Auth Token For COde:", code);
    //todo token revoked dont even try  ;)
    http.request('https://github.com/login/oauth/access_token?' +
        'client_id=b4b0434bbd82a9532c63&' +
        'client_secret=7da379367103ea574cb3d667cb17a3ad39c6f416&' +
        'code='+code, {}, tokenRes=>{

        token = tokenRes.body;

    })
    // let token = 'whatever';
    // console.log("Getting USer Info with token", token);
    // //todo

    res.status(200);
    res.json({token:token, user:user, myData:'whatever'});

})

app.use(express.static(__dirname));

var server = app.listen(port);

var gun = Gun({	file: 'data', web: server, peers: [] });

global.Gun = Gun; /// make global to `node --inspect` - debug only
global.gun = gun; /// make global to `node --inspect` - debug only


gun.get('bullchat3').on((data, key)=> {
    console.log("on bullchat: ", data.val);
})

console.log('Server started on port ' + port + ' with /gun');
