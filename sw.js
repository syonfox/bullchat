// importScripts('assets/vendor/workbox-6.1.5/workbox-sw.js');
importScripts('https://storage.googleapis.com/workbox-cdn/releases/6.1.5/workbox-sw.js');

//#################
// VERSIONING TEST
//#################
const version = '1.1.3';

//no ios support
// const channel4Broadcast = new BroadcastChannel('channel4');
var debug = true;
// if(self.registration.scope.includes('tdl.')) debug = false
workbox.setConfig({//todo make console logs not do anything in prod
    debug: debug,
    // modulePathPrefix: 'assets/vendor/workbox-6.1.5/'
});



addEventListener('message', (event) => {
    if (event.data.type === 'GET_VERSION') {
        event.ports[0].postMessage(version);
    }
});


const staleWhileRevalidate = new workbox.strategies.StaleWhileRevalidate();
const NetworkFirst = workbox.strategies.NetworkFirst;
const networkFirst = new NetworkFirst();


const warmCache = workbox.recipes.warmStrategyCache;
const Queue = workbox.backgroundSync.Queue;


// if(!Notification) {
//     console.warn("No Notification Object in service worker are you running on ios?")
// }

//https://developers.google.com/web/tools/workbox/reference-docs/latest/module-workbox-background-sync.Queue#constructor
const queue = new Queue('syncQ', {
    onSync: (q) => {
        console.log("Q on sync: ", q.queue)
        //started sync process (trigerd by the browser when online  but may be deleayed)
        //todo more advanced valadation of replayed requests
        // if(Notification) {
        //     if (Notification.permission === 'granted') {
        //         self.registration.showNotification("Background Sync Started...");
        //
        //     } else {
        //         console.warn("Notifications denied; Sync Started")
        //     }
        // }
        // try {
        //     self.registration.showNotification("Background Sync Started...");
        // } catch (e) {
        //     console.warn("Cant Notify, ", e)
        // }
        let res = q.queue.replayRequests();
        return res.then(r => {
            console.log('replayed response: ', r);
            //successfully synced all requests in queue
            //  if(Notification) {
            //      if (Notification.permission === 'granted') {
            //          self.registration.showNotification("Background Sync Finished");
            //
            //      } else {
            //          console.warn("Notifications denied; Sync Finished")
            //      }
            //  }
            return r;
        }).catch(e => {
            console.error('replay error: ', e);
            //sync failed to fully compleate
            // if(Notification) {
            //     self.registration.showNotification("Background Sync Interrupted (will retry later).");
            // }
            q.queue.registerSync();
            throw e; //docs say to throw an error
        })


        // console.log("replay res: ", res)
        // return res
    }
});

warmCache({
    strategy: staleWhileRevalidate,
    urls: [//these will get updated but respond quick
        'https://storage.googleapis.com/workbox-cdn/releases/6.1.5/workbox-sw.js',
    ]
});


//this is a network first page cache strategy
workbox.recipes.pageCache();
//this should automatically cache most forgotten stuff with staleWhileRevalidate strategy
//https://developers.google.com/web/tools/workbox/modules/workbox-recipes#static_resources_cache
workbox.recipes.staticResourceCache();

self.addEventListener('register', (event) => {
    console.log('[Service Worker] Registered');
})

self.addEventListener('install', (event) => {
    console.log('[Service Worker] Installed');
    console.warn("DELETING ALL CACHE BURN IT ALL :)");
    caches.keys().then(cacheNames => {
        cacheNames.forEach(cacheName => {
            console.warn("Burn: ", cacheName);
            caches.delete(cacheName);
        });
    });
    // localStorage.setItem('version', "" + version);

})

self.addEventListener('activate', (event) => {
    console.log('[Service Worker] Activated');
    // channel4Broadcast.postMessage({version: version});//no iso support
    self.skipWaiting();// this make the service worker register immediately
})
// self.addEventListener('fetch', (event) => {
//     console.log('[Service Worker] fetch');
// })

self.addEventListener('sync', (event) => {
    console.log('[Service Worker] sync', event);
});



