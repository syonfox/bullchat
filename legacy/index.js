console.log("If module not found, install express globally `npm i express -g`!");
var port    = process.env.OPENSHIFT_NODEJS_PORT || process.env.VCAP_APP_PORT || process.env.PORT || process.argv[2] || 8585;
var express = require('express');
var Gun     = require('gun');

var app    = express();

// app.use(Gun.serve);
// function log(...args) {
//     console.log(...args.map(a=>JSON.stringify(a,null, 2)));
// }

let middleware = (req,res)=> {
    console.log("Request Intercepted");
    // log(req);
    console.log(JSON.stringify(req,null,2));
}
// app.use(middleware);

app.use(express.static(__dirname));

var server = app.listen(port);

var gun = Gun({	file: 'data', web: server, peers: [] });

global.Gun = Gun; /// make global to `node --inspect` - debug only
global.gun = gun; /// make global to `node --inspect` - debug only


gun.get('bullchat3').on((data, key)=> {
    console.log("on bullchat: ", data.val);
})

console.log('Server started on port ' + port + ' with /gun');
