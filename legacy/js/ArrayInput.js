/**
 * A class dom util for when you need to edit an array easy to add component
 * that should work with most styling of inputs and buttons
 * you can override the st
 * .array-input button:hover {bor}
 * @author Kier Lindsay
 *
 */


/**
 * All the animation you need just write css keyframe animations and add the mto a class or do state animation with class transitions
 * and add them to css classes. essentially adds all animations css classes and then removes them after delay
 * if you dont end in a delay the last class wont be removed.
 * @param el
 * @param {String} animation1 - css class to add
 * @param {Number} [delay] - delay in ms
 * @param [animation2]
 * @param [...etc] - accepts any ordering of classes and delays
 *
 * @example
 * bs.animate(document.querySelector('button[name='up']'), 'up', 'active', 500, 'down', 200);
 */
bs.animate = function (el, animation1, delay, animation2, ...etc) {
    function delay(ms) {
        return new Promise(resolve => setTimeout(resolve, ms));
    }

    return new Promise(async resolve => {


        let a = []
        if (Array.isArray(animation1)) a = animation1;
        for (let i = 1; i < arguments.length; i++) {//for each arg after the el
            if (Array.isArray(arguments[i])) a = [...a, ...arguments[i]]; //merge an array
            else if (typeof arguments[i] != "undefined") a.push(arguments[i]) // or append the val
        }

        // if (!Array.isArray(etc)) etc = [];
        // let a = [animation1, delay, animation2, ...etc];

        console.log("animating: ", a);
        let stage = []
        for (let i = 0; i < a.length; i++) {
            if (typeof a[i] == 'string') {
                //play this animation
                el.classList.add(a[i]);
                stage.push(a[i]);
            } else {//its a number
                await delay(a[i]);
                stage.forEach(a => {//remove all the old ones
                    el.classList.remove(a);
                })
            }
        }


        // el.classList.add(animation1);
        // setTimeout(() => {
        //     //then play exit animation class
        //     el.classList.remove(animation1);
        //     el.classList.add(animation2);
        //
        //     if (etc) {
        //
        //     }
        //
        //     setTimeout(() => {
        //         el.classList.remove(animation2);
        //         if ()
        //             }, delay)
        //
        // }, delay)


    })

}

// document.getElementById('array-input-text-hover').addEventListener('change', (e) => {
//     let val = e.target.value;
//     document.documentElement.style.setProperty('--array-input-text-hover', val);
//
// })
// document.getElementById('array-input-hover').addEventListener('change', (e) => {
//     let val = e.target.value;
//     document.documentElement.style.setProperty('--array-input-hover', val);
//
// })
// document.getElementById('array-input-bg').addEventListener('change', (e) => {
//     let val = e.target.value;
//     document.documentElement.style.setProperty('--array-input-bg', val);
//
// })
// document.getElementById('array-input-text').addEventListener('change', (e) => {
//     let val = e.target.value;
//     document.documentElement.style.setProperty('--array-input-text', val);
//
// })



class ArrayInput {

    //https://stackoverflow.com/questions/37801882/how-to-change-css-root-color-variables-in-javascript
//     static getTheme(var) {
//         document.getComputedStyle(element).getPropertyValue(var);
//
//     }
//     static setTheme(prop, val) {
//         document.documentElement.style.setProperty(prop, val);
// }


    /**
     * sets ArrayInput.testHeightVal for user later (force buttons square)
     * @returns {number}
     */
    static testInputHeight() {
        if (!ArrayInput.testHeightVal) {

            let r = ArrayInput.domCreateRow();
            document.body.appendChild(r);
            ArrayInput.testHeightVal = r.lastElementChild.clientHeight;//we want the button not input
            r.remove();
            // setTimeout(r.remove, 50);
        }

        return ArrayInput.testHeightVal;

    }

    /**
     * This is a utility function that should make and array of inputs
     * @param {Array<String>} vals - the vals to put in the
     */
    constructor(vals, options) {
        ArrayInput.testInputHeight();
        // this.rowCount = 0;
        options = options || {};
        let defaults = {}

        this.hackyAnimate = true;

        options.buttonWidth = options.buttonWidth || ArrayInput.testInputHeight();

        this.id = 'ArrayInput_' + ArrayInput.getId();
        let div = document.createElement('div');
        this.div = div;

        div.id = this.id;
        let html = ``;

        if (!Array.isArray(vals)) {
            vals = [];
            console.warn("ArrayInput: No Data");
        }


        // let data = ''
        // if(options.data) {
        //     if(!options.type) options.type = 'datalist'
        //     data = ` data=`
        // }

        vals.forEach(v => {
            let r = ArrayInput.domCreateRow();
            r.querySelector('input').value = v;
            div.appendChild(r);
        })
        let r = ArrayInput.domCreateRow();
        // r.querySelector('input').value =
        div.appendChild(r);

        if (options.appendTo) {
            options.appendTo.appendChild(div);
        }


        return this;
    }

    static getId() {
        if (!ArrayInput.__id) ArrayInput.__id = 0;
        ArrayInput.__id++
        return ArrayInput.__id;
    }

    /**
     *
     * @param {HTMLElement} r - the dom row looks like <div><input><button up><button down><button close></div>
     */
    static arrayInputRowDelete(r) {

        // r = document.getElementById(id);
        console.log('delete:', r)
        // let prev = r.previousElementSibling;

        let next = r.nextElementSibling;
        if (!next) return; //at the bottom dont do anything we cant delete the last input it should alwas be an empty one

        r.parentElement.dispatchEvent(new Event('array-change', r))
        // hacky animation system
        r.firstElementChild.style.background = 'rgba(255,0,0,0.42)';
        setTimeout(() => {
            r.firstElementChild.style.background = 'rgb(255,117,117)';
            setTimeout(() => {
                r.firstElementChild.style.background = 'rgb(255,190,190)';
                setTimeout(() => {
                    r.firstElementChild.style.background = 'white';
                    r.remove();
                    // }
                }, 50)
            }, 100)
        }, 150)

        // r.remove();

    }

    /**
     * swaps row values with prev dom sibling
     * @param  {HTMLElement} r
     */
    static arrayInputRowUp(r) {


        //r looks like <div><input><button><button><button></div>

        console.log('up:', r)

        let prev = r.previousElementSibling;
        // let next = r.nextElementSibling;
        if (!prev) return; //at the top dont do anthing

        //swap prev with row
        let tmp = prev.firstElementChild.value;
        prev.firstElementChild.value = r.firstElementChild.value;
        r.firstElementChild.value = tmp;

        r.parentElement.dispatchEvent(new Event('array-change', r))

        if (this.hackyAnimate) {
            prev.firstElementChild.style.background = 'rgba(0,196,255,0.42)';
            setTimeout(() => {
                prev.firstElementChild.style.background = 'rgb(102,197,255)';
                setTimeout(() => {
                    prev.firstElementChild.style.background = 'rgb(179,235,253)';
                    setTimeout(() => {
                        prev.firstElementChild.style.background = 'unset';
                        // prev.firstElementChild.
                    }, 50)
                }, 70)
            }, 100)
        } else {

            bs.animate(r.firstElementChild, 'off-up',);
            // bs.animate(r.firstElementChild, 'out-down', 200, 'white');

            r.addEventListener('mouseleave', () => {
                r.firstElementChild.classList.remove('off-up')
            }, {once: true})

            bs.animate(prev.firstElementChild, 'black-down', 500);


        }

    }

    /**
     * swaps row values with next  dom sibling
     * @param {HTMLElement} r
     */
    static arrayInputRowDown(r) {
        console.log('down:', r)

        //r looks like <div><input><button><button><button></div>
        // let prev = r.previousElementSibling;
        let next = r.nextElementSibling;
        if (!next) return; //at the bottom dont do anthing

        //swap row with next
        let tmp = next.firstElementChild.value;
        next.firstElementChild.value = r.firstElementChild.value;
        r.firstElementChild.value = tmp;


        r.parentElement.dispatchEvent(new Event('array-change', r))

        //animate

        if (this.hackyAnimate) {
            next.firstElementChild.style.background = 'rgba(0,196,255,0.42)';
            setTimeout(() => {
                next.firstElementChild.style.background = 'rgb(102,197,255)';
                setTimeout(() => {
                    next.firstElementChild.style.background = 'rgb(179,235,253)';
                    setTimeout(() => {
                        next.firstElementChild.style.background = 'var(--background-color)';
                    }, 50)
                }, 70)
            }, 100)
        } else {

            // bs.animate(r.firstElementChild, 'out-down', 200, 'white');
            // r.addEventListener('mouseleave',()=>{r.firstElementChild.classList.remove('white')},{once:true})
            // bs.animate(r.firstElementChild, 'off-down', 500);
            bs.animate(next.firstElementChild, 'black-up', 500);
porn

        }


    }

    /**
     * used to detect if we need to add a new row for the user
     * @param r
     */
    static arrayInputRowChange(r) {
        console.log('change:', r)

        r.parentElement.dispatchEvent(new Event('array-change', r))
        // let prev = r.previousElementSibling;


        let next = r.nextElementSibling;
        let last = r.parentElement.lastElementChild

        //if the last element is not empty let the user add a new one
        if (last.firstElementChild.value) {
            r.parentElement.appendChild(ArrayInput.domCreateRow());
        }

        // if (!next && r.firstElementChild.value) {
        //     //at the bottom so add a new row if there is a value aka somone started typing they can then add a new row bellow.
        //     r.parentElement.appendChild(domCreateRow());
        // }


    }

    /**
     * Create a row with up down delete buttons
     * @returns {HTMLDivElement}
     */
    static domCreateRow(options) {

        options = options || {};
        let type = options.type || 'text'
        let placeholder = options.placeholder || 'Add A Item'

        let div = document.createElement('div');
        div.style.display = 'flex';
        div.classList.add('array-input');

        div.id = 'ArrayInputRow_' + ArrayInput.getId();
        let width = ''
        if (ArrayInput.testHeightVal) {
            width = 'width: ' + ArrayInput.testHeightVal + 'px;'
        }
        //NOTE: notice how the oninput function is set to the id not quoted or anything like tha
        // i though this was a bug at first but onclick=globalFunction(idstring) passes an HTML
        div.innerHTML = `
                <input oninput="ArrayInput.arrayInputRowChange(${div.id})" type="${type}" placeholder="${placeholder}">
                <button name="up" onclick="ArrayInput.arrayInputRowUp(${div.id})" style="${width}">⇑</button>
                <button name="down" onclick="ArrayInput.arrayInputRowDown(${div.id})" style="${width}">⇓</button>
                <button name="delete" onclick="ArrayInput.arrayInputRowDelete(${div.id})" style="${width}">X</button>
            `

        // $(div).on('hover', 'button', div, (e)=>{
        //     div.firstElementChild.style.background = 'yellow';
        // })
        return div;
    }


    getRows() {
        let a = [];

        let row = this.div.firstElementChild;
        while (row) {
            a.push(row);
            row = row.nextElementSibling;
        }

        return a
    }

    getArray() {
        let a = [];

        let row = this.div.firstElementChild;
        while (row) {
            let val = row.firstElementChild.value
            if (val) a.push(val);// filter out empty vals might get 0 by accident
            row = row.nextElementSibling;
        }

        return a


    }


    setArray(array) {

        let row = this.div.firstElementChild;

        array.push(''); // so that we always get the ending empty
        array.forEach(v => {

            if (!row) {
                row = ArrayInput.domCreateRow();
                this.div.appendChild(row);
            }
            row.firstElementChild.value = v;
            row = row.nextElementSibling;
        })
        //we got through all the valuse
        while (row) {
            let tmp = row //= row.firstElementChild.value
            // if (val) a.push(val);// filter out empty vals might get 0 by accident
            row = row.nextElementSibling;
            tmp.remove();
        }

        return this;
    }

}

//switch between hacky but working animate and css classes
//if you dont want to add any css use the hacky animate for a possible demo you can edit the code to edit the animations
ArrayInput.hackyAnimate = true;
