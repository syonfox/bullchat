/**
 * @author Kier Lindsay
 * this is for utility function for gun.js
 * and may assume bs.js enviroment
 *
 * In general functions sho be minimal have not other dependencies if possible
 * should pretrain to the gun npm package
 *
 */


if (!bs) {
    bs = {
        bsNotInitialized: true,
    }
    console.error("BS:GUN: No bs.js loaded");
}


if (!gun) {
    bs.gunNotInitialized = true;
    console.warn("BS:GUN: No gun.js loaded use bs.initGun()");

}

bs.initGun = (options)=>{
    if(options instanceof Gun) {
        bs.gun = options.gun;
    } else {
        bs.gun = new Gun(options)
    }

    bs.gun.user()

}


bs.
