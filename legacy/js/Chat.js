/**
 * So you wanted a chat app one liner :)
 *
 * let chat = new Chat();
 */

// a = new ArrayInput()
/**
 * @requires Gun
 * @requires Bullet
 * @requires jsPa
 * @deprecated
 */
class BullChat {


    _update(data, key) {
        console.log("Gun Key: ", key)
        this.content.value = data;
    }

    /**
     *
     * @param gun - theoreticaly could be any chain element even from gun.get('myBullChatNamspace')
     * @param options
     */
    constructor(gun, options) {


        this.gun = gun;

        // this. grid = new Grid({
        //
        // })

        let defaults = {
            room: 'bullchat'

        }


        this.data = gun.get(options.id).get('scratch');

        this.data.on(this._update);

        this.id = options.id || "chatid_" + bs.id++;

        let content = $(`
            <div>
            <textarea id='${this.id}'  rows="5" >
            </textarea>
            </div>
        `)


        let jspanelOptions = {
            headerTitle: 'Chat Room',
            theme: '#b9e8cc',
            borderRadius: '0.5rem',

            panelSize: {
                width: options.width,
                height: options.height,
            },
            // content: this.gridElement,
            content: content,
            // callback: function (panel) {
            //     this.content.style.padding = '10px';
            //
            //     console.log("callback");
            // },
            // onclosed: () => {
            //     console.log("Closed Help");
            //     help.panel = undefined;
            // },
            aspectRatio: "panel",

            data: {},
            dragit: {
                containment: 0,
                snap: {
                    sensitivity: 70,
                    trigger: 'panel',
                    active: 'both',
                },
            },
            onclosed: () => {
                this.panel = undefined;
            }

        }


        jsPanel.create(jspanelOptions)


    }
}


var gun = new Gun({
    // peers: ['http://bullchat.syon.ca:8585/gun'],
    peers: ['https://bullchat.syon.ca/gun'],

});
// let bullet = new Bullet(gun);

gun = gun.get('bullchat');

ArrayInput.hackyAnimate = false;

// let bullchat = new SiteChat({
//     id: 'bullchat1'
// })

// funtion buildBullchatDom() {
//
//
//
// }


let roomInput = document.getElementById('scratch_room');
roomInput.value = localStorage.getItem('scratch_room') || 'bullchat3'

let scratchInput = document.getElementById('scratch_body');
let nameInput = document.getElementById('name_scratch');
nameInput.value = localStorage.getItem('editor') || 'anon';

let putBtn = document.getElementById('put_scratch');
let editorSpan = document.getElementById('scratch_editor');

let contentDiv = document.getElementById('scratch_content');

// $('#scratch_room');
let id = roomInput.value

let a = new ArrayInput(['cat', 'dog', 'horse'], {
    type: 'text',
    // data: undefined,
    appendTo: contentDiv,
});

a.div.addEventListener('array-change', (e, r) => {
    console.log("Array Change", e, r);
    localChange()
})


// content.appendChild(a);

// gun.get(id).once(d=>{
//     scratch.
// })


function localChange() {
    console.log("Writing somthing", scratchInput.value);
    let d = {
        name: nameInput.value,
        val: scratchInput.value,
        array: JSON.stringify(a.getArray()),
    }

    gun.get(id).put(d);
}

function newlineHelper(input) {
    var newline = String.fromCharCode(13, 10);
    return input.replaceAll(/\\+n/g, newline);
}

async function remoteChange(data, key, _msg, _ev) {
    console.log("Remote Change: ", key, data, _msg, _ev);
    scratchInput.value = newlineHelper(data.val)
    editorSpan.innerText = data.name;

    a.setArray(JSON.parse(data.array));
}

let quote = document.getElementById('qotd');

function updateQOTD() {
    fetch('https://api.quotable.io/random')
        .then(res => {
            return res.json()
        })
        .then(data => {
            quote.innerText = `"${data.content}" ~ ${data.author}`

        })
}

updateQOTD()


roomInput.addEventListener('input', e => {
    gun.get(id).off(remoteChange);//stop listening to the old room
    id = roomInput.value //switch id to the new one
    localStorage.setItem('scratch_room', id);

    gun.get(id)
    gun.get(id).once(data => {
        if (!data || !data.val) {
            let val = "Welcome to a new scratch pad you can edit in real-time with others. =(^o^)=\n "
            val += quote.innerText
            scratchInput.value = val;
            editorSpan.innerText = "Unset Room"
            updateQOTD();
        } else {
            console.log("room had data: ", data);
        }
    }).on(remoteChange); //start listen to the new one

})

//set up the inital listener
gun.get(id).on(remoteChange);

nameInput.addEventListener('input', (e) => {
    console.log("Editor Changed: ", nameInput.value);
    localStorage.setItem('editor', nameInput.value);
    // localChange(e);
});

scratchInput.addEventListener('input', localChange);
putBtn.addEventListener('click', localChange)


let jspanelOptions = {
    headerTitle: 'Scratch Room',
    theme: '#fdd600',
    borderRadius: '0.5rem',

    panelSize: {
        width: 500,
        height: 650,
    },
    // content: this.gridElement,
    content: contentDiv,
    // callback: function (panel) {
    //     this.content.style.padding = '10px';
    //
    //     console.log("callback");
    // },
    // onclosed: () => {
    //     console.log("Closed Help");
    //     help.panel = undefined;
    // },
    aspectRatio: "panel",

    data: {},
    dragit: {
        containment: 0,
        snap: {
            sensitivity: 70,
            trigger: 'panel',
            active: 'both',
        },
    },
    // onclosed: () => {
    //     this.panel = undefined;
    // }

}

if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
    // <div className="jsPanel-titlebar" style="font-size: 1.05rem; touch-action: none; cursor: move;">
    //     <div className="jsPanel-title" style="color: rgb(0, 0, 0);">Scratch Room</div>
    // </div>

    let header = document.createElement('div');
    header.style.background = 'var(--bullchat-theme)';
    header.style.padding = '5px';
    header.innerHTML = `<h2>Scratch Room</h2>`
    contentDiv.firstElementChild.insertAdjacentElement('beforeBegin', header)
} else {// init the jspanel if we are on a computer

    jsPanel.create(jspanelOptions)
}


// If environment is not browser, export it (for node compatibility)
// if (typeof window === 'undefined') module.exports = Bullet
