
// <script type="module" src="assets/mjs/sw-client.mjs"></script>
//
import {Workbox} from 'https://storage.googleapis.com/workbox-cdn/releases/6.1.5/workbox-window.prod.mjs';

console.log("Loading the sw client module");


/**
 * A client side module to load and manage service workers
 * mostly uses workbox window (wb)
 * https://developers.google.com/web/tools/workbox/reference-docs/latest/module-workbox-window.Workbox
 */
class SwClient {
    constructor() {
        console.debug(`SWCLIENT: constructor`);
        if ('serviceWorker' in navigator) {
            //i dont know why but this needs a . using just /service-worker.js doesnt work. (but it should ant that's what the docs use)
            const wb = new Workbox('./sw.js');
            this.wb = wb;
            wb.addEventListener('activated', (event) => {
                console.debug(`SWCLIENT: activated`);
                // `event.isUpdate` will be true if another version of the service
                // worker was controlling the page when this version was registered.
                if (!event.isUpdate) {
                    console.log('Service worker activated for the first time!');

                    // If your service worker is configured to precache assets, those
                    // assets should all be available now.
                }

                this.syncVersion();
                // // Send that list of URLs to your router in the service worker.
                // wb.messageSW({
                //     type: 'GET_VERSION',
                //     payload: {},
                // });
            });

            wb.addEventListener('waiting', (event) => {
                console.log(`SWCLIENT: waiting, A new service worker has installed, but it can't activate` +
                    `until all tabs running the current version have fully unloaded.`);
            });

            wb.addEventListener('controlling', (event) => {
                console.log(`SWCLIENT: controllingA new service worker has started controlling the page`);
            });


            wb.addEventListener('message', (event) => {
                console.debug(`SWCLIENT: message`, event);

                if (event.data.type === 'CACHE_UPDATED') {
                    const {updatedURL} = event.data.payload;

                    console.log(`A newer version of ${updatedURL} is available!`);
                }
            });

            wb.register().then(o => {
                console.log('SWCLIENT: Successfully Registers Service Worker',o)
            }).catch(e => {
                console.error("SWCLIENT: Failed To Register Service Worker, Error: ", e);
                bs.resNotify({success: false, msg: 'Failed To Register Service Worker, You can still use the app like a normal website.'})

            });

            this.syncVersion();

        } else {
            console.error("SWCLIENT: SERVICE WORKER NOT SUPPORTED IN BROWSER");
            bs.resNotify({success: false, msg: 'Unsupported Browser, You can still use the app like a normal website. (No Service Worker)'})
        }


    }

    async syncVersion() {
        this.swVersion = await this.wb.messageSW({type: 'GET_VERSION'});

        // const channel4Broadcast = new BroadcastChannel('channel4');
        tdl._version = this.swVersion;
        localStorage.setItem('version', tdl._version);
        if (tdl._attribution) {
            map.attributionControl.removeAttribution(tdl._attribution);
        }
        tdl._attribution = 'v' + tdl._version
        map.attributionControl.addAttribution(tdl._attribution);

        console.log('Service Worker version:', this.swVersion);

        return this.swVersion;
    }

}

var swclient = new SwClient();
window.swclient = swclient;
// if ('serviceWorker' in navigator) {
//     //i dont know why but this needs a . using just /service-worker.js doesnt work. (but it should ant thats what the docs use)
//     const wb = new Workbox('./service-worker.js');
//
//     wb.addEventListener('activated', (event) => {
//         // `event.isUpdate` will be true if another version of the service
//         // worker was controlling the page when this version was registered.
//         if (!event.isUpdate) {
//             console.log('Service worker activated for the first time!');
//
//             // If your service worker is configured to precache assets, those
//             // assets should all be available now.
//         }
//
//         // // Send that list of URLs to your router in the service worker.
//         // wb.messageSW({
//         //     type: 'GET_VERSION',
//         //     payload: {},
//         // });
//     });
//
//     wb.addEventListener('waiting', (event) => {
//         console.log(`A new service worker has installed, but it can't activate` +
//             `until all tabs running the current version have fully unloaded.`);
//     });
//
//     wb.addEventListener('controlling', (event) => {
//         console.log(`A new service worker has started controlling the page`);
//     });
//
//
//     wb.addEventListener('message', (event) => {
//         if (event.data.type === 'CACHE_UPDATED') {
//             const {updatedURL} = event.data.payload;
//
//             console.log(`A newer version of ${updatedURL} is available!`);
//         }
//     });
//
//     wb.register().then(o => {
//         console.log(o)
//     }).catch(e => {
//         console.error(e);
//     });
//
//     const swVersion = await wb.messageSW({type: 'GET_VERSION'});
//
//     // const channel4Broadcast = new BroadcastChannel('channel4');
//     tdl._version = swVersion;
//     localStorage.setItem('version', tdl._version);
//     if (tdl._attribution) {
//         map.attributionControl.removeAttribution(tdl._attribution);
//     }
//     tdl._attribution = 'v' + tdl._version
//     map.attributionControl.addAttribution(tdl._attribution);
//
//     console.log('Service Worker version:', swVersion);
//
// } else {
//     console.error("NO SERVICE WORKER IN BROWSER");
// }


