//this is how we can import all the other bullchat mode probably from a folder like modules :)
var promiseSimpleMod  = import('./mods/SimpleMod.js').then((module) => {
    console.log("Dynamic Import:", module.default);
    return module.default; // can do any intermediate loading steps
    // window.SimpleMod =SimpleMod;

});
promiseSimpleMod.then(SimpleMod=>console.log("Demo Mod Import",SimpleMod))
// if()
// var promiseTinyEditorMod  = import('./mods/TinyEditor.js').then((module) => {
//     console.log("Dynamic Import:", module.default);
//     window.tinyEditorMod = module.default;
//     return module.default; // can do any intermediate loading steps
//     // window.SimpleMod =SimpleMod;
//
// });

// var promiseGunAvatar  = import('https://cdn.skypack.dev/gun-avatar.js').then((module) => {
//     console.log("Dynamic Import:", module.default);
//     return module.default; // can do any intermediate loading steps
//     // window.SimpleMod =SimpleMod;
//
// });

//
// import { mountElement } from "https://cdn.skypack.dev/gun-avatar";
// mountElement();

// gun.ask

// import * as SimpleMod from './SimpleMod.js';

// console.log("SimpleMod: ", SimpleMod);

/** basic html sanitizer to avoid scripting attack taken from slick grid*/
function sanitizeHtmlString(dirtyHtml) {
    return typeof dirtyHtml === 'string' ? dirtyHtml.replace(/(\b)(on\S+)(\s*)=|javascript:([^>]*)[^>]*|(<\s*)(\/*)script([<>]*).*(<\s*)(\/*)script(>*)|(&lt;)(\/*)(script|script defer)(.*)(&gt;|&gt;">)/gi, '') : dirtyHtml;
}

async function demoInputs() {

    let user = await gunAuth.promiseAuth();

    var tinyEditor = tinyEditorFactory();
    //with gunauth.user as the gun node we have control of write
    // anyone can still read
    let input1 = document.createElement('input')
    input1.type = 'range';
    let input2 = document.createElement('input')
    input2.type = 'email';
    input2.id = 'input2'
    let input3 = document.createElement('input')
    input3.type = 'month';
    input3.id = 'input3'

    document.body.appendChild(input2)
    document.body.appendChild(input3)//this is so that we can find them with query selector
    // let myDiv = document.createElement('div');

    function build(string) {
        let tmp = document.createElement('div');
        tmp.innerHTML = string;
        return tmp
    }

    return new BullChat(gunAuth.user, {
        id: 'inputs',
        room: 'room1',
        initPanel: true,
        gunAuth: gunAuth,
        jspanelOptions: {
            headerTitle: 'BullChat Inputs (Write Protected)',
        },
        modules: [
            'color',
            'date',
            {type: 'datetime-local'},
            {name: 'datetime'},
            {name: 'range', el: input1},
            {name: 'email', el: '#input2'},//note must be in the document somewhere
            {name: 'month', selector: '#input3'},//note must be in the document somewhere
            {name: 'number', el: build(`Number: <input type="number" value="42", step="42" min="0" max="1024">`)},
            {name: 'password', el: $(`<input type="password" minlength="10">`)[0]},//jquery :)
            {name: 'search'},
            {name: 'tel'},
            {name: 'time'},
            {name: 'text'},
            {name: 'url'},
            {name: 'week'},
            'textarea',
            new ArrayInput() // a bullchat mod


        ]
    })


// case "color":
// case "date":
// case "datetime-local":
// case "datetime":
// case "email":
// case "month":
// case "number":
// case "password":
// case "range":
// case "search":
// case "tel":
// case "text":
// case "time":
// case "url":
// case "week":
}


async function demoDiary() {
    let user = await gunAuth.promiseAuth();
    return new BullChat(gunAuth.user, {
        id: 'inputs',
        room: 'room1',
        mode: 'diary',
        initPanel: true,
        gunAuth: gunAuth, //required if you want encryption diary password-blog, password-public
        jspanelOptions: {
            headerTitle: 'BullChat Inputs (Write Protected)',
        },
        modules: [
            'color',
            'range',
            'textarea',
            new ArrayInput(), // a bullchat mod
            'textarea',
        ]
    })

}

async function demoBlog() {
    let user = await gunAuth.promiseAuth();
    return new BullChat(gunAuth.user, {
        id: 'inputs',
        room: 'room1',
        mode: 'blog',
        initPanel: true,
        // gunAuth:gunAuth,
        jspanelOptions: {
            headerTitle: 'BullChat Inputs (Write Protected)',
        },
        modules: [
            'color',
            'range',
            'textarea',
            new ArrayInput(), // a bullchat mod
            'textarea',
        ]
    })

}

//bulletin
async function demoBulletin() {
    let user = await gunAuth.promiseAuth();
    return new BullChat(gunAuth.user, {
        id: 'inputs',
        room: 'room1',
        mode: 'public',
        initPanel: true,
        // gunAuth:gunAuth,
        jspanelOptions: {
            headerTitle: 'BullChat Inputs (Write Protected)',
        },
        modules: [
            'color',
            'range',
            'textarea',
            'textarea',
            new ArrayInput() // a bullchat mod
        ]
    })

}

async function demoPasswordBulletin() {
    let user = await gunAuth.promiseAuth();
    return new BullChat(gunAuth.user, {
        id: 'inputs',
        room: 'room1',
        mode: 'password-public',
        initPanel: true,
        // gunAuth:gunAuth,
        jspanelOptions: {
            headerTitle: 'BullChat Inputs (Write Protected)',
        },
        modules: [
            'color',
            'range',
            'textarea',
            new ArrayInput(), // a bullchat mod
            'textarea',
        ]
    })

}


// <script src="https://unpkg.com/tiny-editor/dist/bundle.js"></script>