


//Initialize gun
var gun = new Gun({
    // peers: ['http://bullchat.syon.ca:8585/gun'],
    peers: ['https://bullchat.syon.ca/gun'],
});


function uuidv4() {
    return ([1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, c =>
        (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
    );
}

let container = document.getElementById('fire-demo-content');

let title = document.createElement('h2');
title.innerText = "Hello World"
container.appendChild(title);


let ffs = [
    new FireSlotMod('2301', 'D'),
    new FireSlotMod('2304', 'C'),
    new FireSlotMod('2305', 'K'),
    new FireSlotMod('2307', 'R'),
    new FireSlotMod('2306', 'M'),

    new FireSlotMod('2315', 'M'),
    new FireSlotMod('2317', 'C'),

    new FireSlotMod('2321', 'P'),
    new FireSlotMod('2323', 'L'),
    new FireSlotMod('2326', 'M'),

]




let selectAction = document.createElement('select');



let turnoutOption = document.createElement('option');
turnoutOption.value="turnout-check";
turnoutOption.innerText="Check all Turnout Gear"
selectAction.appendChild(turnoutOption);

let helmetOption = document.createElement('option');
helmetOption.value="helmet-boot-acct-check";
helmetOption.innerText="Check Helmet and Decals"
selectAction.appendChild(helmetOption);

let bootOption = document.createElement('option');
bootOption.value="boot-check";
bootOption.innerText="Check Boots"
selectAction.appendChild(bootOption);

let jacketOption = document.createElement('option');
jacketOption.value="jacket-check";
jacketOption.innerText="Check Jacket"
selectAction.appendChild(jacketOption);

let actions = {
    "turnout-check": (ff, bc) => {
        console.log("Turnout Check: ",ff);


        ff.setVisibleSlots([1,2,3,4,5,6,7]);
        bc.initPanel();

        //initialize and show fire fighter turnout thing
    },
    "helmet-check": (ff) => {
        console.log("Helmet Check: ", ff);
        //initialize and show fire fighter turnout thing
    },
    "helmet-boot-acct-check": (ff,bc) => {
        console.log("Helmet, Boot Accountability Check: ", ff);
        ff.setVisibleSlots([1,3,4,5]);
        bc.initPanel();

        //initialize and show fire fighter turnout thing
    },
    "boot-check": (ff, bc) => {
        console.log("Boot Check: ", ff);

        ff.setVisibleSlots([3]);
        bc.initPanel();


    },
}

container.appendChild(selectAction);

window.mobileCheck = function() {
    let check = false;
    (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) check = true;})(navigator.userAgent||navigator.vendor||window.opera);
    return check;
};

let isMobile = mobileCheck();
ffs.forEach(ff=>{

    let bc = new BullChat(gun, {
            id: ff.getId(),
            // container: '.bullchat-rich',
            // initPanel:true,
            room: 'turnout',
            modules: [
                ff,
                {
                    type: 'textarea'
                },
                // kvm
            ],
            jspanelOptions: {
                callback: (panel)=>{
                    if(isMobile) panel.maximize()
                }
            }
        })

    let card = document.createElement('div');
    card.innerHTML = `FF: ${ff._data.id} ${ff._data.name}`
    card.name = ff.getId();

    card.addEventListener("click", (e)=>{
        console.log("Card Clicked: ", e);
        let action = selectAction.value;
        actions[action](ff, bc);
    })

    container.appendChild(card);
})





// let devin = new Resource({name: "Devin Lindsay"}); //actually more stuff and FireFighter
// let radio = new Resource({name: 'radion1'}) // actually more stuff and Equipment


function kvmFactory(key, val) {
    let kvm = {};

    kvm.el = document.createElement('div');
    kvm.el.classList.add('kvm-class');


    kvm.el.innerHTML = key + ' <input><button>Submit</button>';

    kvm.input = kvm.el.querySelector('input');
    kvm.button = kvm.el.querySelector('button');
    kvm.button.style.background = 'pink';
    kvm.getValue = function () {
        return kvm.input.value;
    }
    kvm.setValue = function (val) {
        kvm.input.value = val;
    }
    kvm.getDefault = function () {
        return val;
    }
    kvm.hookChange = function (fn) {
        kvm.input.addEventListener('input', fn);
        // kvm.button.addEventListener('click', fn);
    }

    return kvm;
}

let kvm = kvmFactory("foo", "bar");

//
// let devinBullchat = new BullChat(gun, {
//     id: ff.getId(),
//     // container: '.bullchat-rich',
//     initPanel:true,
//     room: '',
//     modules: [
//         ff,
//         {
//             type: 'textarea'
//         },
//         kvm
//     ]
// })
//
