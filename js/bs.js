/**
 * @file
 * This file has a lot of utility to it
 * its worth a gander
 */

/**
 * @class
 * @author Kier Lindsay
 * @desc This is a place where we can make helper functions for all the annoying bs in the web dev world :)
 * note you may also want <link href="../css/bs.css" rel="stylesheet">
 * things should work without it but this is where the css is
 * Also not som functions depend on other libraries I will try an mention dependencies in method descriptions
 * Som common ones are:
 * jquery
 * bootstrap
 * bootstrap.notify
 * leaflet
 * leaflet.draw
 *
 * This is a long file i recommend looking at it in the docs if you have access if not email dev@syon.ca for support plan
 * development
 * you can generally include this file by
 * <script src='https://portal.lorrnel.com/js/bs.js'></scripts>
 * <script src='https://portal3.lorrnel.com/js/bs.js'></scripts>
 * <script src='https://app.planmap.ca/js/bs.js'></scripts>
 * <script src='https://bullchat.syon.ca/js/bs.js'></scripts>
 * Happy coding :)
 */
let bs = {
    /**
     * @property id - an incrementing id for use when needed with dom stuff best practice would be to use a namespace for dom stuff
     */
    id: 0,


    /**
     * Get all css variables
     * @returns {Array<{value: string, key: string}>}
     */
    getCssVars: function () {
        // let ss = document.styleSheets;
        return Array.from(document.styleSheets)
            .filter((styleSheet) => {
                let isLocal = !styleSheet.href || styleSheet.href.startsWith(window.location.origin)
                if (!isLocal) console.warn("Skipping remote style sheet due to cors: ", styleSheet.href);
                return isLocal;
            })
            .map((styleSheet) => Array.from(styleSheet.cssRules))
            .flat()
            .filter((cssRule) => cssRule.selectorText === ':root')
            .map((cssRule) => cssRule.cssText.split('{')[1].split('}')[0].trim().split(';'))
            .flat()
            .filter((text) => text !== '')
            .map((text) => text.split(':'))
            .map((parts) => {
                return {key: parts[0].trim(), value: parts[1].trim()}
            })

    },

    /**
     * Get a css var
     * @param key
     * @returns {{value: string, key: string}}
     */
    getCssVar: (key) => {
        if (!bs._cssVars) bs._cssVars = bs.getCssVars();
        return bs._cssVars.find(v => v.key == key);
    },
    /**
     * set a css var value
     * @example
     * :root {
     *   --backgroun-color: #fff000;
     * }
     * @param key - the var name ie --backgroun-color
     * @param value - the value ie #fff000
     */
    setCssVar: (key, value) => {
        document.documentElement.style.setProperty(key, value);

    },
    /**
     * Opens a jspanel with a css variable editor in  it
     */
    getCssEditor: () => {
        let cssContainer = document.createElement('div')
        let cssVars = bs.getCssVars();

        cssVars.forEach(v => {
            let label = document.createElement('h4');
            label.innerText = `
            Var: ${v.key}, Default: ${v.value} |=| Set to: -`;
            label.style.display = 'flex';
            label.style.flexDirection = 'row';
            label.style.alignItems = 'flex-end';
            label.style.flexWrap = 'wrap';
            let input = document.createElement('input');

            input.type = 'color';
            input.addEventListener('input', (e) => {
                let val = e.target.value;
                document.documentElement.style.setProperty(v.key, val);
            })
            label.appendChild(input);


            let box = document.createElement('input')
            box.type = 'checkbox';
            box.checked = true;

            box.addEventListener('input', (e) => {
                console.log('clicked', e);
                // let val = e.target.checked;
                if (box.checked) {
                    input.type = 'color';
                } else {
                    input.type = 'text';
                }
                // document.documentElement.style.setProperty(v.key, val);
            })
            label.appendChild(box);

            let span = document.createElement('span');
            span.innerText = 'is Color?';
            label.appendChild(span);

            cssContainer.appendChild(label);


            //     <label>Array Input Background Color:</label>
            // <input type="color" id="array-input-bg" >

        })

        return jsPanel.create({
            headerTitle: "Css Editor",
            panelSize: bs.getGoodPanelSize(undefined, 800),
            content: cssContainer
        })
    },

    /**
     * Check if a web component tag is registered
     * @example bs.isRegistered('my-component-tag')
     * @param name - the name of the webcomponents tag
     * @returns {boolean}
     */
    isRegistered: (name) => {
        return document.createElement(name).constructor !== HTMLElement;
    },

    //get a qr to this host with param id
    id2qr: (id, param = 'id', s) => {
        return bs.str2qr(window.location.host + '?' + param + '=' + encodeURIComponent(id), s)
    },

    //make a string into a qr scaling up size until it works
    str2qr: (str, size = 10, setCorrection = ['H', 'Q', /*'M', 'L'*/]) => {
        // L : 1,
        //     M : 0,
        //     Q : 3,
        //     H : 2

        if (!Array.isArray(setCorrection)) setCorrection = [setCorrection];

        let correcti
        console.log(str, size)
        for (let s = size; s <= 40; s++) {
            for (l of setCorrection) {
                try {
                    let qr = qrcode(s, l);
                    qr.addData(str);
                    qr.make();
                    return qr;
                } catch (e) {
                    console.error('failed at  error correction: ', l, " size: ", s, ' e: ', e)
                }
            }
        }


        return qr.createImgTag();
        return bs.str2qr(window.location.host + '?' + param + '=' + encodeURIComponent(id))
    },

    /**
     * Requires qrcode.js see bullchat
     * @param id
     * @param title
     */
    showQr: (id, title = 'My QR') => {
        let qr = bs.id2qr(id)

        let w = bs.getGoodWidth();

        let img = qr.createImgTag()
        jsPanel.create({
            headerTitle: 'QR Code: ' + title,
            panelSize: {
                width: w,
                height: w + 20
            },
            content: qr.createSvgTag(undefined, 1, id, title),
            callback: panel => {
                let s = panel.querySelector(`svg[role='img']`)
                s.setAttribute('width', '99%')
                s.setAttribute('height', null)

            }
        })

    },

    showSvg: (id, title) => {
        let w = bs.getGoodWidth();

        let img = qr.createImgTag()
        jsPanel.create({
            headerTitle: 'QR Code: ' + title,
            panelSize: {
                width: w,
                height: w + 20
            },
            content: qr.createSvgTag(undefined, 1, id, title),
            callback: panel => {
                let s = panel.querySelector(`svg[role='img']`)
                s.setAttribute('width', '99%')
                s.setAttribute('height', null)

            }
        })
    },

    /**
     *
     * @param [aspect=8.5/11] = aspect ratio to target w/h ie 1920/1080 or 8.5/11
     * @param [thresh=500] -  max of 1/2 width or thresh used to find preferred width.
     * @param [pad=100] - how much to pad the edges by note 100 = 50 top 50 bottom
     * @param opts
     */
    getGoodPanelSize(aspect = 8.5 / 11, thresh, pad = 100, opts) {
        // opts = opts || {};
        // opts.aspect = opts.aspect || 8.5/11

        let w = document.body.clientWidth - pad;
        let h = document.body.clientHeight - pad;

        //the
        let t = thresh || Math.max(w / 2, 500)

        let gw = bs.getGoodWidth(t, pad)

        //if the good height is to tall user the panel height
        let ph = Math.min(h, gw * (1 / aspect));

        //then compute with based on height to maintain aspect
        let pw = Math.min(h, ph * aspect);

        return {width: pw, height: ph};


    },

    /**
     * Thresh or screen with whichever smaller
     * @param [thresh=500]
     * @returns {number}
     */
    getGoodWidth: (thresh = 500, pad = 50) => {

        let w = document.body.clientWidth - pad;

        // let thresh = 500
        if (w > thresh) w = thresh;

        return w;
        // let h = document.body.clientHeight;

    },

    /**
     * Get a container or make it if necessary
     * @param el
     * @returns {Element}
     */
    getContainer: (el) => {
        if (el instanceof HTMLElement) {
            return el;
        }
        let domEl;
        if (typeof el == 'string') {
            domEl = document.querySelector(el);
            if (!domEl) domEl = document.getElementById(el);
        }

        if (!domEl) {//finaly just create it
            domEl = document.createElement('div');
            if (typeof el == 'string') {
                //treat it like a query selector to add classes or set id
                if (el[0] == '#') {
                    domEl.id = el.splice(1, el.length - 1)
                }//todo do properly
            }
        }
        return domEl;
    },
    /**
     * Probability of collision: dont worry about it :)
     * @returns {*}
     */
    uuidv4: () => {
        return ([1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, c =>
            (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
        );
    },

    /**
     * Get the params from the window or optionally a provided url
     * @param [url]
     */
    getParams: (url) => {
        if (!url) url = window.location.href;
        a = new URL(url);
        return Object.fromEntries(a.searchParams.entries())
    },
    /**
     * Times the fn and logs
     */
    time: async (fn, ...args) => {
        let time = Date.now();
        if (typeof fn == "function") {

            await fn(...args)
        } else {
            //maybey its a promise
            await Promise.all([fn])
        }

        let delta = Date.now() - time;
        console.log("bs.time: fn took ", delta / 1000, 's');

    },

    /**
     * A simple yet powerful search function
     * @param str - the string you wish to search (if not string will json stringify)
     * @param val - a csv of values to search for or an array if you perfer
     * @param [mode='OR'] - the mode ie val='a, b' "OR" true if a or b, "AND" true if a and b, "OFF" true if str.includes('a, b')
     * @param [opts] - optional options
     * @param [opts.delimiter=','] - if you dont like using the ',' separator say what you want.
     * @returns {boolean|*}
     */
    search: (str, val, mode = 'OR', opts) => {
        if (typeof str != 'string') str = JSON.stringify(str);
        opts = opts || {}
        opts.delimiter = opts.delimiter || ','
        mode = mode.toUpperCase();
        if (mode == "OFF") {
            return str.includes(val);
        }

        if (!Array.isArray(val)) {
            val = val.split(opts.delimiter)
        }

        switch (mode) {
            case "OR":
                for (let v of val) {
                    v = v.trim()
                    if (str.includes(v)) return true;
                }
                return false;
            case "AND":
                for (let v of val) {
                    v = v.trim()
                    if (!str.includes(v)) return false;
                }
                return true;

            default:
                throw "Unknown mode" + mode;
        }
    },


    /** basic html sanitizer to avoid scripting attack taken from slick grid*/
    sanitizeHtmlString: (dirtyHtml) => {
        return typeof dirtyHtml === 'string' ? dirtyHtml.replace(/(\b)(on\S+)(\s*)=|javascript:([^>]*)[^>]*|(<\s*)(\/*)script([<>]*).*(<\s*)(\/*)script(>*)|(&lt;)(\/*)(script|script defer)(.*)(&gt;|&gt;">)/gi, '') : dirtyHtml;
    },

    /**
     * Simple object check.
     * @param item
     * @returns {boolean}
     */
    isObject: (item) => {
        return (item && typeof item === 'object' && !Array.isArray(item));
    },

    /**
     * Deep merge two objects.
     * @param target - the target
     * @param sources - things to merge in these will override taget if duplicity
     *
     * @example
     * mergeDeep(this, { a: { b: { c: 123 } } });
     * // or
     * const merged = mergeDeep({a: 1}, { b : { c: { d: { e: 12345}}}});
     * console.dir(merged); // { a: 1, b: { c: { d: [Object] } } }
     */
    mergeDeep: (target, ...sources) => {
        if (sources.length === 0) {
            return target;
        }
        const source = sources.shift();//pop the first one

        // console.log(source);
        if (bs.isObject(target) && bs.isObject(source)) {
            for (const [key, value] of Object.entries(source)) {

                if (bs.isObject(value)) {
                    if (!target[key]) Object.assign(target, {[key]: {}});
                    bs.mergeDeep(target[key], source[key]);
                } /*else if(typeof value == 'function') {
                    target[key] = value;
                }*/
                else {//

                    Object.assign(target, {[key]: source[key]});
                }
            }
        }
        return bs.mergeDeep(target, ...sources);
    },

    testMergeDeep: () => {
        let a = {
            a: 'a',
            b: {
                c: 'a',
                d: 'a',
                fn: false
            },

            f: true
        };
        let b = {
            b: {
                d: 'b', e: 'b', fn: () => {
                    console.log('fn works')
                }
            },
            f: false,
            g: 'b'
        }

        console.log('a: ', JSON.stringify(a))
        console.log('b: ', JSON.stringify(b))

        let m = bs.mergeDeep(a, b)
        console.log('m: ', JSON.stringify(m));
        m.b.fn();
    },
//
    /**
     * downloadString("a,b,c\n1,2,3", "text/csv", "myCSV.csv")
     * @param text - the text
     * @param fileType - mime type ie application/json text/csv .. etc
     * @param fileName - the file name
     */
    downloadString: (text, fileType, fileName) => {
        var blob = new Blob([text], {type: fileType});

        var a = document.createElement('a');
        a.download = fileName;
        a.href = URL.createObjectURL(blob);
        a.dataset.downloadurl = [fileType, a.download, a.href].join(':');
        a.style.display = "none";
        document.body.appendChild(a);
        a.click();
        document.body.removeChild(a);
        setTimeout(function () {
            URL.revokeObjectURL(a.href);
        }, 1500);
    },

    isClosed: (panel) => {
        if (!jsPanel) {
            console.error("JSPANEL LIBRARY NOT INCLUDED")
        }


    },

    getRandomColor: () => {
        var letters = '0123456789ABCDEF';
        var color = '#';
        for (var i = 0; i < 6; i++) {
            color += letters[Math.floor(Math.random() * 16)];
        }
        return color;
    },

    //https://gist.github.com/franciskim/41a959f8e3989254ef5d
    validateUrl: (value) => {
        var expression = /[-a-zA-Z0-9@:%_\+.~#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&//=]*)?/gi
        var regexp = new RegExp(expression);
        return regexp.test(value);
    },
    /**
     * Convers express route to a url this is handy for having dynamic subdomains
     * @param route - the route ie "/api/doSomthing"
     * @param params - see code un-used curently
     * @returns {string} - ie http://subdomain.domain.com/api/doSomthing
     */
    routeToURL: (route, params) => {
        return window.location.href.split(':')[0] + '://' + window.location.host + route
    },

    resNotify: res => {
        let color = 'danger';
        let icon = 'fa fa-times'
        if (res.success) {
            color = 'success';
            icon = 'fa fa-check'
        }

        if (res.color) {
            color = res.color;
        }

        if (res.debug) {
            console.log(res.debug);
            if (res.debug.code == "22007") {
                res.msg = "Invalid datetime format please use dd/mm/yy (Error: 22007)"
            }
            if (res.debug.code == "42501") {
                res.msg = "Insufficient privileges to execute query (Error: 42501)"
            }

            if (res.debug.code == "22P02") {
                if (res.debug.detail) res.msg = res.debug.detail + `(Error: 22P02)`;
                else res.msg = "Invalid Input Type (Error: 22P02)"
            }
            if (res.debug.code) {
                res.msg += " (Error: " + res.debug.code + ")";
            }

        }


        //beutify error messages for users
        let fileExistsRE = /(Key\s\(file_name\)=\()(.+)(\)\salready\sexists)/
        let match = res.msg.match(fileExistsRE);
        if (match) {
            res.msg = `A file with the name: ${match[2]} already exists. Please enter a unique file name.`
        }


        $.notify({
            "icon": icon,
            "message": res.msg
        }, {
            "type": color,
            "timer": 3000,
            "offset": 65,
            "placement": {
                "from": 'top',
                "align": 'right'
            },
            // template: '<div data-notify="container" class="col-11 col-md-4 alert alert-{0}" role="alert"><button type="button" aria-hidden="true" class="close" data-notify="dismiss"><i class="fa fa-remove"></i></button><span data-notify="icon"></span> <span data-notify="title">{1}</span> <span data-notify="message">{2}</span><div class="progress" data-notify="progressbar"><div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div></div><a href="{3}" target="{4}" data-notify="url"></a></div>'

        })
    },

    resStatus: (res, statusElement) => {
        statusElement.style.display = 'block';
        if (res.success) {
            statusElement.classList.toggle('alert-success', true);
            statusElement.classList.toggle('alert-danger', false);

        } else {
            statusElement.classList.toggle('alert-success', false);
            statusElement.classList.toggle('alert-danger', true);
        }
        statusElement.innerText = res.msg;
    },

    postJson: async (url, json) => {
        // const plainFormData = Object.fromEntries(formData.entries());
        // const formDataJsonString = JSON.stringify(plainFormData);

        const fetchOptions = {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                Accept: "application/json",
                credential: "same-origin"
            },
            body: JSON.stringify(json),
        };

        const response = await fetch(url, fetchOptions);

        if (!response.ok && response.status != 401) {
            const errorMessage = await response.text();
            throw new Error(errorMessage);
        }

        return response.json();
    },


    sendJson: async (method, url, json, nocores = false) => {
        // const plainFormData = Object.fromEntries(formData.entries());
        // const formDataJsonString = JSON.stringify(plainFormData);

        const fetchOptions = {
            method: method,
            headers: {
                "Content-Type": "application/json",
                Accept: "application/json",

                // 'Access-Control-Allow-Origin': '*',
                credential: "same-origin"
            },
            body: JSON.stringify(json),
        };
        if (nocores) {
            fetchOptions.headers.mode = 'no-cors'
            fetchOptions.headers.credential = 'omit'
        }

        const response = await fetch(url, fetchOptions);

        // if (!response.ok) {
        //     response.text().catch(e=>{console.error(e)});
        //     // throw new Error(errorMessage);
        // }

        return response.json().catch(e => {
            console.error(e)
        });
    },


    // /**
    //  * gest common sizing option for creating a panel
    //  * @param size -
    //  */
    // getPanelSize: (width, height) => {
    //
    //
    //   switch (size) {
    //       case ""
    //   }
    // },

    /***
     * Posts json and notifies if the response is standard {success:bool, msg:string}
     * @param url the url to post to
     * @param json the json for the body
     */
    postJsonAndNotify: (url, json) => {
        return bs.postJson(url, json).then(res => {
            bs.resNotify(res);
            return res;
        }).catch(e => {
            console.log(e);
            bs.resNotify({success: false, msg: "Network Error"})
        });
    },


    sendJsonAndNotify: (method, url, json, nocores) => {
        return bs.sendJson(method, url, json, nocores).then(res => {
            bs.resNotify(res);
            return res;
        }).catch(e => {
            console.log(e);
            bs.resNotify({success: false, msg: "Network Error"})
        });
    },

    /**
     * Helper function for POSTing data as JSON with fetch.
     *
     * @param {Object} options
     * @param {string} options.url - URL to POST data to
     * @param {FormData} options.formData - `FormData` instance
     * @return {Object} - Response body from URL that was POSTed to
     */
    postFormDataAsJson: async ({url, formData}) => {
        const plainFormData = Object.fromEntries(formData.entries());
        const formDataJsonString = JSON.stringify(plainFormData);

        const fetchOptions = {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                Accept: "application/json",
            },
            body: formDataJsonString,
        };

        const response = await fetch(url, fetchOptions);

        if (!response.ok) {
            const errorMessage = await response.text();
            throw new Error(errorMessage);
        }

        return response.json();
    },

    /**
     * Event handler for a form submit event.
     * @see https://developer.mozilla.org/en-US/docs/Web/API/HTMLFormElement/submit_event
     * @example const exampleForm = document.getElementById("example-form");
     *          exampleForm.addEventListener("submit", handleFormSubmit);
     * @param {SubmitEvent} event
     */
    handleFormSubmit: async (event) => {
        event.preventDefault();
        const form = event.currentTarget;
        const url = form.action;

        try {
            const formData = new FormData(form);
            const responseData = await bs.postFormDataAsJson({url, formData});
            console.log({responseData});
        } catch (error) {
            console.error(error);
        }
    },

    /***
     * This is a function that will handle the form submit and fetch as json to our api... after it will call the
     * callback function with the response as json or as text if stuff fails
     * @param callback a function that will be called with the fetch response callback(data,err)
     * @return {function(*): Promise<void>}
     */
    handleFormSubmitCallback: (callback) => {
        return async (event) => {
            event.preventDefault();
            const form = event.currentTarget;
            const url = form.action;

            try {
                const formData = new FormData(form);
                const responseData = await bs.postFormDataAsJson({url, formData});
                // console.log({ responseData });
                callback(responseData, null);
            } catch (error) {
                console.error(error);
                callback(null, error);
            }
        }
    },


    /***
     * This is for handling requestes that return {success: true/false msg:"My status message"} it will automaticaly
     * replace the inner tect of the status elament and show it.  It will also show the dic if hiden
     * @param statusElement the html element to show and put the response in.
     * @return {function(*): Promise<void>}
     */
    handleFormSubmitStatus: (statusElement) => {
        return bs.handleFormSubmitCallback((res, err) => {
            // statusElement = $(statusElement);

            if (err) {
                // console.error(err);
                res = {success: false, msg: "Network Request Error Please Try Again"};
            }

            bs.resStatus(res, statusElement);

        });
    },

    handleFormSubmitNotify: () => {
        return bs.handleFormSubmitCallback((res, err) => {
            // statusElement = $(statusElement);

            if (err) {
                // console.error(err);
                res = {"success": false, "msg": "Network Request Error Please Try Again"};
            }

            bs.resNotify(res);
            // statusElement.style.display = 'block';
            // if (res.success) {
            //     statusElement.classList.toggle('alert-success', true);
            //     statusElement.classList.toggle('alert-danger', false);
            //
            // } else {
            //     statusElement.classList.toggle('alert-success', false);
            //     statusElement.classList.toggle('alert-danger', true);
            // }
            // statusElement.innerText = res.msg;
        });
    },

    // https://stackoverflow.com/questions/23150333/html5-javascript-dataurl-to-blob-blob-to-dataurl
    //**dataURL to blob**
    dataURLtoBlob: (dataurl) => {
        var arr = dataurl.split(','), mime = arr[0].match(/:(.*?);/)[1],
            bstr = atob(arr[1]), n = bstr.length, u8arr = new Uint8Array(n);
        while (n--) {
            u8arr[n] = bstr.charCodeAt(n);
        }
        return new Blob([u8arr], {type: mime});
    },

    /**
     * blob to dataURL
     * @param blob
     * @param callback
     * @return {Promise<unknown>}
     */
    blobToDataURL: (blob, callback) => {
        return new Promise(resolve => {
            let reader = new FileReader();
            reader.onload = function (e) {
                if (typeof callback == "function") {
                    callback(e.target.result);
                }
                resolve(e.target.result);
            }
            reader.readAsDataURL(blob);
        });
    },

    /**
     * Makes the elements unique by converting to a set and then back to an array.
     * @param array
     * @return {any[]}
     */
    uniqueArray: (array) => {
        return Array.from(new Set(array));
    },

    async sendJsonAndDownload(method, url, json) {

        const fetchOptions = {
            method: method,
            headers: {
                "Content-Type": "application/json",
                Accept: "application/json",
                credential: "same-origin"
            },
            body: JSON.stringify(json),
        };

        const response = await fetch(url, fetchOptions);

        if (!response.ok) {
            const errorMessage = await response.text();
            // throw new Error(errorMessage);
            console.error(errorMessage);
            bs.resNotify({"success": false, "msg": "File Not Found On Server"});
        }


        let blob = await response.blob();
        let filename = url.split('/').pop();
        filename = decodeURIComponent(filename).split('=').pop();
        bs.downloadBlob(blob, filename)
        console.log("DOWNLOADED");
    },

    downloadBlob: (blob, filename) => {
        // Create an object URL for the blob object
        const url = URL.createObjectURL(blob);

        // Create a new anchor element
        const a = document.createElement('a');

        // Set the href and download attributes for the anchor element
        // You can optionally set other attributes like `title`, etc
        // Especially, if the anchor element will be attached to the DOM
        a.href = url;
        a.download = filename || 'download';

        // Click handler that releases the object URL after the element has been clicked
        // This is required for one-off downloads of the blob content
        const clickHandler = () => {
            setTimeout(() => {
                URL.revokeObjectURL(url);
                this.removeEventListener('click', clickHandler);
            }, 150);
        };

        // Add the click event listener on the anchor element
        // Comment out this line if you don't want a one-off download of the blob content
        a.addEventListener('click', clickHandler, false);

        // Programmatically trigger a click on the anchor element
        // Useful if you want the download to happen automatically
        // Without attaching the anchor element to the DOM
        // Comment out this line if you don't want an automatic download of the blob content
        a.click();

        // Return the anchor element
        // Useful if you want a reference to the element
        // in order to attach it to the DOM or use it in some other way
        return a;
    },

    /**
     * Test the width of some text in px
     * @param string the string
     * @param fontSize https://www.w3schools.com/jsref/prop_style_fontsize.asp
     * @return {*}
     */
    testTextLength: (string, fontSize = 12) => {
        if (!bs._testTextLengthDiv) {
            document.body.insertAdjacentHTML("beforeend", `
            <div id="bs_testTextLengthDiv" style="position: absolute; visibility: hidden;height: auto;width: auto;white-space: nowrap;">
            </div>`);

            bs._testTextLengthDiv = document.getElementById('bs_testTextLengthDiv');
        }

        let test = bs._testTextLengthDiv;
        test.innerText = string;
        test.style.fontSize = fontSize;
        // let height = (test.clientHeight + 1) + "px";
        let width = (test.clientWidth + 1);
        return width
    },
    /**
     * Takes in jquery fourm inputs and givs you an object with outputs back
     * @param $inputs suports inputs and textareas
     * @return {{}}
     */
    $val: ($inputs) => {
        let ret = {}
        Object.keys($inputs).forEach(key => {
            let i = $inputs[key]

            if (i.attr('type') == 'checkbox') {
                ret[key] = i[0].checked;
            } else {
                ret[key] = i.val()
            }
        })
        return ret;
    },

    /**
     * Converts foo bar => Foo Bar
     * @param {string} string - the regular string
     * @return {string} - The Capitalized String
     */
    capitalizeFirstLetters: (string) => {
        return string.split(' ').map(s => s.charAt(0).toUpperCase() + s.slice(1)).join(' ');
    },

    /**
     * Converts foo_bar, foo-bar, foo.bar => Foo Bar
     * @param {string} name - the string
     * @return {string} - the nice name string;
     */
    niceName: (name) => {
        let s = String(name).replace(/\.|-|_/g, ' ')//replace _ or . or - with ' '
        s = bs.capitalizeFirstLetters(s);

        s = s.split(' ').map(w => {
            if (bs._acronyms.includes(w.toUpperCase())) {
                return w.toUpperCase();
            }
            if (bs._connectors.includes(w.toLowerCase())) {
                return w.toLowerCase();
            }
            return w;
        }).join(' ');

        return s
    },

    _acronyms: ['BC', 'AB', 'SK', 'DID'],
    _connectors: ['in', 'and', 'the', 'on'],

    select2AddItemAndSelect: ($el, value, niceName) => {
        niceName = niceName || value;
        // Set the value, creating a new option if necessary
        if ($el.find(`option[value='${value}']`).length) {
            $el.val(value).trigger('change');
        } else {
            // Create a DOM Option and pre-select by default
            var newOption = new Option(niceName, value, true, true);
            // Append it to the select
            $el.append(newOption).trigger('change');
        }
    },
    select2Select: ($el, value) => {
        $el.val(value).trigger('change');
    },

    /**
     * Convert an array of objects to csv
     * https://codepen.io/danny_pule/pen/WRgqNx
     * @param {Object[]} objArray - an array of objects
     * @return {String} - string of csv
     */
    convertToCSV: (objArray, header = true) => {

        let deliminator = ",";
        let array = typeof objArray != 'object' ? JSON.parse(objArray) : objArray;
        let str = '';
        if (header) {
            let h = {}
            for (let index in array[1]) {
                h[index] = index;
            }
            array = [h, ...array];
        }
        //todo make sure that objects have items ordered the same for everything and some headers are not missing from row 1
        for (let i = 0; i < array.length; i++) {
            let line = '';
            for (const key in array[i]) {
                if (line != '') line += deliminator

                let val = JSON.stringify(array[i][key]);
                if (val.includes(deliminator) && val[0] != '\"') {
                    // let escaped = val.replace("", '\"')
                    let escaped = val.replace(/"/g, '""');
                    val = `"${escaped}"`
                }

                line += val;
            }

            str += line + '\r\n';
        }

        return str;
    },

    makeTableGrid: (config) => {
        if(typeof config == 'string') {
            config = {route: config};
        }
        if(!config) config = {route: '/api/lorrnel/base_data/v_pmap_weed_inspections'}

        let grid = new TableGrid(config);

        grid.oninit.subscribe(()=>{
            let fields = grid.avalibleColumns.map(c=>c.name);

            let template = {
                title: 'input',
                width: {type:'radiolist', data: ['50vw','90vw', '500px', '900px'], value: '90vw'},
                height: {type:'radiolist', data: ['42vh','70vh', '420px', '700px'],value: '42vh'},
                position: {type:'radiolist', data: ['center','left-bottom', 'center-bottom', 'right-bottom'],value: 'right-bottom'},
                start: {type:'radiolist', data: ['off', 'normalized','maximized', 'minimized'], value:'normalized'},
                id_field: {type:'select2', data: fields, value:'id',title: 'What filed in the table should be used UNIQUE ID'},
                name_field: {type:'select2', data: fields, value:'id', title: 'What filed in the table should be used a a label / human name'},
                geom_field: {type:'select2', data: fields, value:'geom', title: 'What filed in the table should be used for geom'},
                layer_mode: {type:'radiolist', data: ['geojson', 'cluster'],value: 'geojson'},
            }

            let vals = {
                title: bs.niceName(config.route.split('/').pop().substr(2)),
                id_field: 'id',
                name_field: 'id',
                geom_field: 'geom'
            }
            grid.avalibleColumns.forEach(col => {
                console.log(col);
                // {type:'checklist', data: ['visible','edit'],value: '42vh'}
                let key = 'column_' + col.field;
                template[key] = {type: 'checklist', data: ['visible', 'edit']};
                vals[key] = ['visible'];
                if(['id', 'geom'].includes(col.field)) vals[key] = [];
            });


            bs.prompt('Please Fill out The table form', template, vals)

        })

    },

//https://stackoverflow.com/a/38750895
    //constructs a new object an only copies the specified keys to it.
    filterObject: (data, keys) => {

        return Object.keys(data)
            .filter(key => keys.includes(key))
            .reduce((obj, key) => {
                obj[key] = data[key];
                return obj;
            }, {});

    },

// example
// bs.prompt('Create New Preset', {name:'input', id:'input'}).then(data=>{
//    console.log("Data: ", data)//Data: {name: joe, id: uid34}
//     }).catch(e=>{//user cancels
//   console.error(e);
// });
    /*
    * Types
    * 'input'
    * 'textarea'
    * {
    *  type:'datepicker',
    *  options: {}//https://api.jqueryui.com/datepicker/#method-option
    * }
    * {
    *  type:'datalist',
    *  placeholder: 'foo',
    *  data: ['foo', 'bar', 'baz'],
    *  //inputType: 'number',
    * }
    * {
    *   type: 'select2'
    *   placeholder: 'foo'
    *   data: ['foo', 'bar', 'baz'],
    *   niceNames: ['My Foo', 'My Bar', 'My Bazzz'] // optional default is bs.niceName(data[i]);
    *   options: {//select2 options
    *       tags: true
    *   }
    * }
    * {
    * type: 'draw' //requires leaflet draw and a on map global object
    * featureType: 'point' //Marker todo: set up more https://postgis.net/docs/ST_AsGeoJSON.html
    *                                https://postgis.net/docs/ST_GeomFromGeoJSON.html
    * }
    *
    * all types with use bs,niceName(name) for label unless template[name].niceName exists
    * if  template[name].required = true then submit will fail if empty string or null or empty array or something is present and give an error
    * */
    demoPrompt: () => {

        bs.prompt('DEMO', {
            input: 'input',
            number: {
                type: 'datalist',
                inputType: 'number',
                title: 'this is a tooltip',
                required: true,
                data: [1, 5, 10, 100, 1337]
            },
            date: {type: 'datepicker'},
            select2: {
                type: 'select2',
                data: ['foo', 'bar'],
                niceNames: ['Foody Doody', "Barnys Bar"]
            },
            multiple_tags: {
                type: 'select2',
                data: ['ab', 'bc', 'sk'],
                options: {
                    tags: true,//enables freetext response
                    multiple: true//allows multiple entries outputs array
                }
            },
            tags: {
                type: 'select2',
                data: ['ab', 'bc', 'sk'],
                options: {
                    tags: true,//enables freetext response
                    // multiple: true//allows multiple entries outputs array
                }
            },
            multiple: {
                type: 'select2',
                data: ['ab', 'bc', 'sk'],
                options: {
                    // tags: true,//enables freetext response
                    multiple: true//allows multiple entries outputs array
                }
            },
            radiolist: {
                type: 'radiolist',
                data: ['car', 'plane', 'boat']
            },

            checklist: {
                type: 'checklist',
                data: ['car', 'plane', 'boat']
            },

            checkbox: 'checkbox',
            json: 'json',
            draw: {
                type: 'draw',
                featureType: 'point'
            },
            textarea: {
                type: 'textarea',
                placeholder: 'You Can Type Here :)'
            }

        })


    },
    /**
     * Prompts the user for some data and returns it.
     * todo make this its own lib it has outgrown being a utility function
     * @param {String} title - the title for the modal
     *
     * @param {Object} template - template objects keys = field, value = type / options ie "(name: 'input', id: 'input')"
     * @param {Object | String} template.value - the value of a template field this a config object or a string with the
     *      type as the cellValue
     * @param {String} template.value.type - configure what type you wish
     * @param {Value} template.value.type.input - input field use inputType to set type :: returns string
     * @param {Value} template.value.type.textarea - a html textarea :: returns string
     * @param {Value} template.value.type.datalist - a html datalist needs value.data array :: returns string
     * @param {Value} template.value.type.datepicker - jquery datepicker [options]{@link https://api.jqueryui.com/datepicker/} :: returns string
     * @param {Value} template.value.type.select2 - select2 dropdown list [options]{@link https://select2.org/configuration/options-api} :: returns string | array<string>
     * @param {Value} template.value.type.radiolist - same interface as datalist but makes a bunch of radio buttons. returns string  :: returns string
     * @param {Value} template.value.type.checklist - same interface as datalist/select2 returns array of data values like multiple select2  :: returns array<string>
     * @param {Value} template.value.type.checkbox - makes a checkbox returns boolean true if cheacked :: returns bool
     * @param {Value} template.value.type.json - edit json objects with a jsonEditor [options]{@link https://github.com/josdejong/jsoneditor/blob/master/docs/api.md#configuration-options}   :: returns json_string
     * @param {Value} template.value.type.draw - use Leaflet.Draw plugin to pick a spatial feature on the map :: returns geojson Object
     * @param {String} [template.value.niceName='bs.niceName(key)'] - this will go in the label, defaults to bs.niceName(key)
     * @param {Boolean} [template.value.required=false] - if true this field will be marked required
     * @param {String} [template.value.title='Enter A Value'] - the tooltip/html title to add to the element
     * @param {Array} [template.value.titles] - used for checklists and radiolists to test tooltips on individual checkboxes
     * @param {String} [template.value.placeholder] - if applicable the placeholder value in a input or textarea title will be assignest to data item with the same index
     * @param {Array} [template.value.data] - for things that let you select from a list like (datalist, radiolist, checklist, select2)
     *      this is an array of the available data values
     * @param {Array} [template.value.niceNames='bs.niceName(data)'] - if applicable these are nice names for the data values above (radiolist, checklist,select2)
     * @param {String} [template.value.inputType='text'] - for input/datalist lets you set the html type field (usually for number)
     * @param {String} [template.value.featureType='point'] - for draw only, sets what type of feature to draw. (point, polyline rect, circle, polygon) todo suport wkt
     * @param {String} [template.value.value] - this is an optional redundant way of specifying a default value instead of
     *      in the value object.  note that if both are specified in both the value object takes priority
     *
     * @param {String} [template.value.options]  - for submodule types like datepicker and select2 these options are passed
     *      on to the respective constructor
     *
     * @param {Object} [values] - data to populate the template with; essentially the row/data
     *
     * @param {Object} [options] - optional options for the prompt
     * @param {String} [options.submit="Submit"] - the text for the submit button
     * @param {String} [options.prependHTML] - html to put before the prompt stuff
     * @param {String} [options.appendHTML] - html to put after the prompt stuff
     * @param {Boolean} [options.required=false] - if set to true all fields will be required by default unless template[key].required === false
     * @return {Promise<template>} - Promise returns the filled in template ie {name: joe, id: uid34} or rejects if user cancels
     */
    prompt: (title, template, values, options) => {

        options = options || {};
        let html = '';
        let editButtons = {};
        let drawButtons = {};
        let select2init = {};
        let items = {};

        //find the longest label
        let maxLabelChars = Math.max(...Object.keys(template).map(k => bs.testTextLength(k)));
        maxLabelChars += 8;//a little padding
        // maxLabelChars = bs.testTextLength('m'.repeat(maxLabelChars));
        // let maxLabelChars = Math.max(...Object.keys(template).map(k => k.length));
        // maxLabelChars = bs.testTextLength('m'.repeat(maxLabelChars));

        if (options.prependHTML) {
            html += options.prependHTML;
        }

        //FOR EACH TEMPLATE ITEM build the html /setup
        Object.keys(template).forEach(name => {

            let type = template[name];
            let niceName = bs.niceName(name)

            //defaults
            let id = 'bsid_' + bs.id++;
            let title = 'Enter A Value';
            let placeholder = "";
            let required = !!options.required;
            let inputType = 'text';

            //if object override defaults
            if (bs.isObject(template[name])) {
                type = template[name].type;
                if (template[name].niceName) niceName = template[name].niceName;
                if (template[name].title) title = template[name].title;
                if (template[name].placeholder) placeholder = template[name].placeholder;
                if (typeof template[name].required != "undefined") required = template[name].required;
                if (template[name].inputType) inputType = template[name].inputType;
            }

            if (template[name].required) {
                niceName += '*'
                title += ' (Required Field)'
            }

            let value = ''
            if (values && typeof values[name] != "undefined") {
                value = values[name];
            } else if (typeof template[name].value != "undefined") {
                value = template[name].value
            }

            // if (values) value = values[name] || '';

            switch (type) {
                case 'input': {
                    html += `
                    <div title='${title}' class="form-group">
                        <label  for="${id}" >${niceName}:</label>
                        <input  type="${inputType}" class="form-control" name="${name}" id="${id}" value="${value}" placeholder="Type here">
                    </div>
                    `;
                    items[name] = {"type": type, "id": id};
                    break;
                }
                case 'datepicker': {
                    html += `
                    <div title="${title}" class="form-group">
                        <label for="${id}" >${niceName}:</label>
                        <input type="text" class="form-control" name="${name}" id="${id}" value="${value}" placeholder="Type here">
                    </div>
                    `

                    let options = {}
                    if (items[name] && items[name].options) {
                        options = items[name].options
                    }
                    items[name] = {"type": type, "id": id, "options": options};
                    break;
                }
                case 'textarea': {
                    placeholder = placeholder || 'Type Here'
                    html += `
                <div title="${title}" class="form-group">
                    <label for="${id}" >${niceName}:</label>
                    <textarea type="text" class="form-control" name="${name}" id="${id}" rows="2" placeholder="${placeholder}">${value}</textarea>
                </div>
                `
                    items[name] = {"type": type, "id": id};

                    break;
                }
                case 'select2': {

                    html += `<div title='${title}' class="form-group">
                    <label for="${id}" style="width: ${maxLabelChars}px"> ${niceName}: </label>
                    <select id="${id}" name="${name}"  placeholder="${placeholder}"  value="${value}" class="" style="min-width: 50%; max-width: 95%">
               `
                    //populate select2
                    for (let i = 0; i < template[name].data.length; i++) {

                        let d = template[name].data[i];
                        let niceName;
                        if (d === null) {
                            niceName = 'null'
                        } else if (typeof d != 'string') {
                            // console.error("Data Must be string")
                            niceName = JSON.stringify(d);
                        } else {
                            niceName = bs.niceName(d);
                        }
                        if (template[name].niceNames && template[name].niceNames[i]) {
                            niceName = template[name].niceNames[i];
                        }

                        html += `<option value="${d}"  ${(d == value || (Array.isArray(value) && value.includes(d))) ? 'selected="selected"' : ''}>${niceName}</option>`
                    }
                    html += ` </select> </div>`

                    select2init[id] = template[name].options;

                    items[name] = {"type": type, "id": id};
                    break;
                }
                case 'datalist': {
                    html += `<div title='${title}' class="form-group">
                    <label for="${id}" style="width: ${maxLabelChars}px"> ${niceName} </label>
                    <input type="${inputType}" id="${id}" name="${name}" list="list_${id}"  placeholder="${placeholder}"  value="${value}" class="bs-datalist"/>
                    <datalist id="list_${id}" >   
               `

                    template[name].data.forEach(d => {
                        html += `<option value="${d}">`
                    })
                    html += ` </datalist> </div>`

                    items[name] = {"type": type, "id": id};
                    break;
                }
                case 'radiolist': {


                    html += `<div title='${title}' class="form-group">`
                    html += `<label title='${title}' for="${id}" style="width: ${maxLabelChars}px"> ${niceName}: </label>`


                    let ts = template[name].titles
                    let hasTitles = Array.isArray(ts);

                    for (let i = 0; i < template[name].data.length; i++) {
                        let d = template[name].data[i]
                        let nn;

                        if (template[name].niceNames && template[name].niceNames[i]) {
                            nn = template[name].niceNames[i];
                        } else {
                            nn = bs.niceName(d);
                        }

                        let itemTitle = hasTitles ? ts[i] : title;


                        html += `
                            <label title='${itemTitle}' class="prompt-checkbox-label radio-inline">`
                        if (d == value) {
                            html += `<input type="radio" name="${name}" value="${d}" checked>&nbsp; ${nn}`
                        } else {
                            html += `<input type="radio" name="${name}" value="${d}">&nbsp; ${nn}`
                        }
                        html += ` </label>`
                    }
                    html += `</div>`

                    items[name] = {"type": type};

                    break;
                }
                case 'checklist': {
                    html += `<div title='${title}' class="form-group ">`
                    html += `<label title='${title}' for="${id}" style="width: ${maxLabelChars}px"> ${niceName}: </label>`

                    let ts = template[name].titles
                    let hasTitles = Array.isArray(ts);

                    for (let i = 0; i < template[name].data.length; i++) {
                        let d = template[name].data[i]
                        let nn;
                        if (template[name].niceNames && template[name].niceNames[i]) {
                            nn = template[name].niceNames[i];
                        } else {
                            nn = bs.niceName(d);
                        }

                        let itemTitle = hasTitles ? ts[i] : title;


                        html += `
                            <label title='${itemTitle}' class="prompt-checkbox-label checkbox-inline">`

                        //checlist.data = ['cat', 'dog'] accepts both valuse[asd] = [true, false] and valuse[asd] = {cat: true, dog:false} and calues[asd] = ['cat']
                        if (values && values[name] &&
                            (values[name][d] == true || (Array.isArray(values[name]) && (values[name][i] === true || values[name].includes(d))))) {
                            html += `<input type="checkbox" name="${name}" value="${d}" checked>&nbsp; ${nn}`
                        } else {
                            html += `<input type="checkbox" name="${name}" value="${d}">&nbsp; ${nn}`
                        }
                        html += ` </label>`
                    }
                    html += `</div>`

                    items[name] = {"type": type};

                    break;
                }
                case 'checkbox': {
                    let checked = ''
                    if (value) {
                        checked = 'checked'
                    }
                    console.log(name);
                    html += `<div title='${title}' class="form-group">`

                    html += `<div title='${title}' class="form-group">`
                    html += `<label title='${title}' for="${id}" style="width: ${maxLabelChars}px">${niceName}: </label>`
                    html += `<input title='${title}' id="${id}" name="${name}" type="checkbox" aria-label="Checkbox for following label" ${checked}><br>`
                    html += `</div>`
                    items[name] = {"type": type, "id": id};


                    break;
                }
                case 'json': {

                    value = value || '{}';
                    html += `<div title='${title}' class="form-group">
                            <button id="${id}" name="${name}" class="btn btn-info">${name} Edit JSON</button>
                        </div>`

                    editButtons[id] = {
                        value: value,
                        options: template[name].options
                    };//edet events added later

                    items[name] = {
                        type: type,
                        id: id,
                        options: template[name].options
                    };
                    break;
                }
                case 'draw': {

                    // value = value
                    html += `<div title='${title}' class="form-group">
                             <button id="${id}" name="${name}" class="btn btn-info">${niceName} (Select On Map)</button>
                             <span id="${id}_span"></span>
                        </div>`

                    drawButtons[id] = value;//events added later

                    let item = {
                        "type": type,
                        "featureType": template[name].featureType,
                        "id": id,
                        "name": name,
                    };
                    items[name] = item;
                    break;
                }
                case 'array': {

                    // value = value
                    html += `<div title='${title}' class="form-group">
                             <span id="${id}_span">${niceName}</span>
                             <div id="${id}" name="${name}" class="">(Generated By ArayInput)</div>
                        </div>`

                    // ayrayInputs[id] = value;//events added later
                    let val = value
                    if (typeof val == "string") {
                        try {
                            val = JSON.parse(val);
                        } catch (e) {
                            val = [val]
                        }
                    }

                    if (!Array.isArray(val)) {
                        console.warn("Data not an array : ", val, "  (Resetting to [])");
                        val = [];
                    }

                    let item = {
                        "type": type,
                        // "featureType": template[name].featureType,
                        "id": id,
                        "name": name,
                        "values": val
                    };


                    // if (!Array.isArray(item.value)) {
                    //     if (typeof item.value == "string") {
                    //         item.value == [item.value];
                    //     } else {
                    //         item.value = [];
                    //     }
                    // }


                    items[name] = item;
                    break;
                }

            }


            /* if (type === 'input') {

                 html += `
                 <div title='${title}' class="form-group">
                     <label  for="${id}" >${niceName}:</label>
                     <input  type="${inputType}" class="form-control" name="${name}" id="${id}" value="${value}" placeholder="Type here">
                 </div>
                 `
                 items[name] = {"type": type, "id": id};

             } else if (type === 'datepicker') {

                 html += `
                 <div title="${title}" class="form-group">
                     <label for="${id}" >${niceName}:</label>
                     <input type="text" class="form-control" name="${name}" id="${id}" value="${value}" placeholder="Type here">
                 </div>
                 `

                 let options = {}
                 if (items[name] && items[name].options) {
                     options = items[name].options
                 }
                 items[name] = {"type": type, "id": id, "options": options};

             } else if (type === 'textarea') {

                 placeholder = placeholder || 'Type Here'
                 html += `
                 <div title="${title}" class="form-group">
                     <label for="${id}" >${niceName}:</label>
                     <textarea type="text" class="form-control" name="${name}" id="${id}" rows="2" placeholder="${placeholder}">${value}</textarea>
                 </div>
                 `
                 items[name] = {"type": type, "id": id};

             } else if (type === 'select2') {

                 html += `<div title='${title}' class="form-group">
                     <label for="${id}" style="width: ${maxLabelChars}px"> ${niceName}: </label>
                     <select id="${id}" name="${name}"  placeholder="${placeholder}"  value="${value}" class="" style="min-width: 50%; max-width: 95%">
                `
                 //populate select2
                 for (let i = 0; i < template[name].data.length; i++) {

                     let d = template[name].data[i];
                     let niceName;
                     if (d === null) {
                         niceName = 'null'
                     } else if (typeof d != 'string') {
                         // console.error("Data Must be string")
                         niceName = JSON.stringify(d);
                     } else {
                         niceName = bs.niceName(d);
                     }
                     if (template[name].niceNames && template[name].niceNames[i]) {
                         niceName = template[name].niceNames[i];
                     }

                     html += `<option value="${d}"  ${(d == value || (Array.isArray(value) && value.includes(d))) ? 'selected="selected"' : ''}>${niceName}</option>`
                 }
                 html += ` </select> </div>`

                 select2init[id] = template[name].options;

                 items[name] = {"type": type, "id": id};

             } else if (type === 'datalist') {

                 html += `<div title='${title}' class="form-group">
                     <label for="${id}" style="width: ${maxLabelChars}px"> ${niceName} </label>
                     <input type="${inputType}" id="${id}" name="${name}" list="list_${id}"  placeholder="${placeholder}"  value="${value}" class="bs-datalist"/>
                     <datalist id="list_${id}" >
                `

                 template[name].data.forEach(d => {
                     html += `<option value="${d}">`
                 })
                 html += ` </datalist> </div>`

                 items[name] = {"type": type, "id": id};
             } else if (type === 'radiolist') {

                 html += `<div title='${title}' class="form-group">`
                 html += `<label title='${title}' for="${id}" style="width: ${maxLabelChars}px"> ${niceName}: </label>`
                 for (let i = 0; i < template[name].data.length; i++) {
                     let d = template[name].data[i]
                     let nn;
                     if (template[name].niceNames && template[name].niceNames[i]) {
                         nn = template[name].niceNames[i];
                     } else {
                         nn = bs.niceName(d);
                     }
                     html += `
                             <label title='${title}' class="prompt-checkbox-label radio-inline">`
                     if (d == value) {
                         html += `<input type="radio" name="${name}" value="${d}" checked>&nbsp; ${nn}`
                     } else {
                         html += `<input type="radio" name="${name}" value="${d}">&nbsp; ${nn}`
                     }
                     html += ` </label>`
                 }
                 html += `</div>`

                 items[name] = {"type": type};


             } else if (type === 'checklist') {

                 html += `<div title='${title}' class="form-group ">`
                 html += `<label title='${title}' for="${id}" style="width: ${maxLabelChars}px"> ${niceName}: </label>`
                 for (let i = 0; i < template[name].data.length; i++) {
                     let d = template[name].data[i]
                     let nn;
                     if (template[name].niceNames && template[name].niceNames[i]) {
                         nn = template[name].niceNames[i];
                     } else {
                         nn = bs.niceName(d);
                     }
                     html += `
                             <label title='${title}' class="prompt-checkbox-label checkbox-inline">`

                     //checlist.data = ['cat', 'dog'] accepts both valuse[asd] = [true, false] and valuse[asd] = {cat: true, dog:false} and calues[asd] = ['cat']
                     if (values && values[name] &&
                         (values[name][d] == true || (Array.isArray(values[name]) && (values[name][i] === true || values[name].includes(d))))) {
                         html += `<input type="checkbox" name="${name}" value="${d}" checked>&nbsp; ${nn}`
                     } else {
                         html += `<input type="checkbox" name="${name}" value="${d}">&nbsp; ${nn}`
                     }
                     html += ` </label>`
                 }
                 html += `</div>`

                 items[name] = {"type": type};
             } else if (type === 'checkbox') {

                 let checked = ''
                 if (value) {
                     checked = 'checked'
                 }
                 console.log(name);
                 html += `<div title='${title}' class="form-group">`

                 html += `<div title='${title}' class="form-group">`
                 html += `<label title='${title}' for="${id}" style="width: ${maxLabelChars}px">${niceName}: </label>`
                 html += `<input title='${title}' id="${id}" name="${name}" type="checkbox" aria-label="Checkbox for following label" ${checked}><br>`
                 html += `</div>`
                 items[name] = {"type": type, "id": id};


             } else if (type === 'json') {

                 value = value || '{}';
                 html += `<div title='${title}' class="form-group">
                             <button id="${id}" name="${name}" class="btn btn-info">${name} Edit JSON</button>
                         </div>`

                 editButtons[id] = {
                     value: value,
                     options: template[name].options
                 };//edet events added later

                 items[name] = {
                     type: type,
                     id: id,
                     options: template[name].options
                 };
             } else if (type === 'draw') {

                 // value = value
                 html += `<div title='${title}' class="form-group">
                              <button id="${id}" name="${name}" class="btn btn-info">${niceName} (Select On Map)</button>
                              <span id="${id}_span"></span>
                         </div>`

                 drawButtons[id] = value;//events added later

                 let item = {
                     "type": type,
                     "featureType": template[name].featureType,
                     "id": id,
                     "name": name,
                 };
                 items[name] = item;
             }
 */
            items[name].required = required;

        })//end for each field

        if (options.appendHTML) {
            html += options.appendHTML;
        }


        //return a promise for when its submitted outputs the filled in template object
        return new Promise((resolve, reject) => {

            let $modal;
            if (bs.jspanelModals == true && jsPanel) {

                let container = document.createElement('div');
                container.innerHTML = html;
                let submit = document.createElement('button');
                submit.innerHTML = 'SUBMIT'
                container.appendChild(submit);

                let modal = jsPanel.modal.create({
                    theme: 'dark',
                    headerTitle: '<i class="fad fa-edit"></i>modal panel',
                    content: container,


                });
                $modal = $(modal);

                submit.addEventListener('click', (e) => {
                    modal.close();
                })

                // modal.onload(e => {
                //     console.log(e)
                // })


            } else {
                //todo: fix leaking modals doms on close
                $modal = bs.createModal('bs_prompt_' + bs.id++, title, html, options.submit || "Submit",
                    (modal) => {//called when modal is submited
                        let ret = {}
                        let success = true;
                        Object.keys(items).forEach(name => {
                            let el = document.getElementById(items[name].id);
                            let localSuccess = true;
                            switch (items[name].type) {

                                case "select2":
                                    if (template[name].options && template[name].options.multiple) {
                                        let val = $(el).select2('data').map(d => d.id)
                                        ret[name] = val

                                        if ((!Array.isArray(val) || val.length === 0) && items[name].required) {
                                            bs.resNotify({"success": false, "msg": name + " is required"});
                                            localSuccess = false;
                                        }
                                        break;
                                    }//else continue and use val
                                case "datalist":
                                case "textarea":
                                case "datepicker":
                                case "input":
                                    let val = el.value;
                                    if (!val && items[name].required) {
                                        bs.resNotify({"success": false, "msg": name + " is required"});
                                        localSuccess = false;
                                    }
                                    ret[name] = el.value;
                                    break;

                                case "radiolist": {
                                    let els = modal.querySelectorAll(`input[name="${name}"]`)
                                    // console.log(els)
                                    let val = '';
                                    Array.from(els).forEach(el => {
                                        if (el.checked == true) {
                                            val = el.value;
                                        }
                                    })

                                    if (!val && items[name].required) {
                                        bs.resNotify({"success": false, "msg": name + " is required"});
                                        localSuccess = false;
                                    }
                                    // console.log(val);
                                    ret[name] = val;
                                    break;
                                }
                                case "checklist": {
                                    let els = modal.querySelectorAll(`input[name=${name}]`)
                                    // console.log(els)
                                    let val = [];
                                    Array.from(els).forEach(el => {
                                        if (el.checked == true) {
                                            val.push(el.value)
                                            // val = el.value;
                                        }
                                    })

                                    if (!val.length == 0 && items[name].required) {
                                        bs.resNotify({"success": false, "msg": name + ": at least one is required"});
                                        localSuccess = false;
                                    }
                                    // console.log(val);
                                    ret[name] = val;
                                    break;
                                }
                                case "checkbox": {
                                    let el = modal.querySelector(`input[name="${name}"]`)
                                    let val = el.checked
                                    ret[name] = val;

                                    if (!val && items[name].required) {
                                        bs.resNotify({"success": false, "msg": name + " is required"});
                                        localSuccess = false;
                                    }
                                    break;
                                }
                                case 'json': {
                                    let val = editButtons[items[name].id].value;
                                    ret[name] = val;
                                    break;
                                }
                                case 'draw': {
                                    let val = drawButtons[items[name].id];
                                    ret[name] = val;
                                    if (!val && !val.type && items[name].required) {
                                        bs.resNotify({"success": false, "msg": name + " is required"});
                                        localSuccess = false;
                                    }
                                    break;
                                }
                                case 'array': {

                                    let val = items[name].arrayInput.getArray();

                                    if (options.stringifyArrays) {
                                        val = JSON.stringify(val);
                                    }
                                    console.log("Array Output: ", val)
                                    ret[name] = val;
                                    if (!val && !val.type && items[name].required) {
                                        bs.resNotify({"success": false, "msg": name + " is required"});
                                        localSuccess = false;
                                    }
                                    break;
                                }

                            }

                            if (!localSuccess) {
                                success = localSuccess;

                                if (items[name].type == 'select2') {
                                    el.nextElementSibling.firstElementChild.firstElementChild
                                        .style.borderColor = "red";
                                } else {
                                    el.style.borderColor = "red"
                                }
                            }
                        });


                        if (!success) {
                            return;
                        }

                        console.log("Prompt Data: ", ret);
                        $modal.modal('hide');
                        // $modal.modal('dispose');
                        // $modal[0].remove();
                        resolve(ret);

                    });

            }


            $modal.on('hidden.bs.modal', (e) => {

                if ($modal.drawInControl) return;// do nothing

                $modal.modal('dispose');
                $modal[0].remove();
                reject({"success": false, "msg": "User closed the modal"})
            })

            //setup events for json buttons
            Object.keys(editButtons).forEach(id => {
                $modal.on('click', `#${id}`, e => {
                    bs.editJson('Edit: ' + e.target.name, editButtons[id].value, editButtons[id].options).then(data => {
                        editButtons[id].value = data;
                    })
                });
            })

            //setup events for draw buttons
            Object.keys(drawButtons).forEach(id => {
                $modal.on('click', `#${id}`, e => {


                    let item = items[e.target.name];

                    $modal.drawInControl = true;
                    $modal.modal('hide');
                    let draw
                    switch (item.featureType) {
                        case 'point': {
                            draw = new L.Draw.Marker(map);
                            map.once(L.Draw.Event.CREATED, e => {
                                console.log("Draw Created:", e);
                                let layer = e.layer;
                                let gJ = layer.toGeoJSON();
                                drawButtons[id] = gJ

                                let str = `lat: ${gJ.geometry.coordinates[1]}, lon: ${gJ.geometry.coordinates[0]}`
                                $(`#${id}_span`).html(str);

                                layer.remove()
                                $modal.modal('show');
                                $modal.drawInControl = false;
                            })
                            break
                        }
                        default : {
                            console.error(item.featureType, ' not implemented yet');
                        }
                    }

                    draw.enable();

                });
            })

            //select2ify / datpickerify init stuff
            $modal.one('shown.bs.modal', (e) => {


                Object.keys(select2init).forEach(id => {
                    let el = $(`#${id}`)
                    console.log("SELECTING IT ", select2init[id], 'el:', el);
                    el.select2(select2init[id]);


                    let v = values && values[el.attr('name')] ? values[el.attr('name')] : '';
                    // el.val(v).change();
                    if (typeof v != "undefined") {
                        if (v == "" || v == 'null') {
                            v = null;
                            el.val(v).change();
                        } else {
                            el.val(v).change();

                            bs.select2AddItemAndSelect(el, v);
                        }
                    }

                });

                Object.keys(items).forEach(name => {
                    let item = items[name];

                    if (item.type == 'datepicker') {
                        let el = $(`#${item.id}`);
                        el.datepicker(item.options);
                    }

                    if (item.type == 'select2') {
                        //https://stackoverflow.com/a/50684420/4530300
                        if (item.inputType == "number") {
                            el.on('keypress', '.select2-search__field', function () {
                                $(this).val($(this).val().replace(/[^\d].+/, ""));
                                if ((event.which < 48 || event.which > 57)) {
                                    event.preventDefault();
                                }
                            });
                        }
                    }

                    if (item.type == 'array') {

                        let el = document.getElementById(item.id);
                        el.innerText = '';
                        let a = new ArrayInput(item.values, {
                            appendTo: el
                        });
                        item.arrayInput = a;
                        console.log("Building Array", a);
                        // el.appendChild(a.div);
                    }
                });//end for
            });//end show

            if (!bs.jspanelModals) {
                $modal.modal('show');
            }
        });


    },

    /**
     * Merger 2 geojson feature collections together.
     * @param g1 - the first geojson
     * @param g2 - the second geojson
     * @return {{features, type: string}}
     */
    concatGeoJSON: (g1, g2) => {
        return {
            "type": "FeatureCollection",
            "features": g1.features.concat(g2.features)
        }
    },

    /**
     * Requres some libs like tokml and shpwrite
     * @param geojson the geojson
     * @param title a title for the prompt
     */
    geojsonDownload:
        (geojson, title, filePrefex, dontAppendDate) => {
            title = title || 'Download GeoJSON'


            let fname = filePrefex;
            if (!dontAppendDate) {
                fname += '_' + moment(moment.now()).format("MM-DD-YYYY")
            }

            let types = ['geojson', 'kml', 'shp', 'userlayer']
            if (!UserLayer) types.pop();
            bs.prompt(title, {
                file_name: 'input',
                file_type: {type: 'radiolist', data: types}
            }, {
                file_name: fname,
                file_type: 'kml'
            }).then(data => {

                    switch (data.file_type) {
                        case 'geojson': {
                            bs.downloadString(JSON.stringify(geojson), 'application/geo+json', data.file_name + '.geojson');
                            break;
                        }
                        case 'kml': {

                            if (!tokml) {
                                bs.resNotify({success: false, msg: 'Error: tokml not installed'});
                                return
                            }
                            let kml = tokml(geojson);

                            console.log(kml);
                            // let kmlURI = 'application/xml;charset=utf-8,' +
                            //     encodeURIComponent(kml);

                            // if you want to use the official MIME type for KML
                            // let kmlURI = 'application/vnd.google-earth.kml+xml;charset=utf-8,'+ encodeURIComponent(kml);
                            bs.downloadString(kml, 'application/vnd.google-earth.kml+xml', data.file_name + '.kml')

                            break;
                        }
                        case 'shp': {

                            var options = {
                                "folder": data.file_name,
                                "types": {
                                    "point": data.file_name,
                                    "polygon": data.file_name,
                                    "line": data.file_name
                                }
                            }

                            if (geojson.type != 'FeatureCollection') {
                                geojson = {
                                    "type": "FeatureCollection",
                                    "features": [geojson],
                                }
                            }
                            if (!shpwrite) {
                                bs.resNotify({success: false, msg: 'Error: shpwrite not installed'});
                                return
                            }
                            let shp = 'data:application/zip;base64,' + shpwrite.zip(geojson, options)
                            let blob = bs.dataURLtoBlob(shp)
                            bs.downloadBlob(blob, data.file_name + '.zip')
                            // shpwrite.download(geojson, options);
                            break;
                        }

                        case 'userlayer': {

                            userLayerDialog.dialog('open')
                            // UserLayer.geojson2layer(geojson);

                            let layer = UserLayer.geojson2layer(geojson);
                            UserLayer.addLayerToLists(layer, data.file_name);
                            // bs.downloadString(JSON.stringify(geojson), 'application/geo+json', data.file_name + '.geojson');
                            break;
                        }

                    }
                }
            )
        },

    /**
     * Groups array by key https://stackoverflow.com/questions/14446511/most-efficient-method-to-groupby-on-an-array-of-objects
     * @param {Array} xs the array to group
     * @param {string} key the key to groupby
     * @return {Object}
     */
    "groupBy": (xs, key) => {
        return xs.reduce(function (rv, x) {
            (rv[x[key]] = rv[x[key]] || []).push(x);
            return rv;
        }, {});
    },


    /**
     * Undoes what groupBy() does
     * @param {Object} grp - Object with arrays of values
     * @return {Array} -the original arrays
     */
    "unGroup": (grp) => {

        //  let keys = Object.keys(grp);
        //  let array = [];
        //  keys.forEach(k => {
        //      array = [...grp[k], ...array]
        //  })
        //  //[...Object.entries(grp).map((v, k) => v)];
        // return array
        return Object.values(grp).flat(2);

    },
    /**
     * @requires npm install jsoneditor
     * @param title - Modal Title
     * @param json - The json to edit
     * @return {Promise<json>} The edited json  or rejects if closed
     * @example //see prompt
     */

    //mode(['pear', 'apple', 'orange', 'apple']); // apple
    //https://stackoverflow.com/a/20762713/4530300
    "mode": (arr) => {
        return arr.sort((a, b) =>
            arr.filter(v => v === a).length
            - arr.filter(v => v === b).length
        ).pop();
    },

// https://github.com/josdejong/jsoneditor/blob/master/docs/api.md#configuration-options
    "editJson": (title, json, options) => {

        let defaults = {
            modes: ['tree', 'view', 'form', 'code', 'text', 'preview'],
            mode: 'tree',
            history: true,

        }
        options = Object.assign(defaults, options || {});

        // if(typeof options.)
        return new Promise((resolve, reject) => {
            let id = bs.id++
            //todo: fix leaking modals doms on close
            let $modal = bs.createModal('bs_editjson_' + id, title, `<div id="jsoneditor_${id}"></div>`, "Save JSON", (modal) => {
                let ret = editor.get();
                console.log("Edited Json: ", ret);
                $modal.modal('hide');//disposed on hidden event after animations;
                resolve(ret);
            });//on submit;

            $modal.on('hidden.bs.modal', (e) => {
                // do something...
                // $modal.modal('hide');
                $modal.modal('dispose');
                $modal[0].remove();
                reject({"success": false, "msg": "User closed the edit modal"})
            });//end on modal close

            const dialog = $modal[0].querySelector(`.modal-dialog`);
            dialog.classList.add('modal-xl');

            // create the editor
            const container = $modal[0].querySelector(`#jsoneditor_${id}`);
            options = bs.mergeDeep({}, options);
            const editor = new JSONEditor(container, options)
            editor.set(json);

            $modal.modal('show');
        });
    },

    /***
     * A function to convert an array of property object to geojson
     * @param {Array<Object>} rows the array of rows
     * @param {String} [geomProperty='geojson'] the property that contains the geometry
     * @returns {{features: [], type: string}} A geojson Feature Collection
     */
    "rowsToGeojson": (rows, geomProperty, options) => {

        if (typeof options == "undefined") {
            options = {};
        }
        let defaults = {
            "onlyid": false,
            "idField": 'id',
            "onlyFields": undefined
        }

        options = bs.mergeDeep(defaults, options)

        if (options.onlyid) {
            options.onlyFields = [options.idField]
        }

        if (typeof geomProperty == "undefined") {
            geomProperty = 'geojson';
        }

        let features = [];
        rows.forEach(row => {
            let geom = typeof row[geomProperty] == 'string' ? JSON.parse(row[geomProperty]) : row[geomProperty];

            let props = {}
            if (Array.isArray(options.onlyFields)) {
                options.onlyFields.forEach(f => {
                    props[f] = row[f];
                })

            } else {
                props = row;
                delete props[geomProperty]
            }
            let feature = {
                "type": 'Feature',
                "geometry": geom,
                "geometry_name": "geom",
                "properties": props,
            }
            features.push(feature);
        });
        let geojson = {
            "type": 'FeatureCollection',
            "features": features
        }
        return geojson;
    },
// const exampleForm = document.getElementById("example-form");
// exampleForm.addEventListener("submit", handleFormSubmit);
    /**
     * Create a bootstrap modal
     * @param id - the html id for the modal submit button gets id+'_submit'
     * @param title - the modal title
     * @param html - the content html
     * @param submit - the button text for submit if false will hide;
     * @param callback callback(modal) when submitted
     * @param [options.size] -'almost_fullscreen'
     * @param [options.classes] - classes to add to the modal hint: $modal.toggleClass()

     * @return DomElement - The $modal;
     */
    "createModal": (id, title, html, submit, callback, options) => {
        let subHidden = '';

        if (!options) options = {};
        let classes = '' || options.classes;

        if (options.size === 'almost_fullscreen') {
            classes += ' modal-fs';
        }

        if (submit === false) subHidden = 'display:none; ';//https://stackoverflow.com/questions/18487056/select2-doesnt-work-when-embedded-in-a-bootstrap-modal
        let $modal = $(`<div class="modal fade ${classes}" id="${id}" role="dialog" aria-labelledby="bsmodal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header p-1">   
                    <h4 class="modal-title m-1" id="myModalLabel">${title}</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    ${html}
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" id="${id + "_submit"}" class="btn btn-primary " style="${subHidden}">${submit}</button>
                </div>
            </div>
        </div>
        </div>
        `);

        // document.body.appendChild($modal[0]);

        $modal.find('#' + id + '_submit').on('click', e => {
            callback($modal[0]);
        });

        /*
        document.getElementById(id + "_submit").addEventListener('click', e => {
            callback($modal[0]);
        })*/

        return $modal;

    },

    /**
     * https://stackoverflow.com/a/15511463/4530300
     * @param size
     * @return {any[]}
     */
    "newArray": (size, width, fill) => {
        var result = new Array(size);
        for (var i = 0; i < size; i++) {
            if (width) {
                result[i] = new Array(width);

            } else {
                result[i] = new Array(size);
            }
            if (typeof fill != "undefined") {
                result[i].fill(fill);
            }
        }

        return result;
    },
    /**
     * https://stackoverflow.com/a/15511463/4530300
     * performs a convolutions using js 2d arrays // i dont think this is correct todo fix it
     * @param {Array<Array<Number>>} filter - the filter or kernal ie [[0.25,0.25], [0.25,0.25]]
     * @param {Array<Array<Number>>} image - the image to be convolved over
     * @return {Array<Array<Number>>}
     */
    "convolveArrays":
        (filter, image, pad) => {
            //todo: take other dimensions into account might break with portrait screens
            // var result = bs.newArray(image.length - filter.length + 1);
            let result = new Array(image.length - filter.length + 1);
            for (var i = 0; i < image.length; i++) {
                var imageRow = image[i];
                result[i] = new Array(imageRow.length - filter.length + 1);

                for (var j = 0; j < imageRow.length; j++) {

                    var sum = 0;
                    for (var w = 0; w < filter.length; w++) {
                        //todo: add padding support
                        if (image.length - i < filter.length) break;

                        var filterRow = filter[w];
                        for (var z = 0; z < filter.length; z++) {
                            if (imageRow.length - j < filterRow.length) break;
                            sum += image[w + i][z + j] * filter[w][z];
                        }
                    }

                    if (i < result.length && j < result.length)
                        result[i][j] = sum;
                }
            }

            return result;
        },
    "thresholdArrays": (thresh, image) => {
        let result = new Array(image.length);
        for (var i = 0; i < image.length; i++) {
            var imageRow = image[i];
            result[i] = new Array(imageRow.length);
            for (var j = 0; j < imageRow.length; j++) {
                if (imageRow[j] < thresh) {
                    result[i][j] = 0;
                } else {
                    result[i][j] = 1;
                }
            }
        }
        return result;
    },

    "maxArrays": (image) => {
        return Math.max(...image.map(r => Math.max(...r)))

    },
    "minArrays": (image) => {
        return Math.min(...image.map(r => Math.min(...r)))
    },

//https://stackoverflow.com/a/30418912/4530300
    "maxRect": (a) => {

        //a sample
        // 0 0 0 0 1 0
        // 0 0 1 0 0 1
        // 0 0 0 0 0 0
        // 1 0 0 0 0 0
        // 0 0 0 0 0 1
        // 0 0 1 0 0 0
        let nrows = a.length;
        let ncols = a[0].length;
        let skip = 1
        let area_max = [0, []]

        let w = bs.newArray(nrows, ncols, 0)
        let h = bs.newArray(nrows, ncols, 0)
        for (let r = 0; r < nrows; r++) {
            for (let c = 0; c < ncols; c++) {

                if (a[r][c] == skip) {
                    continue;
                }
                if (r == 0) {
                    h[r][c] = 1;
                } else {
                    h[r][c] = h[r - 1][c] + 1;
                }
                if (c == 0) {
                    w[r][c] = 1
                } else {
                    w[r][c] = w[r][c - 1] + 1;
                }
                let minw = w[r][c]

                for (let dh = 0; dh < h[r][c]; dh++) {


                    minw = Math.min(minw, w[r - dh][c])
                    let area = (dh + 1) * minw
                    if (area > area_max[0]) {
                        area_max = [area, [r - dh, c - minw + 1, r, c]]
                    }
                }

            }

        }
        console.log('area', area_max[0])
        // for (let t in area_max[1]) {
        console.log("max pos:", area_max[1])
        // }

        return area_max;
// area 12
// Cell 1:(2, 1) and Cell 2:(4, 4)
    },


    // [2, 3, 4] => [0, 0.5, 1]
    "normalizeArrays":
        (image) => {
            let min = this.minArrays(image);
            let max = this.maxArrays(image);
            let range = max - min;
            for (let i = 0; i < image.length; i++) {
                let imageRow = image[i];
                for (let j = 0; j <= imageRow.length; j++) {
                    imageRow[j] = (imageRow[j] - min) / range;
                }
                return image;
            }

        },


}

//table builder
var _table_ = document.createElement('table'),
    _tr_ = document.createElement('tr'),
    _th_ = document.createElement('th'),
    _td_ = document.createElement('td');

_td_.style.paddingRight = '10px';
_table_.style.overflowY = 'auto';
_table_.style.width = '100%';

/**
 * Builds the HTML Table out of a List json data from any restful service.
 * note you can override the above defults to clone
 * @param arr - array of row objects
 * @returns {Node} - the dom for the table
 */
function buildHtmlTable(arr) {
    var table = _table_.cloneNode(false),
        columns = addAllColumnHeaders(arr, table);
    for (var i = 0, maxi = arr.length; i < maxi; ++i) {
        var tr = _tr_.cloneNode(false);
        for (var j = 0, maxj = columns.length; j < maxj; ++j) {
            var td = _td_.cloneNode(false);
            cellValue = arr[i][columns[j]];
            td.appendChild(document.createTextNode(arr[i][columns[j]] || ''));
            tr.appendChild(td);
        }
        table.appendChild(tr);
    }
    return table;
}

// Adds a header row to the table and returns the set of columns.
// Need to do union of keys from all records as some records may not contain
// all records
function addAllColumnHeaders(arr, table) {
    var columnSet = [],
        tr = _tr_.cloneNode(false);
    for (var i = 0, l = arr.length; i < l; i++) {
        for (var key in arr[i]) {
            if (arr[i].hasOwnProperty(key) && columnSet.indexOf(key) === -1) {
                columnSet.push(key);
                var th = _th_.cloneNode(false);
                th.appendChild(document.createTextNode(key));
                tr.appendChild(th);
            }
        }
    }
    table.appendChild(tr);
    return columnSet;
}


//formToObject
// https://github.com/serbanghita/formToObject.js/blob/master/dist/formToObject.min.js
!function (E, t, e) {
    "use strict";
    var r = function () {
        if (!(this instanceof r)) {
            var e = new r;
            return e.init.call(e, Array.prototype.slice.call(arguments))
        }
        var o, n = null, l = {includeEmptyValuedElements: !1, w3cSuccessfulControlsOnly: !1}, i = /[^\[\]]+|\[\]/g,
            a = null;

        function f(e) {
            if (e && "object" == typeof e) return Object.keys(e).filter(function (e) {
                return !isNaN(parseInt(e, 10))
            }).splice(-1)[0]
        }

        function c(e) {
            var n = f(e);
            return "string" == typeof n ? parseInt(n, 10) + 1 : 0
        }

        function s(e) {
            if ("object" != typeof e || null === e) return 0;
            var n, t = 0;
            if ("function" == typeof Object.keys) t = Object.keys(e).length; else for (n in e) e.hasOwnProperty(n) && t++;
            return t
        }

        function p(e) {
            return "INPUT" === e.nodeName && "radio" === e.type
        }

        function v(e) {
            return "INPUT" === e.nodeName && "checkbox" === e.type
        }

        function d(e) {
            return "SELECT" === e.nodeName && "select-multiple" === e.type
        }

        function y(e) {
            return e.checked
        }

        function m(e) {
            if (p(e)) return !!y(e) && e.value;
            if (v(e)) return !!y(e) && e.value;
            if ("INPUT" === (t = e).nodeName && "file" === t.type) return !(!a.enctype || "multipart/form-data" !== a.enctype) && (n = e, E.FileList && n.files instanceof E.FileList && 0 < e.files.length ? e.files : !(!e.value || "" === e.value) && e.value);
            var n, t, r, u;
            if ("TEXTAREA" === e.nodeName) return !(!e.value || "" === e.value) && e.value;
            if ("SELECT" === (r = e).nodeName && "select-one" === r.type) return e.value && "" !== e.value ? e.value : !(!e.options || !e.options.length || "" === e.options[0].value) && e.options[0].value;
            if (d(e)) {
                if (e.options && 0 < e.options.length) {
                    var i = [];
                    return function (e, n) {
                        if ([].forEach) return [].forEach.call(e, n);
                        var t;
                        for (t = 0; t < e.length; t++) n.call(e, e[t], t)
                    }(e.options, function (e) {
                        e.selected && i.push(e.value)
                    }), l.includeEmptyValuedElements ? i : !!i.length && i
                }
                return !1
            }
            return "BUTTON" === (u = e).nodeName && "submit" === u.type ? e.value && "" !== e.value ? e.value : !(!e.innerText || "" === e.innerText) && e.innerText : void 0 !== e.value && (l.includeEmptyValuedElements ? e.value : "" !== e.value && e.value)
        }

        function h(e, n, t, r) {
            var u, i = n[0];
            if (p(e)) return !1 !== t ? r[i] = t : void 0;
            if (v(e)) {
                if (!1 === t) return;
                if (u = e.name, 1 < Array.prototype.filter.call(o, function (e) {
                    return e.name === u
                }).length) return r[i] || (r[i] = []), r[i].push(t);
                r[i] = t
            }
            if (d(e)) {
                if (!1 === t) return;
                r[i] = t
            }
            return r[i] = t
        }

        function g(e, n, t, r) {
            var u, i, o = n[0];
            return 1 < n.length ? "[]" === o ? (r[c(r)] = {}, g(e, n.splice(1, n.length), t, r[(u = r, i = f(u), "string" == typeof i ? parseInt(i, 10) : 0)])) : (r[o] && 0 < s(r[o]) || (r[o] = {}), g(e, n.splice(1, n.length), t, r[o])) : 1 === n.length ? ("[]" === o ? r[c(r)] = t : h(e, n, t, r), r) : void 0
        }

        return {
            init: function (e) {
                return !(!e || "object" != typeof e || !e[0]) && (n = e[0], void 0 !== e[1] && 0 < s(e[1]) && function (e, n) {
                    var t;
                    for (t in n) n.hasOwnProperty(t) && (e[t] = n[t])
                }(l, e[1]), !!function () {
                    switch (typeof n) {
                        case"string":
                            a = t.getElementById(n);
                            break;
                        case"object":
                            (e = n) && "object" == typeof e && "nodeType" in e && 1 === e.nodeType && (a = n)
                    }
                    var e;
                    return a
                }() && !!(o = a.querySelectorAll("input, textarea, select")).length && function () {
                    var e, n, t, r = 0, u = {};
                    for (r = 0; r < o.length; r++) !(n = o[r]).name || "" === n.name || n.disabled || p(n) && !y(n) || (!1 !== (t = m(n)) || l.includeEmptyValuedElements) && (1 === (e = n.name.match(i)).length && h(n, e, t || "", u), 1 < e.length && g(n, e, t || "", u));
                    return 0 < s(u) && u
                }())
            }
        }
    };
    "function" == typeof define && define.amd ? define(function () {
        return r
    }) : "object" == typeof module && module.exports ? module.exports = r : E.formToObject = r
}(window, document);


/**
 * @module grabbable
 *
 * Version: 1.0.4
 *
 * Copyright 2016 Wolfgang Kurz
 * Released under the MIT license
 * https://github.com/WolfgangKurz/grabbable
 *
 * @desc Grabbable lets you set up divs to be darg and droped and rearranged in an array lik manner
 *  it hooks on to the HTMLElement class so just
 *
 *  @example

 <div class="box grabbable-parent">
 <div class="item">1</div>
 <div class="item">2</div>
 <div class="item">3</div>
 <div class="item">4</div>
 <div class="item">5</div>
 <div class="item">6</div>
 <div class="item">7</div>
 <div class="item">8</div>
 <div class="item">9</div>
 <div class="item">10</div>
 <div class="item">11</div>
 </div>

 <script type="text/javascript">
 "use strict";
 !function(){
	document.querySelector(".grabbable-parent").grabbable();
 }()
 </script>
 *
 */
"use strict";
!function () {
    /**
     * Example of how to style somthing in a js function
     */
    var grabbableStyle = function () {
        var style = document.createElement("style");
        style.type = "text/css";
        style.innerHTML = ".grabbable > * { -webkit-user-select: none; -moz-user-select: none; -ms-user-select: none; user-select: none; cursor: -webkit-grab; cursor: grab } "
            + ".grabbable > .grabbable-dummy {"
            + " border: 1px solid #d4d4d4;"
            + " background: repeating-linear-gradient( -45deg, #fff, #fff 4px, #d4d4d4 4px, #d4d4d4 5px );"
            + "}";
        document.querySelector("body").appendChild(style);
    };
    var callCallback = function (elem) {
        if (document.createEventObject) {
            elem.fireEvent("ondragged");
        } else {
            var evt = document.createEvent("HTMLEvents");
            evt.initEvent("dragged", false, true);
            elem.dispatchEvent(evt);
        }
    };

    var dummy = null, bg = null;
    var createDummy = function () {
        bg = document.createElement("div");
        bg.style.position = "absolute";
        bg.style.width = "1px";
        bg.style.height = "1px";
        bg.style.overflow = "hidden";

        dummy = document.createElement("div");
        dummy.className = "grabbable-dummy";
        dummy.style.position = "relative";
        dummy.addEventListener("drop", function (e) {
            var data = e.dataTransfer.getData("text");
            if (data != "draggable") return;

            e.preventDefault();
            e.stopPropagation();

            while (bg.children.length > 0) {
                var elem = bg.children[0];
                this.parentNode.insertBefore(elem, this);
            }

            dummy.style.display = "none";
            callCallback(dummy.parentNode);
        });

        var x = document.querySelector("body");
        x.appendChild(dummy);
        x.appendChild(bg);
    };
    var updateDummy = function (el) {
        bg.style.left = el.offsetLeft + "px";
        bg.style.top = el.offsetTop + "px";
        dummy.style.width = el.offsetWidth + "px";
        dummy.style.height = el.offsetHeight + "px";

        var style = window.getComputedStyle(el);
        dummy.style.display = style.display;
        dummy.style.margin = style.marginTop + " " + style.marginRight + " " + style.marginBottom + " " + style.marginLeft;
        dummy.style.padding = style.paddingTop + " " + style.paddingRight + " " + style.paddingBottom + " " + style.paddingLeft;
    };
    var initGrabbable = function () {
        grabbableStyle();
        createDummy();
    };

    var prevent = function (e) {
        e.preventDefault();
        e.stopPropagation();
    };
    var allowDrop = function (e) {
        e.preventDefault();

        e.stopPropagation();

        if (this.previousElementSibling != dummy)
            this.parentNode.insertBefore(dummy, this);
        else
            this.parentNode.insertBefore(dummy, this.nextElementSibling);
    }
    var dragOn = function (e) {
        e.dataTransfer.setData("text", "draggable");
    };
    var resetDrop = function (e) {
        var data = e.dataTransfer.getData("text");
        if (data != "draggable") return;

        prevent(e);

        while (bg.children.length > 0) {
            var elem = bg.children[0];
            dummy.parentNode.insertBefore(elem, dummy);
        }
        dummy.style.display = "none";
        callCallback(dummy.parentNode);
    };

    if (document.readyState == "complete") initGrabbable();
    else document.addEventListener("DOMContentLoaded", function () {
        initGrabbable()
    });

    HTMLElement.prototype.grabbable = function () {
        if ((" " + this.className + " ").indexOf(" grabbable ") < 0)
            this.className += " grabbable";

        for (var i = 0; i < this.children.length; i++) {
            var el = this.children[i];
            if (typeof el.draggabled == "undefined") {
                if (el == dummy) continue;

                el.draggable = true;

                el.addEventListener("dragstart", dragOn);
                el.addEventListener("dragover", allowDrop);
                el.addEventListener("drag", function () {
                    if (this.parentNode == bg) return;
                    if (this == dummy) return;

                    updateDummy(this);
                    this.parentNode.insertBefore(dummy, this);
                    bg.appendChild(this);
                });
                el.addEventListener("drop", function (e) {
                    prevent(e);

                    if (document.createEventObject) dummy.fireEvent("ondrop", e);
                    else dummy.dispatchEvent(new DragEvent(e.type, e));
                });
                el.draggabled = true;
            }
        }

        if (typeof document.draggabled == "undefined") {
            document.addEventListener("dragover", prevent);
            document.addEventListener("drop", resetDrop);
            document.draggabled = true;
        }
    };
}()


// https://raw.githubusercontent.com/Bernardo-Castilho/dragdroptouch/master/DragDropTouch.js
var DragDropTouch;
(function (DragDropTouch_1) {
    'use strict';
    /**
     * Object used to hold the data that is being dragged during drag and drop operations.
     *
     * It may hold one or more data items of different types. For more information about
     * drag and drop operations and data transfer objects, see
     * <a href="https://developer.mozilla.org/en-US/docs/Web/API/DataTransfer">HTML Drag and Drop API</a>.
     *
     * This object is created automatically by the @see:DragDropTouch singleton and is
     * accessible through the @see:dataTransfer property of all drag events.
     */
    var DataTransfer = (function () {
        function DataTransfer() {
            this._dropEffect = 'move';
            this._effectAllowed = 'all';
            this._data = {};
        }

        Object.defineProperty(DataTransfer.prototype, "dropEffect", {
            /**
             * Gets or sets the type of drag-and-drop operation currently selected.
             * The value must be 'none',  'copy',  'link', or 'move'.
             */
            get: function () {
                return this._dropEffect;
            },
            set: function (value) {
                this._dropEffect = value;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(DataTransfer.prototype, "effectAllowed", {
            /**
             * Gets or sets the types of operations that are possible.
             * Must be one of 'none', 'copy', 'copyLink', 'copyMove', 'link',
             * 'linkMove', 'move', 'all' or 'uninitialized'.
             */
            get: function () {
                return this._effectAllowed;
            },
            set: function (value) {
                this._effectAllowed = value;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(DataTransfer.prototype, "types", {
            /**
             * Gets an array of strings giving the formats that were set in the @see:dragstart event.
             */
            get: function () {
                return Object.keys(this._data);
            },
            enumerable: true,
            configurable: true
        });
        /**
         * Removes the data associated with a given type.
         *
         * The type argument is optional. If the type is empty or not specified, the data
         * associated with all types is removed. If data for the specified type does not exist,
         * or the data transfer contains no data, this method will have no effect.
         *
         * @param type Type of data to remove.
         */
        DataTransfer.prototype.clearData = function (type) {
            if (type != null) {
                delete this._data[type.toLowerCase()];
            } else {
                this._data = {};
            }
        };
        /**
         * Retrieves the data for a given type, or an empty string if data for that type does
         * not exist or the data transfer contains no data.
         *
         * @param type Type of data to retrieve.
         */
        DataTransfer.prototype.getData = function (type) {
            return this._data[type.toLowerCase()] || '';
        };
        /**
         * Set the data for a given type.
         *
         * For a list of recommended drag types, please see
         * https://developer.mozilla.org/en-US/docs/Web/Guide/HTML/Recommended_Drag_Types.
         *
         * @param type Type of data to add.
         * @param value Data to add.
         */
        DataTransfer.prototype.setData = function (type, value) {
            this._data[type.toLowerCase()] = value;
        };
        /**
         * Set the image to be used for dragging if a custom one is desired.
         *
         * @param img An image element to use as the drag feedback image.
         * @param offsetX The horizontal offset within the image.
         * @param offsetY The vertical offset within the image.
         */
        DataTransfer.prototype.setDragImage = function (img, offsetX, offsetY) {
            var ddt = DragDropTouch._instance;
            ddt._imgCustom = img;
            ddt._imgOffset = {x: offsetX, y: offsetY};
        };
        return DataTransfer;
    }());
    DragDropTouch_1.DataTransfer = DataTransfer;
    /**
     * Defines a class that adds support for touch-based HTML5 drag/drop operations.
     *
     * The @see:DragDropTouch class listens to touch events and raises the
     * appropriate HTML5 drag/drop events as if the events had been caused
     * by mouse actions.
     *
     * The purpose of this class is to enable using existing, standard HTML5
     * drag/drop code on mobile devices running IOS or Android.
     *
     * To use, include the DragDropTouch.js file on the page. The class will
     * automatically start monitoring touch events and will raise the HTML5
     * drag drop events (dragstart, dragenter, dragleave, drop, dragend) which
     * should be handled by the application.
     *
     * For details and examples on HTML drag and drop, see
     * https://developer.mozilla.org/en-US/docs/Web/Guide/HTML/Drag_operations.
     */
    var DragDropTouch = (function () {
        /**
         * Initializes the single instance of the @see:DragDropTouch class.
         */
        function DragDropTouch() {
            this._lastClick = 0;
            // enforce singleton pattern
            if (DragDropTouch._instance) {
                throw 'DragDropTouch instance already created.';
            }
            // detect passive event support
            // https://github.com/Modernizr/Modernizr/issues/1894
            var supportsPassive = false;
            document.addEventListener('test', function () {
            }, {
                get passive() {
                    supportsPassive = true;
                    return true;
                }
            });
            // listen to touch events
            if (navigator.maxTouchPoints) {
                var d = document,
                    ts = this._touchstart.bind(this),
                    tm = this._touchmove.bind(this),
                    te = this._touchend.bind(this),
                    opt = supportsPassive ? {passive: false, capture: false} : false;
                d.addEventListener('touchstart', ts, opt);
                d.addEventListener('touchmove', tm, opt);
                d.addEventListener('touchend', te);
                d.addEventListener('touchcancel', te);
            }
        }

        /**
         * Gets a reference to the @see:DragDropTouch singleton.
         */
        DragDropTouch.getInstance = function () {
            return DragDropTouch._instance;
        };
        // ** event handlers
        DragDropTouch.prototype._touchstart = function (e) {
            var _this = this;
            if (this._shouldHandle(e)) {
                // raise double-click and prevent zooming
                if (Date.now() - this._lastClick < DragDropTouch._DBLCLICK) {
                    if (this._dispatchEvent(e, 'dblclick', e.target)) {
                        e.preventDefault();
                        this._reset();
                        return;
                    }
                }
                // clear all variables
                this._reset();
                // get nearest draggable element
                var src = this._closestDraggable(e.target);
                if (src) {
                    // give caller a chance to handle the hover/move events
                    if (!this._dispatchEvent(e, 'mousemove', e.target) &&
                        !this._dispatchEvent(e, 'mousedown', e.target)) {
                        // get ready to start dragging
                        this._dragSource = src;
                        this._ptDown = this._getPoint(e);
                        this._lastTouch = e;
                        e.preventDefault();
                        // show context menu if the user hasn't started dragging after a while
                        setTimeout(function () {
                            if (_this._dragSource == src && _this._img == null) {
                                if (_this._dispatchEvent(e, 'contextmenu', src)) {
                                    _this._reset();
                                }
                            }
                        }, DragDropTouch._CTXMENU);
                        if (DragDropTouch._ISPRESSHOLDMODE) {
                            this._pressHoldInterval = setTimeout(function () {
                                _this._isDragEnabled = true;
                                _this._touchmove(e);
                            }, DragDropTouch._PRESSHOLDAWAIT);
                        }
                    }
                }
            }
        };
        DragDropTouch.prototype._touchmove = function (e) {
            if (this._shouldCancelPressHoldMove(e)) {
                this._reset();
                return;
            }
            if (this._shouldHandleMove(e) || this._shouldHandlePressHoldMove(e)) {
                // see if target wants to handle move
                var target = this._getTarget(e);
                if (this._dispatchEvent(e, 'mousemove', target)) {
                    this._lastTouch = e;
                    e.preventDefault();
                    return;
                }
                // start dragging
                if (this._dragSource && !this._img && this._shouldStartDragging(e)) {
                    this._dispatchEvent(e, 'dragstart', this._dragSource);
                    this._createImage(e);
                    this._dispatchEvent(e, 'dragenter', target);
                }
                // continue dragging
                if (this._img) {
                    this._lastTouch = e;
                    e.preventDefault(); // prevent scrolling
                    this._dispatchEvent(e, 'drag', this._dragSource);
                    if (target != this._lastTarget) {
                        this._dispatchEvent(this._lastTouch, 'dragleave', this._lastTarget);
                        this._dispatchEvent(e, 'dragenter', target);
                        this._lastTarget = target;
                    }
                    this._moveImage(e);
                    this._isDropZone = this._dispatchEvent(e, 'dragover', target);
                }
            }
        };
        DragDropTouch.prototype._touchend = function (e) {
            if (this._shouldHandle(e)) {
                // see if target wants to handle up
                if (this._dispatchEvent(this._lastTouch, 'mouseup', e.target)) {
                    e.preventDefault();
                    return;
                }
                // user clicked the element but didn't drag, so clear the source and simulate a click
                if (!this._img) {
                    this._dragSource = null;
                    this._dispatchEvent(this._lastTouch, 'click', e.target);
                    this._lastClick = Date.now();
                }
                // finish dragging
                this._destroyImage();
                if (this._dragSource) {
                    if (e.type.indexOf('cancel') < 0 && this._isDropZone) {
                        this._dispatchEvent(this._lastTouch, 'drop', this._lastTarget);
                    }
                    this._dispatchEvent(this._lastTouch, 'dragend', this._dragSource);
                    this._reset();
                }
            }
        };
        // ** utilities
        // ignore events that have been handled or that involve more than one touch
        DragDropTouch.prototype._shouldHandle = function (e) {
            return e &&
                !e.defaultPrevented &&
                e.touches && e.touches.length < 2;
        };

        // use regular condition outside of press & hold mode
        DragDropTouch.prototype._shouldHandleMove = function (e) {
            return !DragDropTouch._ISPRESSHOLDMODE && this._shouldHandle(e);
        };

        // allow to handle moves that involve many touches for press & hold
        DragDropTouch.prototype._shouldHandlePressHoldMove = function (e) {
            return DragDropTouch._ISPRESSHOLDMODE &&
                this._isDragEnabled && e && e.touches && e.touches.length;
        };

        // reset data if user drags without pressing & holding
        DragDropTouch.prototype._shouldCancelPressHoldMove = function (e) {
            return DragDropTouch._ISPRESSHOLDMODE && !this._isDragEnabled &&
                this._getDelta(e) > DragDropTouch._PRESSHOLDMARGIN;
        };

        // start dragging when specified delta is detected
        DragDropTouch.prototype._shouldStartDragging = function (e) {
            var delta = this._getDelta(e);
            return delta > DragDropTouch._THRESHOLD ||
                (DragDropTouch._ISPRESSHOLDMODE && delta >= DragDropTouch._PRESSHOLDTHRESHOLD);
        }

        // clear all members
        DragDropTouch.prototype._reset = function () {
            this._destroyImage();
            this._dragSource = null;
            this._lastTouch = null;
            this._lastTarget = null;
            this._ptDown = null;
            this._isDragEnabled = false;
            this._isDropZone = false;
            this._dataTransfer = new DataTransfer();
            clearInterval(this._pressHoldInterval);
        };
        // get point for a touch event
        DragDropTouch.prototype._getPoint = function (e, page) {
            if (e && e.touches) {
                e = e.touches[0];
            }
            return {x: page ? e.pageX : e.clientX, y: page ? e.pageY : e.clientY};
        };
        // get distance between the current touch event and the first one
        DragDropTouch.prototype._getDelta = function (e) {
            if (DragDropTouch._ISPRESSHOLDMODE && !this._ptDown) {
                return 0;
            }
            var p = this._getPoint(e);
            return Math.abs(p.x - this._ptDown.x) + Math.abs(p.y - this._ptDown.y);
        };
        // get the element at a given touch event
        DragDropTouch.prototype._getTarget = function (e) {
            var pt = this._getPoint(e), el = document.elementFromPoint(pt.x, pt.y);
            while (el && getComputedStyle(el).pointerEvents == 'none') {
                el = el.parentElement;
            }
            return el;
        };
        // create drag image from source element
        DragDropTouch.prototype._createImage = function (e) {
            // just in case...
            if (this._img) {
                this._destroyImage();
            }
            // create drag image from custom element or drag source
            var src = this._imgCustom || this._dragSource;
            this._img = src.cloneNode(true);
            this._copyStyle(src, this._img);
            this._img.style.top = this._img.style.left = '-9999px';
            // if creating from drag source, apply offset and opacity
            if (!this._imgCustom) {
                var rc = src.getBoundingClientRect(), pt = this._getPoint(e);
                this._imgOffset = {x: pt.x - rc.left, y: pt.y - rc.top};
                this._img.style.opacity = DragDropTouch._OPACITY.toString();
            }
            // add image to document
            this._moveImage(e);
            document.body.appendChild(this._img);
        };
        // dispose of drag image element
        DragDropTouch.prototype._destroyImage = function () {
            if (this._img && this._img.parentElement) {
                this._img.parentElement.removeChild(this._img);
            }
            this._img = null;
            this._imgCustom = null;
        };
        // move the drag image element
        DragDropTouch.prototype._moveImage = function (e) {
            var _this = this;
            requestAnimationFrame(function () {
                if (_this._img) {
                    var pt = _this._getPoint(e, true), s = _this._img.style;
                    s.position = 'absolute';
                    s.pointerEvents = 'none';
                    s.zIndex = '999999';
                    s.left = Math.round(pt.x - _this._imgOffset.x) + 'px';
                    s.top = Math.round(pt.y - _this._imgOffset.y) + 'px';
                }
            });
        };
        // copy properties from an object to another
        DragDropTouch.prototype._copyProps = function (dst, src, props) {
            for (var i = 0; i < props.length; i++) {
                var p = props[i];
                dst[p] = src[p];
            }
        };
        DragDropTouch.prototype._copyStyle = function (src, dst) {
            // remove potentially troublesome attributes
            DragDropTouch._rmvAtts.forEach(function (att) {
                dst.removeAttribute(att);
            });
            // copy canvas content
            if (src instanceof HTMLCanvasElement) {
                var cSrc = src, cDst = dst;
                cDst.width = cSrc.width;
                cDst.height = cSrc.height;
                cDst.getContext('2d').drawImage(cSrc, 0, 0);
            }
            // copy style (without transitions)
            var cs = getComputedStyle(src);
            for (var i = 0; i < cs.length; i++) {
                var key = cs[i];
                if (key.indexOf('transition') < 0) {
                    dst.style[key] = cs[key];
                }
            }
            dst.style.pointerEvents = 'none';
            // and repeat for all children
            for (var i = 0; i < src.children.length; i++) {
                this._copyStyle(src.children[i], dst.children[i]);
            }
        };
        DragDropTouch.prototype._dispatchEvent = function (e, type, target) {
            if (e && target) {
                var evt = document.createEvent('Event'), t = e.touches ? e.touches[0] : e;
                evt.initEvent(type, true, true);
                evt.button = 0;
                evt.which = evt.buttons = 1;
                this._copyProps(evt, e, DragDropTouch._kbdProps);
                this._copyProps(evt, t, DragDropTouch._ptProps);
                evt.dataTransfer = this._dataTransfer;
                target.dispatchEvent(evt);
                return evt.defaultPrevented;
            }
            return false;
        };
        // gets an element's closest draggable ancestor
        DragDropTouch.prototype._closestDraggable = function (e) {
            for (; e; e = e.parentElement) {
                if (e.hasAttribute('draggable') && e.draggable) {
                    return e;
                }
            }
            return null;
        };
        return DragDropTouch;
    }());
    /*private*/
    DragDropTouch._instance = new DragDropTouch(); // singleton
    // constants
    DragDropTouch._THRESHOLD = 5; // pixels to move before drag starts
    DragDropTouch._OPACITY = 0.5; // drag image opacity
    DragDropTouch._DBLCLICK = 500; // max ms between clicks in a double click
    DragDropTouch._CTXMENU = 900; // ms to hold before raising 'contextmenu' event
    DragDropTouch._ISPRESSHOLDMODE = false; // decides of press & hold mode presence
    DragDropTouch._PRESSHOLDAWAIT = 400; // ms to wait before press & hold is detected
    DragDropTouch._PRESSHOLDMARGIN = 25; // pixels that finger might shiver while pressing
    DragDropTouch._PRESSHOLDTHRESHOLD = 0; // pixels to move before drag starts
    // copy styles/attributes from drag source to drag image element
    DragDropTouch._rmvAtts = 'id,class,style,draggable'.split(',');
    // synthesize and dispatch an event
    // returns true if the event has been handled (e.preventDefault == true)
    DragDropTouch._kbdProps = 'altKey,ctrlKey,metaKey,shiftKey'.split(',');
    DragDropTouch._ptProps = 'pageX,pageY,clientX,clientY,screenX,screenY,offsetX,offsetY'.split(',');
    DragDropTouch_1.DragDropTouch = DragDropTouch;
})(DragDropTouch || (DragDropTouch = {}));
