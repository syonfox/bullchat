/**
 * Constructs a tinyEditor module for bullchat
 * @requires node_modules/tiny-editor/dist/bundle.js
 * @param opts
 * @returns {{getValue: (function(*): {data: *}), onInit: onInit, el: HTMLDivElement, setValue: setValue, hookChange: simpleInputMod.hookChange, getDefault: (function(*): {data: string})}}
 */
function tinyEditorMod(opts) {
    if (typeof transformToEditor != "function") {
        // transformToEditor
        throw "Error: You need to include tiny-editor/dist/bundle.js ";
    }

    let div = document.createElement('div');
    div.setAttribute('data-tiny-editor', '');//probably not needed
    // document.body.appendChild(div);
    // transformToEditor(div);
    let notTransformed = true;
    return { // the timey editor module

        el: div,
        setValue: (val, el) => {
            if (el) {
                // let oldpos = getCaretPosition(el);
                let oldpos = CaretUtil.getCaretPosition(el);
                if (val.data == el.innerHTML) {
                    console.log("tis my data, returning!");
                    return;
                }
                el.innerHTML = sanitizeHtmlString(val.data);
                //todo correct position if edit happend before
                // setCaret(el, oldpos);
                CaretUtil.setCaretPosition(el, oldpos)
                let newpos = CaretUtil.getCaretPosition(el);

                console.log('old,new caret: ', oldpos, newpos);

                // https://cheatsheetseries.owasp.org/cheatsheets/Cross_Site_Scripting_Prevention_Cheat_Sheet.html
                // el.setHTML(val.data);//https://developer.mozilla.org/en-US/docs/Web/API/Element/setHTML //too new
            }
        },
        getValue: (el) => {
            return {data: el.innerHTML}
        },
        getDefault: (el) => {
            return {data: 'Welcome to Tiny Text Editor'}
        },
        hookChange: (fn, el) => {
            // el.addEventListener('change', fn);
            el.addEventListener('input', fn)
            if (notTransformed && document.contains(el)) {
                transformToEditor(el);
                notTransformed = false;
            }
        },
        onInit: (panel, el, bullchat) => {
            console.log("Init Panel")
        }
    }

}
var tinyEditorFactory = tinyEditorMod; //@deprecated


/**
 * Creates a demo bullchat with a timy editor
 * note how it passes in a user graph so that others cant edit
 * @returns {Promise<BullChat>}
 */
async function demoTinyEditorFactory() {

    let user = await gunAuth.promiseAuth();

    var tinyEditor = tinyEditorMod();
    //with gunauth.user as the gun node we have control of write this is essentially 'diary mode'
    // anyone can still read
    return new BullChat(gunAuth.user, {
        id: 'timyDemo',
        room: 'room1',
        initPanel: true,
        gunAuth: gunAuth,
        jspanelOptions: {
            headerTitle: 'BullChat Tiny Editor (Write Protected)',
            // headerToolbar: '',// disables toolbar
        },
        modules: [tinyEditor, {type: 'textarea'}]
    })

}