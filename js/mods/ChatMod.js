/**
 * This class is a bullchat history module.
 * The main concept of this is that values must also refer to the previous
 * chain nodes sole this can be relized through the bullchat.getLatestSole function
 */
class ChatMod {

    /**
     *  Create a chat module for bullchat
     */
    constructor() {
        //el is the container some people call it the 'root'
        //could extend HTMLElement or something but nah
        this.el = document.createElement('div');
        this.msgs = document.createElement('div');
        this.textarea = document.createElement('textarea');
        this.submit = document.createElement('button');
        this.submit.innerHTML = "Send Message"

        this.el.appendChild(this.msgs);
        this.el.appendChild(this.textarea);
        this.el.appendChild(this.submit);

        /**
         * Walk up the parents
         * @param val
         * @param stepCB
         * @param rootCB
         */
        this.walk = (val, stepCB, rootCB) => {
            let room = this.bc.getRoom()
            if (!val) {
                console.log("No Data Ending")
                return;
            }
            if (stepCB) {
                if (!stepCB(val)) {
                    console.log("Canceling walk cuz stepCB returned true", val);
                    return;
                }
            }
            if (val.parent) {
                console.log("WALKING TO", val.parent)
                room.get(val.parent).once(data => this.walk(data, stepCB, rootCB))
                // console.log(room.get(val.parent))
            } else {
                console.log('ROOT: ', val);
                if (rootCB) rootCB(val)
            }
        }

        /**
         * Adds a message to the dom
         * @param val
         * @returns {boolean}
         */
        this.addMsg = (val) => {
            let room = this.bc.getRoom();

            if (val && val.id && this.msgMap[val.id]) {
                console.log("Skipping already in map", val.id)
                return false;//cancel the walk
            } else {

            }
            console.log("add Msg", val.data);

            let msg = document.createElement('div');
            msg.classList.add('msg');

            if (bs.isRegistered('gun-avatar') && val.pub) {
                msg.innerHTML += `<gun-avatar title="Logged in as ${val.pub}"  size=29 round pub=${val.pub}></gun-avatar>`
            } else {
                msg.innerHTML += '😀'
            }

            msg.innerHTML += `<span>${val.data}</span> 
                    <span style="font-size: small">Time: ${new Date(Date.now()).toTimeString()} |  Parent: ${val.parent} </span>`;


            this.msgMap[val.id] = {msg, val};


            msg.id = val.id;


            let p = this.msgMap[val.parent];
            if (p) {
                p.msg.append(msg);
            } else {
                console.log("No Parent Placing End")
                // this.msgs.appendChild(msg)
                if (this.msgs.firstChild) {
                    this.msgs.firstElementChild.prepend(msg);
                } else {
                    this.msgs.appendChild(msg);
                }
            }
            return true;
        }


        this.setValue = (val, el, bullchat) => {
            // let g = bullchat.gunAuth;
            // g.promiseAuth().then((r)=>{
            //     console.log(r);

            if (val == this.val) {
                console.log('Tis My Data Returning')
                return;
            }

            this.textarea.value = val.data;


            this.oldVal = this.val;
            this.val = val;

            if (!this.msgMap) this.msgMap = {};


            // this.msgs.innerHTML = '';
            // this.msgMap = {}
            this.walk(val, this.addMsg);
        }

        /**
         * Get the curent chat value
         * @param el
         * @param bullchat
         * @returns {*|{date: number, parent, data: string, id: *}}
         */
        this.getValue = (el, bullchat) => {
            if (!this.val) console.log("WTF no val here??");
            return this.val;
        }

        this.getDefault = () => {
            return 'Hello World';
        }

        this.hookChange = (fn, el, bc) => {

            if(this.submit.onclick && this._change) {
                console.warn("Already Hooked Up, skipping")
                return;
            }
            this._change = fn;
            this.bc = bc;

            let room = bc.getRoom();

            room.map().on((v) => {
                if (!this.msgMap) this.msgMap = {};
                if (v && v.id && !this.msgMap[v.id]) {
                    this.addMsg(v);
                }
            })


            // room.get('head').once(val => {
            //     this.walk(val, this.addMsg);
            // });
            let head =  room.get('head')
            head.on(val=>{
                console.log("Head Changed", val.id);
                this.addMsg(val);
            })
            this.submit.onclick = () => {

                this.oldVal = this.val;
                this.val = {
                    data: this.textarea.value,
                    id: bs.uuidv4(),
                    date: Date.now(),
                    parent: this.oldVal.id
                }
                try {
                    this.val.pub = gunAuth.user.is.pub
                } catch {
                    console.error("Couldn't Get User Pub You Logged In???");
                }
                let room = this.bc.getRoom();
                room.get(this.val.id).put(this.val);
               head.put(this.val);



                this._change();
            }


        }

    }
}


//asumes gun and gunAuth are initialized
var chatRoom

function openChat() {

    if (chatRoom instanceof BullChat) {
        chatRoom.initPanel();
        return;
    }

    chatRoom = new BullChat(gun, {
        title: 'Chat Room',
        room: "test5",
        history: true,
        initPanel: true,
        modules: [
            new ChatMod()
        ]

    })

}