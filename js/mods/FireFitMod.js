// https://developer.mozilla.org/en-US/docs/Web/API/HTMLElement
// https://developer.mozilla.org/en-US/docs/Web/Web_Components/Using_custom_elements
/**
 * So this is a system for extending modulesMust be an instance of HTMLElement or provide an element in its 'el' property
 *  and YOU MUST IMPLEMENT getValue() setValue(), getDefault(), hootChange()  functions that complement each other you are responsible for the values to work
 *  ie myMod.setValue(myMod.getValue()) should work and same for getDefault() ... warning JSON.stringified so no cycles.
 */


class Resource {

    static findBy(key, val) {
        return Resource._instances.find(i => i[key] == i[val]);
    }

    static findById(id) {
        return Resource.findBy('id', id);
    }

    static findByName(name) {
        return Resource.findBy('name', name);
    }

    static findByLocation(location) {
        return Resource.findBy('location', location);
    }

    /**
     * @param {String} [config.id=uuid4] - normally generated if you wish to specify it must be unique
     * @param config.name
     * @param config.location
     */
    constructor(config, options) {

        if (!config.name) throw "You must provide resource name";
        if (!config.location) console.warn("No location provided");

        this.id = config.id || uuidv4();
        this.name = config.name;
        this.location = config.location;

        Resource._instances.push(this);
    }

    toString() {
        return JSON.stringify({
            name: this.name,
            location: this.location
        })
    }


    inspect() {
    }// for quality poruposes
    check() {
    }// for invintory pourposes

}

Resource._instances = []


class Equipment extends Resource {

    constructor(config) {
        super(config);

        this.props
        this.tag = config.tag || null;
    }


    setTag(tag) {
        this.tag = tag;
    }

    tagOut() {
        this.setTag('out_of_service');
    }

}

/**
 * Turnout Gear -> Equipment -> Resource
 * pants
 * coat
 * gloves
 * helmet
 * mask
 * boots
 * balaclava
 * ... (slots)
 * accountability_tags
 * decals
 * radio
 * webbing
 * seat_belt_tool
 * flashlight
 * props:
 *  date_of_manufacture
 *  size: 'human string'
 *  measurements: {
 *      measurement: val
 *      ...
 *  }
 *  color
 *  manufacturer
 *  serial_num
 *  last_inspected*
 *  fit_test*
 *  washed {set of dates}
 *  batteries_changed
 *  status
 *
 *
 */


class Unit extends Resource {


    /**
     *
     * @param {Object || Resource} config.resource
     * @param config.resource.id
     * @param config.resource.name
     * @param config..location
     * @param config.call_sign
     * @param {Array<Resources>} config.units
     * @param config.assignment
     * @param config.unit_type
     * @param options - optional options
     */
    constructor(config, options) {
        super(config.resource);

        // if(config.resource instanceof Resource) {
        // } else if(config.resource && config.resource.id) {
        //     //search for existing resorce
        // } else {
        //     //try and create resorce
        // }
        return
    }

    setAssignmet(who, what, where) {

        //who = array of resources
        //what = callsign, unit_name, assignment

    }
}

/**
 * For keeping track of stuff in slots on firefighters
 */
class FireSlotMod {
    constructor(fire_num, name, options) {


        // write element functionality in here
        // this.attachShadow({mode: 'open'});
        this.el = document.createElement('div');


        this._data = {
            id: fire_num,
            name: name,
            slots: []
        };

        let ppeTypes = {}


        this.initModFuncs();

        this._slots = [];

        //helmet
        this.addSlot(0,
            'equipment',
            'anything you want',
            {
                serial_number: 'string', //SN20019970502
                manufacturer: 'string', //honda
                manufacture_date: 'string', //dec 1998 // user pg timestamp format
                model: 'string',//R220
                color: 'string',//null
                size: 'string',//small
                identifier: 'string', //honda1
                location: 'string', //engine1
                type: 'string', //portable pump
                tag: 'string', //out of service
                checked_by: 'reference'//gun.get('@bob').#
            }
        )


        this.addSlot(1,
            'helmet',
            'A ppe helmet for a firefighter',
            {
                style: 'string',
                manufacturer: 'string',
                manufacture_date: 'string',
                model: 'string',
                color: 'string',
                light: 'string',//does helmet have integrated light
                // accountability_tag: 'reference',
                decals: 'reference'
            }
        )

        this.addSlot(2,
            'balaclava',
            'A neck covering for added fire protection and comfort',
            {
                manufacturer: 'string',
                manufacture_date: 'string',
                model: 'string',
                size: 'string'
            }
        )

        this.addSlot(3,
            'boots',
            'Turnout boots',
            {
                manufacturer: 'string',
                manufacture_date: 'string',
                model: 'string',
                size: 'string'
            }
        )
        this.addSlot(4,
            'decals',
            'Helmet markings / stickers',
            {
                // valid: 'bool',
                last_updated: 'date',
                rankrole: 'string',
                qualification: 'string', // exterior, interior, full service, rookie
                callsign: 'string',
                junior: 'bool',
                probationary: 'bool',
                identifier: 'string',//RANK QUALIFICATION NUMBER AGE STATUS
                size: 'string'
            }
        )

        this.addSlot(5,
            'accountability_tags',
            '',
            {
                // valid: 'bool',
                tag1: 'reference',
                tag2: 'reference'
            }
        )

        this.addSlot(6,
            'radio',
            'Communications device',
            {
                manufacturer: 'string',
                manufacture_date: 'string',
                model: 'string',
                size: 'string',
                batteries: 'string',
                battery_change_date: 'date'
            }
        )
        this.addSlot(7,
            'flashlight',
            'Personal flashlight',
            {
                manufacturer: 'string',
                manufacture_date: 'string',
                model: 'string',
                size: 'string',
                batteries: 'string',
                battery_change_date: 'date'
            }
        )


        // this.addSlot(1, 'Scba Mask', 'Subamasks are requiers to fit due to artical #4323')

        return this;
    }

    getId() {
        return this._data.id;
    }

    addSlot(id, name, desc, props) {
        let s = {id, name, desc, props}
        this._slots.push(s);

        // let root = this.shadowRoot

        let div = this.domCreateSlot(id);

        s.div = div;

        /*        let div = document.createElement('div');
                div.classList.add('fire-fit');
                div.innerHTML = `
                <span name='name' >${name}</span>
                <input list="${id}size" placeholder="what size">
                <datalist id="${id}size">
                    <option value='small'>
                    <option value="medium">
                    <option value="large">
                </datalist>

                <textarea rows="3" placeholder="how did it fit"></textarea>
                <span>Do you want fires with that</span><input type='checkbox'>
                `*/

        this.el.appendChild(div);

    }

    setVisibleSlots(ids) {
        this._slots.forEach(slot => {
            if (ids.includes(slot.id)) {
                slot.div.classList.toggle('hidden', false);
            } else {
                slot.div.classList.toggle('hidden', true);
            }

        })
    }

    domCreateProp(name, type) {

        let div = document.createElement('div');
        div.classList.add('fire-fit-slot-prop');

        let label = document.createElement('span');
        // label.innerText = bs.niceName(name);
        label.innerText = name;

        let input = document.createElement('input');
        input.name = name;

        switch (type) {
            case "string":
                //string specific input options
                input.type = 'text';
                break;
            case "bool":
                input.type = 'checkbox';
                break;
            case'reference':
            // input.addEventListener('change', (e)=>{
            //     //validate
            // })
            // break;
            default:
                console.warn("UNKNOWN SLOT PROP TYPE: ", type);
                input.type = type;

        }

        div.appendChild(label);
        div.appendChild(input);
        return div;
    }

    domCreateSlot(id) {
        let slot = this._slots.find(s => s.id === id);

        let div = document.createElement('div');
        div.classList.add('fire-fit-slot');
        div.style.padding = '1em';
        let header = document.createElement('div');
        header.innerHTML = `
        <h1>${this._data.name} - ${slot.name}</h1>
        <p>${slot.desc}</p>
        `
        let body = document.createElement('div');


        Object.keys(slot.props).forEach(key => {
                let propDiv = this.domCreateProp(key, slot.props[key]);
                body.appendChild(propDiv);
            }
        )

        div.appendChild(header);
        div.appendChild(body);
        return div;
    }

    delSlot(id) {
        //todo
    }


    //if you need "this" to remain unchanged use arrow functions.
    // and call from constructor
    initModFuncs() {
        this.setValue = (data, el, bullchat) => {
            //called when remote change happens... sets value.
            //this should also be the bullchat
            // this.querySelector('input').value = data
            let slotData = data.slotData;
            this._slots.forEach(slot => {
                slot.div.querySelectorAll('input').forEach(input => {
                    input.value = slotData[slot.id][input.name];
                })
            })
            return
        }


        this.getValue = (el, bullchat) => {//arrow functions do not modify 'this'
            //this :== bullchatmod
            //do whatever you need to do to get and return a value

            let slotData = {};
            this._slots.forEach(slot => {
                slotData[slot.id] = {};
                slot.div.querySelectorAll('input').forEach(input => {
                      slotData[slot.id][input.name] = input.value;
                })
            })

            return {
                id: this.getId(),
                slotData: slotData
            };
        }
        this.getDefault = this.getValue;
        // this.getDefault = (el, bullchat) => {//arrow functions do not modify 'this'
        //     //this :== bullchatmod
        //     //do whatever you need to do to get and return a value
        //
        //
        //     return {
        //         id: this.getId(),
        //         slotData: slotData
        //     };
        //     return 'trivial';
        // }
        this.hookChange = (fn, el, bullchat) => {//arrow functions do not modify 'this'

            this._localChange = fn;
            console.log("Hooking Change", this.getId(), el);
            el.addEventListener('change', e=>{
                console.log("WOO CALED ME");

                fn();

            });

            // this._slots.forEach(slot => {
            //     slot.div.querySelectorAll('input').forEach(input => {
            //         input.onchange = fn;
            //     })
            // })
            //this :== bullchatmod
            //do whatever you need to do to get and return a value
            // return 'trivial';
        }
    }


    connectedCallback() {
        console.log('Custom square element added to page.');

        this.updateStyle(this);
    }

    disconnectedCallback() {
        console.log('Custom square element removed from page.');
    }

    adoptedCallback() {
        console.log('Custom square element moved to new page.');
    }

    // Note that to get the attributeChangedCallback() callback to fire when an attribute changes, you have to observe
    // the attributes. This is done by specifying a static get observedAttributes() method inside custom element class
    // - this should return  an array containing the names of the attributes you want to observe:
    static get observedAttributes() {
        return ['c', 'l'];
    }

    attributeChangedCallback(name, oldValue, newValue) {
        console.log('Custom square element attributes changed.');
        this.updateStyle(this);
    }

    updateStyle() {
        const shadow = this.shadowRoot;
        //wow much css this is cool official hack
        this.style.textContent = `
        div
        {
            width: ${elem.getAttribute('l')}px;
            height: ${elem.getAttribute('l')}px;
            background - color
        : ${elem.getAttribute('c')}
            ;
        }
        `;
    }


    //above is sample of what a webcomponent can do

    //bellow are Bullchat functions;


}

//or you can set it up later

BullChatModule.prototype.getDefault = function (el, bullchat) {//normal functions 'this' is the calling context
    //this is bullchat
}//.bind(youValForThis) // you can also bind any val you want to this when creating a function (only regular functions not arrow)


customElements.define('fire-fit', FireSlotMod);
