
/*!
* Automatically expand a textarea as the user types
* (c) 2021 Chris Ferdinandi, MIT License, https://gomakethings.com
* @param  {Node} field The textarea
*/
function autoExpand(field) {
    // Reset field height
    field.style.height = "inherit";

    // Get the computed styles for the element
    let computed = window.getComputedStyle(field);

    // Calculate the height
    let height =
    parseFloat(computed.paddingTop) +
    field.scrollHeight +
    parseFloat(computed.paddingBottom);

    field.style.height = height + "px";
}

//make all text areas expand as you type
document.addEventListener(
        "input",
        function (event) {
            if (event.target.tagName.toLowerCase() !== "textarea") {
                return;
            }
            autoExpand(event.target);
            },
        false
        );

let input = document.getElementById("textarea");
let richEditor = document.getElementById("richEditor");
let richEditorParent = document.getElementById("richEditorParent");

let output = document.getElementById("output");

function setup(ein, eout, erich, erichParent) {
    let state = "plain";

    let self = {
        ein,
        eout,
        erich,erichParent,
        mode: "plain",
        convert(e) {
            switch (self.mode) {
                case "plain": {
                    self.eout.innerText = `${self.ein.value}`;
                    break;
                }
            case "md": {
                self.eout.innerHTML = markdown(self.ein.value);

                break;
            }
            case "html": {
                self.eout.innerHTML = bs.sanitizeHtmlString(self.ein.value);

                break;
            }
            case "rich": {
                self.eout.innerHTML = self.erich.innerHTML;
                break;
            }
            }
        },

        setMode(m) {
            if (m == self.mode) return;
            //
            if (m == "rich") { // if new mode is rich
                self.ein.style.display = "none";
                self.eout.style.display = 'none';
                self.erich.parentElement.style.display = "unset";
                self.erich.innerHTML = self.eout.innerHTML;

            } else if(self.mode == 'rich') { // if we are switching off of rich
                self.ein.style.display = "unset";
                self.eout.style.display = 'unset';
                self.ein.value = self.erich.innerHTML;
                console.log('fooasdasd');
                self.erichParent.style.display = "none";

            }

            self.mode = m;
            console.log("Mode Set: ", self.mode);
            self.convert();
            },

        switchToPlain() {
            self.setMode("plain");
            },
        switchToMD() {
            self.setMode("md");
            },
        switchToRich() {
            self.setMode("rich");

            // let text =
            // window.__tinyEditor.transformToEditor(ein)
        },
        switchToHTML() {
            self.setMode("html");
        }
    };

    //set us up to convert the preview on change
    ein.addEventListener("input", self.convert);
    erich.addEventListener("input", self.convert);

    return self;
}
var box = setup(input, output, richEditor, richEditorParent);

let plainBtn = document.querySelector(".ezBoxBtn[name=plain]");
let mdBtn = document.querySelector(".ezBoxBtn[name=md]");
let richBtn = document.querySelector(".ezBoxBtn[name=rich]");
let htmlBtn = document.querySelector(".ezBoxBtn[name=html]");

plainBtn.onclick = box.switchToPlain;
mdBtn.onclick = box.switchToMD;
htmlBtn.onclick = box.switchToHTML;
richBtn.onclick = box.switchToRich;
/**
* drawdown.js
* (c) Adam Leggett
* https://adamvleggett.github.io/drawdown/
These are the supported features:

Block quotes > qote
Code blocks ```
Links [txt](link)
Images ![]()
Headings
Lists (including lettered lists)
Bold
Italic
Strikethrough
Monospace
Subscript
Horizontal rule
Tables
*/

function markdown(src) {
    //todo test and comment each of these
    var rx_lt = /</g;
    var rx_gt = />/g;
    var rx_space = /\t|\r|\uf8ff/g;
    var rx_escape = /\\([\\\|`*_{}\[\]()#+\-~])/g;
    var rx_hr = /^([*\-=_] *){3,}$/gm;
    var rx_blockquote = /\n *&gt; *([^]*?)(?=(\n|$){2})/g;
    var rx_list = /\n( *)(?:[*\-+]|((\d+)|([a-z])|[A-Z])[.)]) +([^]*?)(?=(\n|$){2})/g;
    var rx_listjoin = /<\/(ol|ul)>\n\n<\1>/g;
    var rx_highlight = /(^|[^A-Za-z\d\\])(([*_])|(~)|(\^)|(--)|(\+\+)|`)(\2?)([^<]*?)\2\8(?!\2)(?=\W|_|$)/g;
    var rx_code = /\n((```|~~~).*\n?([^]*?)\n?\2|((    .*?\n)+))/g;
    var rx_link = /((!?)\[(.*?)\]\((.*?)( ".*")?\)|\\([\\`*_{}\[\]()#+\-.!~]))/g;
    var rx_table = /\n(( *\|.*?\| *\n)+)/g;
    var rx_thead = /^.*\n( *\|( *\:?-+\:?-+\:? *\|)* *\n|)/;
    var rx_row = /.*\n/g;
    var rx_cell = /\||(.*?[^\\])\|/g;
    var rx_heading = /(?=^|>|\n)([>\s]*?)(#{1,6}) (.*?)( #*)? *(?=\n|$)/g;
    var rx_para = /(?=^|>|\n)\s*\n+([^<]+?)\n+\s*(?=\n|<|$)/g;
    var rx_stash = /-\d+\uf8ff/g;

    function replace(rex, fn) {
        src = src.replace(rex, fn);
    }

    function element(tag, content) {
        return "<" + tag + ">" + content + "</" + tag + ">";
    }

    function blockquote(src) {
        src = src || "";
        return src.replace(rx_blockquote, function (all, content) {
            return element(
                    "blockquote",
                    blockquote(highlight(content.replace(/^ *&gt; */gm, "")))
                    );
        });
    }

    function list(src) {
        return src.replace(rx_list, function (all, ind, ol, num, low, content) {
            var entry = element(
                    "li",
                    highlight(
                            content
                            .split(
                                    RegExp("\n ?" + ind + "(?:(?:\\d+|[a-zA-Z])[.)]|[*\\-+]) +", "g")
                                    )
                            .map(list)
                            .join("</li><li>")
                            )
                            );

            return (
                    "\n" +
                    (ol
                    ? '<ol start="' +
                    (num
                    ? ol + '">'
                    : parseInt(ol, 36) -
                    9 +
                    '" style="list-style-type:' +
                    (low ? "low" : "upp") +
                    'er-alpha">') +
                    entry +
                    "</ol>"
                    : element("ul", entry))
                    );
        });
    }

    function highlight(src) {
        return src.replace(
                rx_highlight,
                function (all, _, p1, emp, sub, sup, small, big, p2, content) {
                    return (
                            _ +
                            element(
                                    emp // if emp
                                        ? p2 // if p2 + emp
                                            ? "strong"//else
                                            : "em"
                                        : sub // not emp if sub
                                            ? p2  //if p2
                                                ? "s" // ???
                                                : "sub" // else sub
                                        : sup // else if sup
                                            ? "sup"
                                        : small // else if small
                                            ? "small"
                                        : big // else if big
                                            ? "big"
                                        : "code" , // finaly make it code
                                    highlight(content)
                                    )
                                    );
                }
                );
    }

    function unesc(str) {
        return str.replace(rx_escape, "$1");
    }

    var stash = [];
    var si = 0;

    src = "\n" + src + "\n";

    replace(rx_lt, "&lt;");
    replace(rx_gt, "&gt;");
    replace(rx_space, "  ");

    // blockquote
    src = blockquote(src);

    // horizontal rule
    replace(rx_hr, "<hr/>");

    // list
    src = list(src);
    replace(rx_listjoin, "");

    // code
    replace(rx_code, function (all, p1, p2, p3, p4) {
        // if (typeof p4 !== "string") {
        //   console.error("Empty code Tag");
        //   return "";
        // }

        stash[--si] = element(
                "pre",
                element("code", p3 || p4.replace(/^    /gm, ""))
                );
        return si + "\uf8ff";
    });

    // link or image
    replace(rx_link, function (all, p1, p2, p3, p4, p5, p6) {
        stash[--si] = p4
        ? p2
        ? '<img src="' + p4 + '" alt="' + p3 + '"/>'
        : '<a href="' + p4 + '">' + unesc(highlight(p3)) + "</a>"
        : p6;
        return si + "\uf8ff";
    });

    // table
    replace(rx_table, function (all, table) {
        var sep = table.match(rx_thead)[1];
        return (
                "\n" +
                element(
                        "table",
                        table.replace(rx_row, function (row, ri) {
                            return row == sep
                            ? ""
                            : element(
                                    "tr",
                                    row.replace(rx_cell, function (all, cell, ci) {
                                        return ci
                                        ? element(
                                                sep && !ri ? "th" : "td",
                                                unesc(highlight(cell || ""))
                                                )
                                        : "";
                                    })
                                    );
                        })
                        )
                        );
    });

    // heading
    replace(rx_heading, function (all, _, p1, p2) {
        return _ + element("h" + p1.length, unesc(highlight(p2)));
    });

    // paragraph
    replace(rx_para, function (all, content) {
        return element("p", unesc(highlight(content)));
    });

    // stash
    replace(rx_stash, function (all) {
        return stash[parseInt(all)];
    });

    return src.trim();
}
