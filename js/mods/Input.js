


/**
 * Utilit function to document.createElement and append attrabutes to it
 * @param tag - the tag name
 * @param attrs - object of key: value atrubues for tag
 * @param events - NOTE NOT RESPONSIBLE FOR ADDING LISTENERS BUT WILL SHOVE THE FUNCTIONS INTO this.events.
 * @return {*}
 * @constructor
 */
function BuildDom(tag, attrs, events) {

    let el = document.createElement(tag);

    Object.entries(attrs).forEach(e => {
        let key = e[0];
        let val = e[1];
        el.setAttribute(key, val)
    })

    el.events = events;

    el.el = el;
    this.el = el;
    return el;

}

/**
 * https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input
 * @param atters
 * @constructor
 * @example
 * BuildInput({
 *     type: number,
 *     required: true,
 *     placeholder: 'type here :)',
 *     onchange: "(e)=>{console.log(e.target.value)}"
 * })
 */
function BuildInput(atters) {
    atters = atters || {};
    let defaults = {
        type: 'text',
    }

    let opts = Object.assign({}, defaults, atters)
    return BuildDom('input', opts);
}



/**
 * A bit of an un-orthodox module to define each html input type
 * also adds label attribute which accepts a string for content or a label object
 */

let jslabel = (attributes) => {

    return BuildDom('label', attributes)
}

/**
 * <div "name=name+'container'"><label for="input[name=${name}]">${niceName(name) | label}</label>
 *    <input name=${name}, ...attributes>
 * </div>

/**
 * Factory for jsinput layout
 * Note thet the type atter is overriden by inputType as type refers to 'input' or 'draw' etc in my schema ie the name of the factory function
 * also not the unconventional layout and label labels are allowed. they weill be added as attrbutes to the input but the
 * extra html nodes will be made acordingly
 *
 * @param options.layout - row, col, wrap for flex things or custom css class String
 * @param options.title - the tooltip
 * @param options.type - input type
 * @param options.HTMLatter - the attrubutes are added to the input tag.
 * @example
 * let nameInput = jsinput('name', {type:'text' , placeholder:'foo'};
 * nameInput.el.addEventListener('click', console.log);
 */
function bsinput(options, events) {

    options = options || {};
    let name = options.name || 'input';

    let nameSelector = `div[name=${name}]`;
    let divSelector = 'div' + nameSelector;
    let labelSelector = divSelector + ' > label';
    let inputSelector = 'inout' + nameSelector;


        //so we want to suport both the old prompt standard and the new one.
    options.type = options.inputType || options.type || 'text';
    options.title = options.title || options.tooltip || "Enter A Value";
    options.placehoder = options.placeholder || "Type Here";
    // {
    //     type: options.inputType || options.type,
    //         title: options.title || options.tooltip || "Enter A Value",
    //     placeholder: options.placeholder || "Type Here",
    // ...options
    // }


    let layoutClass = ''
    switch (options.layout) {
        case "row": {
            layoutClass = 'flex-row';
            break;
        }
        case "col": {
            layoutClass = 'flex-row';
            break;
        }
        case "":
        case "wrap": {
            layoutClass = 'flex-wrap';
            break;
        }
        default: {
            layoutClass = options.layout;
            break;
        }
    }

    let id = bs.uuidv4();
    //build container <div calss=layout><label/><input/></div>
    let el = BuildDom('div', {
        class: layoutClass,
        id: 'input_parent_' + id
    })

//build label
    let label = BuildDom('label', {
        for: '#' + id,
    })
    let labelText = options.label || options.niceName || bs.niceName(options.name) || bs.niceName(options.key)
    label.innerText = labelText

    if (options.required) label.innerText += '*';
    label.innerText += ' : ';

    //build input
    options.id = id
    let input = BuildInput(options);


    el.appendChild(label)
    el.appendChild(input)
    el._input = input;
    el._label = label;

    el.el = el;

    el.getValue = () => {
        return el._input.value;
    }
    el.setValue = (v) => {
        el._input.value = v;
    }
    el.getDefault = () => {
        return el.default || "Input Value";
    }
    //called after added to dom.
    el.hookChange = (fn) => {
        // el.onchange = fn; //todo maybe save for later if dom is destroyed and created.
        el._input.addEventlistener('change', fn)
        el._fn = fn;
        // return el.default || "Input Value";
    }

    return el;
}
