/**
 * Simple buckchat module things get much more advanced if you wish you can define it in almost any way you like
 * Classes arrow functions etc.
 * @param key - a label
 * @param val - default val
 * @returns {{}}
 */
function kvmFactory(key, val) {
    let kvm = {};//new object

    //init dom
    kvm.el = document.createElement('div');
    kvm.el.classList.add('kvm-class');
    kvm.el.innerHTML = key + ' <input><button>Submit</button>';
    kvm.input = kvm.el.querySelector('input');
    kvm.button = kvm.el.querySelector('button');
    kvm.button.style.background = 'pink';

    //define bullchat functions Note they all pass
    kvm.getValue = function (el ,bullchat) {
        return kvm.input.value;
    }
    kvm.setValue = function (val, el, bullchat) {
        kvm.input.value = val;
    }
    kvm.getDefault = function (el, bullchat) {
        return val;
    }
    kvm.hookChange = function (fn, el, bullchat) {
        kvm.input.addEventListener('input', fn);
        // kvm.button.addEventListener('click', fn);
    }

    return kvm;
}

let kvm = kvmFactory("Simple Input", "Aloha!");

export default {'kvmFactory': kvmFactory, kvm};

