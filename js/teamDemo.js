//Initialize gun
var gun = new Gun({
    // peers: ['http://bullchat.syon.ca:8585/gun'],
    peers: ['https://bullchat.syon.ca/gun'],
});


function uuidv4() {
    return ([1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, c =>
        (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
    );
}


var typeNumber = 4;
var errorCorrectionLevel = 'L';
var qr = qrcode(typeNumber, errorCorrectionLevel);

let searchParams = new URLSearchParams(window.location.search)
let qrRoom = searchParams.get('r');

if (!qrRoom) qrRoom = uuidv4();

const url = window.location.origin + window.location.pathname + '?r=' + qrRoom;

qr.addData(url);
// qr.make();
document.getElementById('qrCode').innerHTML = qr.createImgTag();
let ff = new FireFitMod('2-301', 'Lindsay, Devin')
document.body.appendChild(ff.el);

//webrtc img capture
(function () {
    // The width and height of the captured photo. We will set the
    // width to the value defined here, but the height will be
    // calculated based on the aspect ratio of the input stream.

    var width = 320;    // We will scale the photo width to this
    var height = 0;     // This will be computed based on the input stream

    // |streaming| indicates whether or not we're currently streaming
    // video from the camera. Obviously, we start at false.

    var streaming = false;

    // The various HTML elements we need to configure or control. These
    // will be set by the startup() function.

    var video = null;
    var canvas = null;
    var photo = null;
    var startbutton = null;

    function startup() {
        video = document.getElementById('video');
        canvas = document.getElementById('canvas');
        photo = document.getElementById('photo');
        startbutton = document.getElementById('startbutton');

        navigator.mediaDevices.getUserMedia({video: true, audio: false})
            .then(function (stream) {
                video.srcObject = stream;
                video.play();
            })
            .catch(function (err) {
                console.log("An error occurred: " + err);
            });

        video.addEventListener('canplay', function (ev) {
            if (!streaming) {
                height = video.videoHeight / (video.videoWidth / width);

                // Firefox currently has a bug where the height can't be read from
                // the video, so we will make assumptions if this happens.

                if (isNaN(height)) {
                    height = width / (4 / 3);
                }

                video.setAttribute('width', width);
                video.setAttribute('height', height);
                canvas.setAttribute('width', width);
                canvas.setAttribute('height', height);
                streaming = true;
            }
        }, false);

        startbutton.addEventListener('click', function (ev) {
            takepicture();
            ev.preventDefault();
        }, false);

        clearphoto();
    }

    // Fill the photo with an indication that none has been
    // captured.

    function clearphoto() {
        var context = canvas.getContext('2d');
        context.fillStyle = "#AAA";
        context.fillRect(0, 0, canvas.width, canvas.height);

        var data = canvas.toDataURL('image/png');
        photo.setAttribute('src', data);
    }

    // Capture a photo by fetching the current contents of the video
    // and drawing it into a canvas, then converting that to a PNG
    // format data URL. By drawing it on an offscreen canvas and then
    // drawing that to the screen, we can change its size and/or apply
    // other changes before drawing it.

    function takepicture() {
        var context = canvas.getContext('2d');
        if (width && height) {
            canvas.width = width;
            canvas.height = height;
            context.drawImage(video, 0, 0, width, height);

            var data = canvas.toDataURL('image/png');
//	document.querySelector('#asd').innerHTML = data;
//	mySimpleMod.el.value = data;
            foto.image.setAttribute('src', data);
            foto.taken();
            photo.setAttribute('src', data);
        } else {
            clearphoto();
        }
    }

    // Set up our event listener to run the startup process
    // once loading is complete.
    window.addEventListener('load', startup, false);
})();


if (navigator.share) {
    navigator.share({
        title: 'BullChat Room',
        text: 'Check out my bullchat room',
        url: url,
    })
        .then(() => console.log('Successful share'))
        .catch((error) => console.log('Error sharing', error));
}


let clipLink = document.getElementById('clipLink')
clipLink.value = url;
let clipboard  = new ClipboardJS('#clipButton');
clipboard.on('success', function(e) {
    console.info('Action:', e.action);
    console.info('Text:', e.text);
    console.info('Trigger:', e.trigger);
    let st =  document.getElementById('clipStatus');

        st.innerText = "Copied!";
    bs.animate(st, 'fade-hidden',0, 1000, 'fade-hidden', );
    e.clearSelection();
});
clipboard.on('error', function(e) {
    console.error('Action:', e.action);
    console.error('Trigger:', e.trigger);
         document.getElementById('clipStatus').innerText = "Failed!"
});

// let bullet = new Bullet(gun);
// var gunauth = new GunAuth();


// gun = gun.get('bullchat');

ArrayInput.hackyAnimate = false;

let mySimpleMod = {
    el: document.createElement('input'),


    setValue: (val, el, bullchat) => {
        el.value = val;
    },
    getValue: (el, bullchat) => {
        return el.value;
    },
    getDefault: (el, bullchat) => {
        //      return "yolo " + bullchat.getQuote();
        return '';
    },
    hookChange: (fn, el, bullchat) => {
        el.addEventListener('input', fn);
    }

}

function kvmFactory(key, val) {
    let kvm = {};

    kvm.el = document.createElement('div');
    kvm.el.classList.add('kvm-class');


    kvm.el.innerHTML = key + ' <input><button>Submit</button>';

    kvm.input = kvm.el.querySelector('input');
    kvm.button = kvm.el.querySelector('button');
    kvm.button.style.background = 'pink';
    kvm.getValue = function () {
        return kvm.input.value;
    }
    kvm.setValue = function (val) {
        kvm.input.value = val;
    }
    kvm.getDefault = function () {
        return val;
    }
    kvm.hookChange = function (fn) {
        kvm.input.addEventListener('input', fn);
        // kvm.button.addEventListener('click', fn);
    }

    return kvm;
}

let kvm = kvmFactory("foo", "bar");


function photoFactory(key, val) {
    let photo = {};


    photo.el = document.createElement('div');
    photo.el.classList.add('photo-class');
    photo.el.innerHTML = key + ' <img >';

    photo.image = photo.el.querySelector('img');
    photo.getValue = function () {
        return photo.image.src;
    }
    photo.setValue = function (val) {
        photo.image.src = val;
    }
    photo.getDefault = function () {
        return val;
    }
    photo.hookChange = function (fn) { //bullchat._localChange();
        photo.taken = fn;
        //      photo.input.addEventListener('input', fn);
        // photo.button.addEventListener('click', fn);
    }

    return photo;
}

let foto = photoFactory("photo1", '');


let fireRich = new BullChat(gun, {
    id: 'fireRich',
    container: '.bullchat-rich',
    room: qrRoom,
    modules: [
        foto,
        {
            type: 'textarea'
        },
        {
            type: 'textarea'
        },
        mySimpleMod,
        kvm
    ]
})

//
// let contentDiv = document.getElementById('scratch_content');
// let roomInput = document.getElementById('scratch_room');
// roomInput.value = localStorage.getItem('scratch_room') || 'bullchat3'
//
// let scratchInput = document.getElementById('scratch_body');
// let nameInput = document.getElementById('name_scratch');
// nameInput.value = localStorage.getItem('editor') || 'anon';
//
// let putBtn = document.getElementById('put_scratch');
// let editorSpan = document.getElementById('scratch_editor');
//
//
//
// // $('#scratch_room');
// let id = roomInput.value
//
// let a = new ArrayInput(['cat', 'dog', 'horse'], {
//     type: 'text',
//     // data: undefined,
//     appendTo: contentDiv,
// });
//
// a.div.addEventListener('array-change', (e, r) => {
//     console.log("Array Change", e, r);
//     localChange()
// })
