/*
async function getSoleByAlias(alias) {
    if (alias[0] != '~' && alias[1] != '@') alias = '~@' + alias;
    return new Promise(resolve => {
        gun.get(alias).once((data) => {
            resolve(Object.keys(data).find(k => k[0] == '~'));
        });
    });
}

async function getUserByAlias(alias) {
    return new Promise(async resolve => {
        let pub = await getSoleByAlias(alias)
        console.log(pub)

        gun.get(pub).once(resolve)
    })
}

getUserByAlias('metoo').then(console.log)

*/
/*
var DEAD = {


    register: async (username, password, pair, sq) => {

        if (!pair) pair = SEA.pair();

        let user = gunAuth.user.auth(pair);
        let salt = bs.uuidv4()
        let salt_hash = await SEA.work(salt, "username");
        let hash = await SEA.work(password, salt_hash);
        let auth = await SEA.encrypt(pair, hash);

        let db = gun.get('#DEAD');

        user.get('salt').put(salt);
        let a = user.get('auth')
        a.put(auth);
        gun.get('#DEAD').get('#' + salt).put(a);
    },

    restore: async (id, password, cb) => {//cb pair also does user auth in gunAuth
        // let salt = bs.uuidv4()
        let salt_hash = await SEA.work(id);
        let hash = await SEA.work(password, salt_hash);
        gun.get('#DEAD').get('#' + salt).once(async (auth) => {
            console.log('hash');
            let pair = await SEA.decrypt(auth, hash);
            gunAuth.user.auth(pair);

            if (typeof cb == "function") cb(pair);
        });
    },

    encode: async (pair, password) => {
        let salt = bs.uuidv4()
        let hash = await SEA.work(password, salt);

        let auth = SEA.encrypt(pair, hash);

        let db = gun.get('#DEAD')
        gun.get('#DEAD').get('#' + salt).put(auth);

        let url = 'x.syon.ca/' + salt;
        // let qr = DEAD.data2qr(url);


    },
    decode: (pair, password) => {

    },


}

*/

//model
/*
gun.#dead.

 */


class GunAuth {

     /*
      IMPORTANT: A Certificate is like a Signature. No one knows who (authority) created/signed a cert until you put it into their graph.
      "certificants": '*' or a String (Bob.pub) || an Object that contains "pub" as a key || an array of [object || string]. These people will have the rights.
      "policy": A string ('inbox'), or a RAD/LEX object {'*': 'inbox'}, or an Array of RAD/LEX objects or strings.
                RAD/LEX object can contain key "?" with indexOf("*") > -1 to force key equals certificant pub.
                This rule is used to check against soul+'/'+key using Gun.text.match or String.match.
      "authority": Key pair or priv of the certificate authority.
      "cb": A callback function after all things are done.
      "opt": If opt.expiry (a timestamp) is set, SEA won't sync data after opt.expiry. If opt.blacklist is set, SEA will look for blacklist before syncing.
      */
    async issueCert(hashed, pair, users = '*') {

        // let hashed =
    }


    /**
     * This saves the content in a frozen space and returns its id key to find it by
     *
     * @param data - the data you want to save
     * @param [cb] - callback with the full key ie id&tag1&tag2#hash and just the id as second param
     * @param {string<SHA-256>} [opts.hash] - if you have already computed the hash pass it in here
     * @param {Array<string>} [opts.tags] - if you wish to add other tags between id and hash key/path is like id&tag1&tag2#hash
     * @param {string} [opts.id=uuidv4()] - if you wish specify an existing id
     * @param {string} [opts.node='#DEAD'] - If you wish to set the name space where to freeze your data
     * @returns {string<uuidv4>} - returns uuid synchronously for making your qr code or whatever
     */
    freeze(data, cb, opts) {

        if (!opts) opts = {};
        if (!opts.node) opts.node = '#DEAD';

        let id = opts.id
        if (!id) id = uuidv4(); //our id/salt

        let d = this.gun.get(opts.node);// the node we are working from root
        let key = id;

        if (opts.tags) {//let the user inject tags
            if (!Array.isArray(opts.tags)) opts.tags = [opts.tags]
            key += "&" + opts.tags.join('&');
        }

        if (opts.hash) {//if the hash of the data was provided use that
            key += '#' + opts.hash
            d.get(key).put(data)
            cb(key, id)
        } else {//otherwise comput hash and save to graph
            SEA.work(data, null, hash => {
                key += '#' + hash
                d.get(key).put(data)
                cb(key, id)
            }, {name: "SHA-256"});
        }
        //final return the id for our node
        return id
    }

    /**
     * Find a previously frozen item by maping the node
     * @param tag - what we are looking for can be any tag or the data or the hash also accepts string , csv or array ie foo / foo,bar [foo, bar] all valid
     * @param cb - called for each match found cb(data, key, tags, hash)
     * @param {Array<string>} [opts.node='#DEAD'] - If you wish to set the name space where to look for your data
     * @param {Function} [opts.search=bs.search] - fn(str, val, opts) lets you define a search function for searching for your tags will be called on both the key and the data
     * @param {String} [opts.onlyKey=false] - set true if you wish to only search the key not the data
     * @param {String} [opts.searchOpts] -  options to pass on to search function see bs.search
     * @returns {Promise<unknown>} - resolves with first found match WARNING use the callback if there is possibly many matches
     */
    findFrozen(tag, cb, opts) {
        if (!opts) opts = {};
        if (!opts.node) opts.node = '#DEAD';
        //if no search function use bs.search or trivial search if that's not defined
        if (!opts.search) opts.search = bs.search;
        if (!opts.search) opts.search = (str, val) => str.includes(val);

        let d = this.gun.get(opts.node);// the node

        return new Promise(resolve => {

            d.map().once((data, key) => {
                let found = false;
                found = opts.search(key, tag, opts.searchOpts);
                if (!found && !opts.onlyKey) {
                    found = opts.search(data, tag, opts.searchOpts);
                }
                if (found) {
                    let s = key.split('#');
                    let hash = s[1]
                    let tags = s[0].split('&');
                    if (typeof cb == "function") cb(data, key, tags, hash);
                    resolve({data, key, tags, hash})
                }
            })
        })
    }

    async testFrozen() {

        let f = gun.get("#DEAD");
        f.map().on((data, key) => {
            console.log('MAP got somthing: ', data, key)
        });
        let d = 'data_' + uuidv4()
        let id = await this.freeze(d, (key, id) => {
            console.log("We got the hash now, KEY: ", key)
        });
        console.log("Froze with id tag: ", id);

        let first = await this.findFrozen(id);

        console.log('Fist found: ', first)
        setTimeout(() => {
            console.log("Testing different and same data")
            f.get(first.key).put('different data'); //error
            f.get(first.key).put(d); //still works
        }, 2000)


        return f
    }

    /**
     * This saves some data probably a sole with som tags to find it by
     * {@see GunAuth#freeze}
     * @param sole - the data to save
     * @param {Array<String>} tags - tags to be saved in the key
     * @returns {Promise<string>}
     */
    async putRef(sole, tags = []) {
        return this.freeze(sole, undefined, {
            node: '#refdex',
            tags: tags,
        })
    }

    /**
     * Find a references saved previously
     * @param tag - what we are looking for can be any tag or the data or the hash
     * @param cb - called for each match found
     * @returns {Promise<unknown>} - resolves with first found match
     */
    async findRef(tag, cb) {
        let d = this.gun.get('#refdex')
        return this.findFrozen(tag, cb, {
            node: '#refdex'
        })
    }




    //these dont work yet and idk if its worth it or we should just user gun.user
    async registerRecovery(password, cb, opts) {

        if (!this.isLoggedIn()) throw "You must be loged in to register a recovery password"

        let salt = bs.uuidv4()
        let pair = this.user._.sea;
        let eKey = await SEA.work(password, salt);
        let auth = await SEA.encrypt(pair, eKey);
        // let sole = '~' + this.user.is.pub


        await this.freeze(auth, cb, {
            id: salt,
            node: '#gunAuthRecovery',
            auth,
        })

        return salt;
    }

    recoverPair(id, password, cb) {
        this.findFrozen(id, async (data, key, tags, hash) => {
            let eKey = await SEA.work(password, tags[0]);
            let pair = await SEA.decrypt(data, eKey);
            if (!pair) {
                console.error("Failed to decrypt pair")
                return;
            }
            await this.user.auth(pair);
            return (pair)
        }, {node: '#gunAuthRecovery'});
    }

    /**
     * Test brute force on a known password this is why you need a good password or a 2 factor system
     * @param password - the password to try on every account
     * @param cb - with a matched pair
     */
    brutePair(password, cb) {
        let time = Date.now();
        let start = time;
        let f = this.gun.get('#gunAuthRecovery')
        return new Promise(resolve => {

            f.map().once(async (data, key) => {

                // let now = Date.now();
                // console.error("Trying decrypt pair:", now - time);
                let s = key.split('#');
                let hash = s[1]
                let tags = s[0].split('&');

                let eKey = await SEA.work(password, tags[0]);
                let pair = await SEA.decrypt(data, eKey);
                if (!pair) {
                    let now = Date.now();
                    console.error("Failed to decrypt pair:", now - time , 'total: ', now-start);
                    time = now;
                    return;
                }

                console.log("FOUND PASSWORD WOO OR BOO!", Date.now() - start, " pair: ", pair);
                await this.user.auth(pair);
                if (typeof cb == "function") cb(pair);
                resolve(pair)


            })
        })


    }

    /**
     * Show you private
     */
    showPair() {
        let pair = this.user._.sea;
        let qr = bs.str2qr(pair)

        let w = bs.getGoodWidth();

        let img = qr.createImgTag()

        let html = ''
        html += qr.createSvgTag(undefined, 1, this.user.is.alias, "GUN AUTH PAIR")

        html += `<pre>${JSON.stringify(pair, null, 1)}</pre>`

        jsPanel.create({
            headerTitle: "GUN AUTH PAIR",
            headerToolbar: [
                "<input name='password' placeholder='password make it secure!'>",
                "<button name='recovery'>Freeze Recovery</button>"
            ],
            panelSize: {
                width: w,
                height: w + 20
            },
            content: html,
            callback: panel => {
                let s = panel.querySelector(`svg[role='img']`)
                s.setAttribute('width', '99%')
                s.setAttribute('height', null)

                let i = panel.querySelector(`input[name='password']`)
                let b = panel.querySelector(`button[name='recovery']`)
                b.addEventListener('click', e => {
                    let p = i.value;
                    if (p.length < 12) {
                        this.resNotify({success: false, msg: 'Make a good password its your only line of defence'});
                        return;
                    }

                    this.registerRecovery(p, (key, id) => {
                        bs.showQr(id);
                    })


                })

            }
        })
    }

    async deadSimpleLogin(username, password, cb) {//cb pair also does user auth in gunAuth
        // let salt = bs.uuidv4()

        return new Promise(resolve => {

            this.findRef(username, async (sole, key, tags, hash) => {
                this.gun.get(sole).on(async u => {
                    // let salt = await u.get('salt')
                    // let auth = await u.get('auth')
                    let salt = u.salt;
                    let auth = u.auth;
                    if (!salt || !auth || !password) {
                        throw "Bad User AUth Registration, What have you done?"
                    }


                    let hash = await SEA.work(password, salt);
                    let pair = await SEA.decrypt(auth, hash);

                    cb(pair, sole)

                    resolve(pair);

                })


            })
        })

    }

    //these dont work yet and idk if its worth it or we should just user gun.user
    async register(password, pair, cb) {
        if (!pair) pair = await SEA.pair();


        await this.user.create(pair);
        // let user = await gunAuth.user.


        let salt = bs.uuidv4()
        // await this.user.auth(pair)
        //
        // var pair = typeof args[0] === 'object' && (args[0].pub || args[0].epub) ? args[0] : typeof args[1] === 'object' && (args[1].pub || args[1].epub) ? args[1] : null;


        let eKey = await SEA.work(password, salt);
        let auth = await SEA.encrypt(pair, eKey);
        let sole = '~' + this.user.is.pub
        //

        let data
        this.user.put({
            salt, auth
        }, console.log);
        // this.gun.get(sole).get('auth').put(auth);
        this.putRef(sole, [username, salt])
        this.putRef(auth, [username, salt, 'auth'])


        return salt;
    }

    async deadSimpleLogin(username, password, cb) {//cb pair also does user auth in gunAuth
        // let salt = bs.uuidv4()

        return new Promise(resolve => {

            this.findRef(username, async (sole, key, tags, hash) => {


                this.gun.get(sole).on(async u => {
                    // let salt = await u.get('salt')
                    // let auth = await u.get('auth')
                    let salt = u.salt;
                    let auth = u.auth;
                    if (!salt || !auth || !password) {
                        throw "Bad User AUth Registration, What have you done?"
                    }


                    let hash = await SEA.work(password, salt);
                    let pair = await SEA.decrypt(auth, hash);

                    cb(pair, sole)

                    resolve(pair);

                })


            })
        })

    }

    async demoSimpleAuth() {

        let id = await this.register('foo', 'pass');
        // await this.register('bar', 'pass');

        console.log(id);
        console.log("user: ", this.user.is);
        this.logout()
        console.log("user: ", this.user.is);
        let fooPair = await this.deadSimpleLogin(id, 'pass')

        await this.user.auth(fooPair);
        console.log("user: ", this.user.is);


        console.log(fooPair);


    }


    async restore(salt, password, cb) {//cb pair also does user auth in gunAuth
        // let salt = bs.uuidv4()


        // let salt_hash = await SEA.work(salt);
        let hash = await SEA.work(password, salt);


        gun.get('#DEAD' + salt).on(async (auth) => {

            console.log("got auth: ", auth);
            let pair = await SEA.decrypt(auth, hash);
            gunAuth.user.auth(pair);

            if (typeof cb == "function") cb(pair);
        });
    }


    resNotify(res) {

        let closeCB = (panel) => {
            setTimeout(() => {
                console.log('freken close: ', panel.close(), '  panel: ', panel);
                // panel.close();

            }, 4000)
        }

        if (res.success) {
            jsPanel.hint.create({
                position: 'right-top 0 15 down',
                contentSize: '310 auto',
                content: res.msg,
                theme: 'success filled',
                headerTitle: '<i class="fad fa-check mr-2"></i>Success',
                animateIn: 'animate__animated animate__bounceInUp',
                animateOut: 'animate__animated animate__bounceOutUp',
                autoclose: {
                    time: '4s',
                    background: 'linear-gradient(90deg, rgba(255,0,0,1) 0%, rgba(0,255,17,1) 100%)'
                },
                // callback: closeCB
            });
        } else {
            jsPanel.hint.create({
                position: 'right-top 0 15 down',
                contentSize: '310 auto',
                content: res.msg,
                theme: 'danger filled',
                headerTitle: '<i class="fad fa-check mr-2"></i>Failure',
                animateIn: 'animate__animated animate__bounceInUp',
                animateOut: 'animate__animated animate__bounceOutUp',
                autoclose: {
                    time: '4s',
                    background: 'linear-gradient(90deg, rgba(255,0,0,1) 0%, rgba(0,255,17,1) 100%)'
                },
                // callback: closeCB
            });
        }
    }


    constructor(gun) {
        if (!gun) {
            gun = new Gun('https://bullchat.syon.ca/gun');
        }
        this.gun = gun;
        this.user = gun.user();

        var user = gun.user().recall({sessionStorage: true});


        this.msgLimit = 100;
        this.msgSize = 500;
        this.msgs = [];
    }

    /**
     * Lookup pubkeys by gun aliase
     * @param alias
     * @return {Promise<unknown>}
     */
    async getSolesByAlias(alias) {
        if (alias[0] != '~' && alias[1] != '@') alias = '~@' + alias;
        return new Promise(resolve => {
            this.gun.get(alias).once(resolve)
        })
    }

    // async getSoleByAlia(alias) {
    //     return this.getSoleByAlias(alias)
    // }
    async getUserByAlias(alias) {
        return new Promise(async resolve => {
            let pubs = this.getSolesByAlias(alias)

            let keys = Object.keys(pubs).filter(k=>k[0] === '~');

            let latest = 0;
            let pub

            keys.forEach(k=>{
                if(pubs[k] > latest) {
                    latest = pubs[k];
                    pub = k;
                }
            })

            this.gun.get(pubs[0]).once(resolve)
        })
    }

    isLoggedIn() {
        return !!this.user.is
    }

    // getQr


    /**
     * Does not return till gun has a user. Prompts user to login if necessary
     * @returns {Promise<unknown>}
     */
    promiseAuth() {

        return new Promise(resolve => {
            if (this.isLoggedIn()) {
                resolve(this.user);
            } else {
                this.login().then((ack) => {
                    console.log(ack)
                    resolve(this.user);
                })
            }

        })

    }

    async restoreUser() {
        if (!this._user) throw "No user to restore";
        this.user.auth(this._user);
    }

    getPair() {
        return this._user;
    }


    createUser(username) {

        let div = document.createElement('div');
        div.style.padding = '2em';

        div.innerHTML = `
        
        <label>Username:</label><input>
        <label>Password: </label><input type="password" >
        <label>Email: (optional) </label><input type="email">

        <button>Register</button>
        `;
        let modal = jsPanel.modal.create({
            headerTitle: 'Register User',
            content: div,
        });

        div.querySelector('button').addEventListener('click', e => {
            let inputs = div.querySelectorAll('input');
            let u = inputs[0].value;
            let p = inputs[1].value;
            let email = inputs[2].value;

            this.user.create(u, p, ack => {
                console.log(ack);
                if (ack.err) {
                    div.innerHTML += "<span style='background: #ff5b5b'>" + ack.err + '</span>';
                } else {
                    this.user.get('email').put(email);
                    modal.close();
                }
            })
        })

    }

    getUsers() {
        return new Promise((resolve, reject) => {
            this.gun.get('#user_catalogue_1')
                .on((indexedCatalogueOfPubKeys) => {
                    this.indexedCatalogueOfPubKeys = indexedCatalogueOfPubKeys;
                    delete indexedCatalogueOfPubKeys._;
                    this.allPubKeys = Object.values(indexedCatalogueOfPubKeys);
                    resolve(this.allPubKeys);
                })
        });
    }

    catalogUser() {
        let pub = this.user.is.pub;
        SEA.work(pub, null, hash => {
            console.log("saving to catalog: ", hash);
            this.gun.get('#user_catalogue_1').get(hash).put(pub);

        }, {name: "SHA-256"});
    }

    login(username) {
        return new Promise((resolve, reject) => {
            let div = document.createElement('div');
            div.style.padding = '2em';
            username = username || '';
            div.innerHTML = `
        <span></span>
        <div style="display: flex; align-items: center;">
            <label>Username: </label>
            <input value="${username}"></div>
        <div style="display: flex; align-items: center;">
            <label>Password: </label>
            <input type="password" ></div>
            
            <small>min password is 8 characters long. If you want security chose a much stronger and uncommon password this is your only line of deffence.</small><br>
        <button name="login">Login</button>
        <button name="create">Register</button>
        
        `;
            let modal = jsPanel.modal.create({
                headerTitle: 'Login Gun',
                content: div,

                contentSize: {width: '400px', height: '300px'},

                onclosed: function (panel, closedByUser) {
                    console.log(`Panel with id: ${panel.id} closed!\nclosedByUser: ${closedByUser}`);
                    if (closedByUser) {
                        reject("Login Modal Closed By User!");
                    }
                },

                callback: (panel) => {

                    let u, p

                    //handle gun response
                    let loginCB = (ack) => {
                        console.log(ack);
                        if (ack.err) {
                            status.innerHTML = "<span style='background: #ff5b5b'>" + ack.err + '</span>';
                            // status.style.background = 'var(--danger)'

                        } else {
                            this.user.get('logins').set(Date.now());

                            this._user = gunAuth.user._.sea;
                            modal.close();
                            resolve(this.user);
                            this.resNotify({success: true, msg: 'Successfully logged in as ' + u})
                            // let gab = document.getElementById('ga_bottom');
                            // if(gab) gab.pub = this.user.is.pub;

                            this.catalogUser();
                            // SEA.work(pub, null, hash => {
                            //     console.log("saving to catalog: ", hash);
                            //     this.gun.get('#user_catalogue_1').get(hash).put(pub);
                            // }, {name: "SHA-256"});
                        }

                    }
                    //for submitting login
                    let loginClick = e => {
                        let inputs = div.querySelectorAll('input');
                        u = inputs[0].value;
                        p = inputs[1].value;
                        this.user.auth(u, p, loginCB, {});
                        // module.onCl
                    }

                    let inputs = div.querySelectorAll('input');
                    let status = div.querySelector('span');
                    inputs[0].focus();
                    inputs[0].addEventListener('keyup', (e) => {
                        if (e.key === 'Enter' || e.keyCode === 13) {
                            inputs[1].focus();
                        }
                    })

                    inputs[1].addEventListener('keyup', (e) => {
                        if (e.key === 'Enter' || e.keyCode === 13) {
                            // Do something
                            loginClick()
                        }
                    })
                    div.querySelector('button[name="login"]').addEventListener('click', loginClick)

                    div.querySelector('button[name="create"]').addEventListener('click', e => {
                        u = inputs[0].value;
                        p = inputs[1].value;
                        // let email = inputs[2].value;

                        this.user.create(u, p, ack => {
                            console.log(ack);
                            if (ack.err) {

                                status.innerHTML = "<span style='background: #ff5b5b'>" + ack.err + '</span>';
                                // div.innerHTML += "<span style='background: #ff5b5b'>" + ack.err + '</span>';
                            } else {

                                this.resNotify({success: true, msg: 'Successfully created user ' + u})

                                this._user = this.user._.sea;

                                // register the user in a content addressed node
                                const pub = this.gun.user().is.pub;

                                this.catalogUser()

                                // this.user.auth(u, p, loginCB);
                                // this.user.get('email').put(email);
                                modal.close();
                            }
                        })
                    })

                }
            });
        });
    }

    createUser(alias, pw) {
           this.user.create(alias, pw, ack => {
                            console.log(ack);
                            if (ack.err) {

                                status.innerHTML = "<span style='background: #ff5b5b'>" + ack.err + '</span>';
                                // div.innerHTML += "<span style='background: #ff5b5b'>" + ack.err + '</span>';
                            } else {

                                this.resNotify({success: true, msg: 'Successfully created user ' + u})

                                this._user = this.user._.sea;

                                // register the user in a content addressed node
                                const pub = this.gun.user().is.pub;

                                this.catalogUser()

                            }
                        })
    }

    setupMail() {

        let outbox = this.user.get('outbox');
        let inbox = this.user.get('inbox');
    }


    async restoreAuth() {
        if (!this._user) console.error("Nothing to restore :(");

        await this.user.auth(this._user);
        return this.user;
    }

    logout() {
        this.user.leave();
    }

    /**
     * Creates a group with you as the owner cannot be transferred
     * @param name
     * @param pair
     * @returns {Promise<void>}
     */
    async createGroup(name, pair) {
        if (!pair) pair = SEA.pair();
        if (!name) name = pair.pub;

        await this.promiseAuth();

        let dbGroup = this.user.get('groups').get(name);

        dbGroups.put({
            owner: this.user.is.pub,
            pub: pair.pub,
            epub: pair.epub,
            name: name,
            desc: 'A gun Auth Group',
            members: []
        });

        // dbGroup.put(in)


        this.gun.get('ga').get('groups').get('#' + name).put({
            // pub: pair.pub,
            // epub: pair.epub,
            // owner: user.

        });


    }


    groupAddMember(name, pub) {
        if (typeof pub == "object") pub = pub.pub;
        let dbGroup = this.user.get('groups').get(name);


    }


    createRoom(name) {

    }


    public(key, val) {


    }

    getChat() {


        var match = {
            // lexical queries are kind of like a limited RegEx or Glob.
            '.': {
                // property selector
                '>': new Date(+new Date() - 1 * 1000 * 60 * 60 * 3).toISOString(), // find any indexed property larger ~3 hours ago
            },
            '-': 1, // filter in reverse
        };
        /*
                // Get Messages
                this.gun.get('chat')
                    .map(match)
                    .once(async (data, id) => {
                        if (data) {
                            // Key for end-to-end encryption
                            const key = '#foo';

                            var message = {
                                // transform the data
                                who: await db.user(data).get('alias'), // a user might lie who they are! So let the user system detect whose data it is.
                                what: (await SEA.decrypt(data.what, key)) + '', // force decrypt as text.
                                when: GUN.state.is(data, 'what'), // get the internal timestamp for the what property.
                            };

                            if (message.what) {
                                messages = [...messages.slice(-100), message].sort((a, b) => a.when - b.when);
                                if (canAutoScroll) {
                                    autoScroll();
                                } else {
                                    unreadMessages = true;
                                }
                            }
                        }
                    });*/
    }

}

let btn;

//async function getCat(e) {
let getCat = async (e) => {
    //time = time of invocation
    let res = await fetch('syon.ca/img/cat.png');
    //time = time after request

    return res;
}

setTimeout(getCat, 1000);

let promiseCat = getCat();
//time = time of invocation
let cat;
promiseCat.then(val => {
    //time = time after request

    cat = val;
}).catch(error => {
    console.log(error)
});
//time = time of invocation


// btn.addEventListener('click',)

// async function sendMessage() {
//     const secret = await SEA.encrypt(newMessage, '#foo');
//     const message = user.get('all').set({ what: secret });
//     const index = new Date().toISOString();
//     db.get('chat').get(index).put(message);
//     newMessage = '';
//     canAutoScroll = true;
//     autoScroll();
// }

// var gunauth = new GunAuth();
