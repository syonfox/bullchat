/**
 * Js file for index page
 * Could be seen as a demo but may be advanced
 */


// https://developer.mozilla.org/en-US/docs/Web/API/HTMLElement
// https://developer.mozilla.org/en-US/docs/Web/Web_Components/Using_custom_elements
/**
 * So this is a system for extending modules MUST be an instance of HTMLElement OR provide an element in its 'el' property
 *  and YOU MUST IMPLEMENT getValue() setValue(), getDefault(), hootChange()  functions that complement each other you are responsible for the values to work
 *
 *  ie myMod.setValue(myMod.getValue()) should work and same for getDefault() ... warning JSON.stringified so no cycles.
 */





//Initialize gun
var gun = new Gun({
    // peers: ['http://bullchat.syon.ca:8585/gun'],
    peers: ['https://bullchat.syon.ca/gun'],
});


function uuidv4() {
    return ([1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, c =>
        (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
    );
}

let container = document.getElementById('my_content');


window.mobileCheck = function () {
    let check = false;
    (function (a) {
        if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4))) check = true;
    })(navigator.userAgent || navigator.vendor || window.opera);
    return check;
};

let isMobile = mobileCheck();


var gunAuth = new GunAuth(gun);
// gun.auth = gunAuth;

var g = gunAuth;
var u = gunAuth.user;

var params = bs.getParams();

function restorePair() {

    let id;
    if (params.id) {
        id = params.id;
    }
    jsPanel.create({
        headerTitle: 'Restor User',
        content: `
        <input name='id' value="${id}">
        <input name='pass'>
        <button name="submit">Restore</button>
        `,
        callback: panel => {
            let id = panel.querySelector('input[name="id"]')
            let pass = panel.querySelector('input[name="pass"]')
            let b = panel.querySelector('button[name="submit"]')
            b.addEventListener('click', e=>{
                let i = id.value;
                let p = pass.value;
                gunAuth.recoverPair(i, p, ()=>{
                    gunAuth.showPair();
                })
            })

        }
    })
}

if(params.id && !gunAuth.isLoggedIn()) {
    restorePair()
}


// # example: messaging a friend
// keybase chat send --exploding-lifetime "30s" friend1234 \
// "Yo - meet @ 10pm under the powerlines. Bring the stuff"

let blog = new BullChat(gun, {
    modules: ['textarea'],
    mode: 'block',
    gunAuth: gunAuth
})

let bullchat = new BullChat(gun, {
    id: 'scratch_container',
    // container: '.bullchat-rich',
    // gunAuth: gunAuth,
    initPanel: !isMobile,
    room: 'bullchat3',
    modules: [
        {
            type: 'textarea'
        },
        tinyEditorFactory(),

        new ArrayInput(),
    ]
})

if (isMobile) {
    bullchat.container.firstElementChild.prepend($(bullchat._getTools()[0])[0])//magic jquery :)
    bullchat._setupTools();
    // bullchat.container.style.width =  document.body.clientWidth + 'px';
}

