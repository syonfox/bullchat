/**
 * So you wanted a chat app one liner :)
 *
 * Gun is an graph database that is pretty powerfully the useful function is that its acs as a chain
 * this abstraction is brought to js by returning an object for each .get(key) command. You can think of this as the
 * master reference.  This is similar to Objects in js
 * let chainNode = gun.get('myThing').get(myProperty).get('anything')
 * chainNode.once((data)=>{console.log('my code to run only once when the object is received')})
 * chainNode.on((data)=>{console.log('runs any time the data is updated including immediately')})
 *
 * See gunAuth for details on users
 *
 * //ok two lines for clean code
 * let gun = new Gun()
 * let chat = new BullChat(gun, {});//options
 */

// https://developer.mozilla.org/en-US/docs/Web/API/HTMLElement
// https://developer.mozilla.org/en-US/docs/Web/Web_Components/Using_custom_elements
/**
 * So this is a system for extending modulesMust be an instance of HTMLElement or provide an element in its 'el' property
 *  and also preoved getValue() setValue() aand getDefault()  functions that complement each other you are responsible for the values to work
 *  ie myMod.setValue(myMod.getValue()) should work and same for getDefault() ... warning JSON.stringified so no cycles.
 */
class BullChatModule extends HTMLDivElement {
    constructor() {
        // Always call super first in constructor when extending
        super();

        // write element functionality in here
        this.attachShadow({mode: 'open'});
        this.container = document.createElement('div');
        this.style = document.createElement('style');
        this.shadowRoot.appendChild(this.style);
        this.shadowRoot.appendChild(this.container);

        this.myData = 'Hello World';


    }

    //web component methods you might want/need to implement
    connectedCallback() {
        console.log('Custom square element added to page.');
        this.updateStyle(this);
    }

    disconnectedCallback() {
        console.log('Custom square element removed from page.');
    }

    adoptedCallback() {
        console.log('Custom square element moved to new page.');
    }

    // Note that to get the attributeChangedCallback() callback to fire when an attribute changes, you have to observe
    // the attributes. This is done by specifying a static get observedAttributes() method inside custom element class
    // - this should return  an array containing the names of the attributes you want to observe:
    static get observedAttributes() {
        return ['c', 'l'];
    }

    attributeChangedCallback(name, oldValue, newValue) {
        console.log('Custom square element attributes changed.');
        this.updateStyle(this);
    }

    updateStyle() {
        const shadow = this.shadowRoot;
        //wow much css this is cool official hack
        this.style.textContent = `
        div {
          width: ${elem.getAttribute('l')}px;
          height: ${elem.getAttribute('l')}px;
          background-color: ${elem.getAttribute('c')};
        }
      `;
    }

    //^^^^^^^^^
    //above is sample of what a webcomponent can do
    //=================================================
    //bellow are Bullchat functions you need to implement for your module
    //  todo see if this modal can be adapted from backbone.Model
    //vvvvvvvvvv
    setValue(data, el, bullchat) {
        //called when remote change happens... sets value.
        //this should also be the bullchat
        // this.querySelector('input').value = data
    }

    //if you need "this" to remain unchanged use arrow functions.
    // and call from constructor
    initModFuncs() {
        this.getValue = (el, bullchat) => {//arrow functions do not modify 'this'
            //this :== bullchatmod
            //do whatever you need to do to get and return a value
            return this.myData;
        }

        this.hookChange = (fn, el, bullchat) => {
            this.fnToCallWhenMyDataChange = fn;
        }
    }
}

//or you can set it up later to an exiting class

BullChatModule.prototype.getDefault = function (el, bullchat) {//normal functions 'this' is the calling context
    //this is bullchat
}//.bind(youValForThis) // you can also bind any val you want to this when creating a function (only regular functions not arrow)

//Bullchat will look for these methods but can be provided them when registering module
// so you can just give it any old function you want :) all the params you need should be provided

//todo module register function

/**
 * Get a container or make it if necessary
 * @param el
 * @returns {Element}
 */
bs.getContainer = (el) => {
    if (el instanceof HTMLElement) {
        return el;
    }
    let domEl;
    if (typeof el == 'string') {
        domEl = document.querySelector(el);
        if (!domEl) domEl = document.getElementById(el);
    }

    if (!domEl) {//finaly just create it
        domEl = document.createElement('div');
        if (typeof el == 'string') {
            //treat it like a query selector to add classes or set id
            if (el[0] == '#') {
                domEl.id = el.splice(1, el.length - 1)
            }//todo do properly
        }
    }

    return domEl;
}

// All that is overkill but all you need is an
// container el
// getValue(modElement, bullchat)
// setData(data, modElement, bullchat)
// getDefault(modElement,bullchat) functions in you plugin and the constructor should return a dom Element
// hookChange: (fn, el, bullchat) => {//this asks you to hook bullchats function to your change event(s)
//a sample instance of a minimal object that is accepted
/**
 * @requires Gun
 * @requires Bullet
 * @requires jsPanel4
 *
 * BullChat is a gun wrapper that allows you to create simple modules and have them synchronize
 */
class BullChat {

    /**
     * Constuct a bullchat
     * @param gun - theoreticaly could be any chain element even from gun.get('myBullChatNamspace')
     * @param options
     * @param {String} [options.id] - the namespace within bullchat and the dom id for building jspanel if desired
     * @param {String | HTMLElement} [options.container='#id'] - a css selector for the panel content caution this will be manipulated in jspanel: true
     *      this will override the id as the source of container
     * @param {Boolean | Object} [options.initPanel=false] - set to true if you want bullchat to initialise the panel for you
     * @param {Boolean | Object} [options.toolbar=true] - set to false to disable toolbar or a string to set its contents

     * @param {Boolean | Object} [options.jspanelOptions] options for the jspanel4 if desired see code for defaults
     * otherwise https://github.com/Flyer53/jsPanel4 for info on customizing
     * @param {String} [options.room='bullchat'] - the default room to enter
     * @param {Boolean} [options.restoreRoom=true] - Whether or not to restore the room from local storage will also prevent saving to localStorage if false
     @param {Boolean} [options.appendModules=true] - if set to false bullchat will not move you module elements
     * @param {Array<Object>} options.modules - an array of bullchat modules names for this instance see module doc somewhere lest :)
     * @param {String} options.modules.type - the module type ie scratch, arrayinput, chat.. etc todo update as built
     * @param {String} options.modules.selector - the css selector bullchat should use for an existing dom element usually a container for complicated things
     * @param {Object} options.modules.options - module options see module docs
     * @param {Function} options.callback.callback -   callback(panel, el, bullchat) on dom creation for all your hacking needs dont use jspanelOptions.callback it is used internally
     */
    constructor(gun, options) {

        if (!gun) throw new Error("You must provide a instance of gun for bullchat to use")

        this.gun = gun.get('bullchat');

        let defaults = {
            id: 'bullchat_container',
            initPanel: false,
            jspanelOptions: {
                headerTitle: 'BullChat Room',
                theme: '#fae000',
                borderRadius: '0.5rem',
                panelSize: bs.getGoodPanelSize(8.5/11, 600, 25),
                aspectRatio: "panel",

                data: {},
                dragit: {
                    containment: 0,
                    snap: {
                        sensitivity: 50, trigger: 'panel', active: 'both',
                    },
                }, onclosed: () => {//todo do this work in merge deep
                    this.panel = undefined;//clean up reference
                },
                callback: (panel) => {
                    this.panelInitialized(panel);
                }
            },
            room: 'bullchat',
            restoreRoom: true,
            appendModules: true,
            modules: [
                {type: 'textarea'}
            ]
        }

        if (options.gunAuth) {//we cant merge a complicated object like gunauth
            this.gunAuth = options.gunAuth;
            options.gunAuth = true;

            this._pair = this.gunAuth.user._.sea;
        }

        this.options = bs.mergeDeep(defaults, options)

        //start listening to bullchat updates

        this.id = options.id || "chatid_" + bs.id++;

        //The data for this chat will be namspaced into gun.bullchat.chatid.roomname
        this.data = gun.get(this.id);

        //Allow the user to give us the container in a variety of ways
        // HTMLElement, css selector, or using the id field as the id
        if (this.options.container instanceof HTMLElement) {
            this.container = this.options.container
        } else if (typeof this.options.container == 'string') {
            this.container = document.querySelector(options.container);
        } else {
            this.container = document.getElementById(this.id);
        }

        //finally if we dont have a container make it
        if (!this.container) {
            this.container = document.createElement('div');
            this.container.id = this.id
        }

        // this.data = gun.get(options.id).get('scratch');//we dont realy need another namespace but it doesnt hurt

        this.initChangeFuncs(); //set them up so event callsdont override 'this'

        //Ok so now we need to configure all our modules
        this._mods = []; // a place to keep track of internal module elements

        options.modules.forEach((moduleOptions) => {
            if (typeof moduleOptions == 'string') {
                moduleOptions = {name: moduleOptions};
            }
            //switch to name without breaking backwards compatibility to {type: 'textarea'} => prefered {name:'textarea', selector, or el} or 'textarea'
            if (moduleOptions && moduleOptions.type && !moduleOptions.name) moduleOptions.name = moduleOptions.type;

            //initalize the module
            let m = {}
            switch (moduleOptions.name) {
                case 'arrayinput': {
                    console.error("NOT IMPLEMENT")

                    break;
                }
                case 'chat': {
                    console.error("NOT IMPLEMENT")

                    break;
                }
                case 'groupchat': {
                    console.error("NOT IMPLEMENT")
                    break;
                }
                case 'anothername': {
                    console.error("NOT IMPLEMENT")
                    break;
                }

                case "color":
                case "date":
                case "datetime-local":
                case "datetime":
                case "email":
                // case "hidden":
                // case "image":
                case "month":
                case "number":
                case "password":
                case "range":
                case "search":
                case "tel":
                case "text":
                case "time":
                case "url":
                case "week":
                //this will run for everything above
                // m.type = 'input'
                // m.
                case 'input': {

                    let modElement, input;
                    //if the dev gave use a selector use that element
                    if (moduleOptions.selector) {
                        modElement = document.querySelector(moduleOptions.selector);
                    }
                    //el will override a selector if provided
                    if (moduleOptions.el) {
                        if (typeof moduleOptions.el == 'string') modElement = document.querySelector(moduleOptions.el);
                        else if (moduleOptions.el instanceof HTMLElement) modElement = moduleOptions.el;
                        else {
                            console.warn('mod.el provided but no was not a selector or html element :(')
                        }
                    }

                    if (moduleOptions.name == 'input') moduleOptions.name = 'text';

                    //if we dont have it yet build it
                    if (!modElement) {
                        modElement = document.createElement('input');
                        modElement.type = moduleOptions.name;
                    }


                    //if its not an input look for one
                    if (modElement.tagName != 'INPUT') {
                        input = modElement.querySelector('input');
                    } else {
                        input = modElement;
                    }


                    input.addEventListener('input', this._localChange);
                    //save our reference
                    moduleOptions.el = modElement;// the element

                    //implement module methods
                    moduleOptions.getValue = () => { //how to turn it into a string
                        return input.value;
                    }
                    moduleOptions.setValue = (data) => { //how to restore from your getValue Function
                        return input.value = data;//newlinehelper???
                    }

                    moduleOptions.getDefault = () => { // what if its a new room
                        let val = input.value;
                        if (typeof moduleOptions.default != "undefined") val = moduleOptions.default()
                        return val
                    }
                    moduleOptions.hookChange = (fn) => { // what if its a new room
                        input.addEventListener('input', fn);
                    }
                    //this switch is task is to define the module m given moduleOptions
                    m = moduleOptions
                    break

                }
                case 'scratch'://deprecated
                case 'textarea': {
                    //todo properly implement module standard of some sort :)
                    let modElement, textarea;
                    //if the dev gave use a selector use that element
                    if (moduleOptions.selector) {
                        modElement = document.querySelector(moduleOptions.selector);
                        textarea = modElement.querySelector('textarea');
                    }
                    //
                    if (!modElement) {
                        //if we dont have it build it
                        modElement = document.createElement('div');
                        modElement.name = 'textarea';
                    }
                    if (!textarea) {
                        textarea = document.createElement('textarea');
                        textarea.rows = 5;
                        modElement.appendChild(textarea);
                    }
                    //subscribe to changes

                    textarea.addEventListener('input', this._localChange);
                    //save our reference
                    moduleOptions.el = modElement;// the element

                    //implement module methods
                    moduleOptions.getValue = () => { //how to turn it into a string
                        return textarea.value;
                    }
                    moduleOptions.setValue = (data) => { //how to restore from your getValue Function
                        return textarea.value = data;//newlinehelper???
                    }

                    moduleOptions.getDefault = () => { // what if its a new room
                        let val = "Welcome to a new scratch pad you can edit in real-time with others. =(^o^)=\n "
                        val += this.getQuote();
                        // this.getQuote();
                        return val;
                    }
                    moduleOptions.hookChange = (fn) => { // what if its a new room
                        textarea.addEventListener('input', fn);
                    }


                    //this switch is task is to define the module m given moduleOptions
                    m = moduleOptions
                    break;
                }
                default: {

                    //default is to assume we got a module todo implement module registration
                    console.error('UNKNOWN BULLCHAT MODULE :(');
                    if (moduleOptions instanceof HTMLElement) {
                        m.el = moduleOptions;
                    } else if (moduleOptions.el) {
                        m = moduleOptions;
                    }

                    //validate module
                    if (typeof moduleOptions.getValue == "function") m.getValue = moduleOptions.getValue;
                    if (typeof m.getValue != "function") throw "Module getValue not implemented";

                    if (typeof moduleOptions.setValue == "function") m.setValue = moduleOptions.setValue;
                    if (typeof m.setValue != "function") throw "Module setValue not implemented";

                    if (typeof moduleOptions.getDefault != "undefined") m.getDefault = moduleOptions.getDefault;

                    //this makes getDefault: false  or getDefault: {foo:'bar'} also work
                    // but if you really want undefined you need to getDefault: ()=>{return undefined}
                    if (typeof m.getDefault != "function") {
                        console.warn("Module getDefault(el, bullchat) Function not implemented.  setting to value if != undefined otherwise getQuote()")
                        if (typeof m.getDefault == "undefined") {
                            m.getDefault = () => this.getQuote();
                        } else {
                            let val = m.getDefault;
                            m.getDefault = () => val;
                        }
                    }


                    if (typeof moduleOptions.hookChange == "function") m.hookChange = moduleOptions.hookChange;
                    if (typeof m.hookChange != "function") throw "Module hookChange not implemented";
                    m.hookChange(this._localChange, m.el, this);

                    // this.container.appendChild(m.el);

                    break;
                }
            }//end switch
            //common module stuff
            let id = m.id || m.el.id;
            if (!id) id = this._mods.length;
            m.id = id;
            this._mods.push(m);//we need to keep track of them internally

        })//end forEach now we should have an array of this._mods

        //now the module is complete and we can add it.
        //append our now constructed modules
        if (this.options.appendModules == true) {
            this._mods.forEach(mod => {
                this.container.appendChild(mod.el);
                if (typeof mod.hookChange == "function") {
                    mod.hookChange(this._localChange, mod.el, this);//todo dosent have to be so often?
                }
            })
        }

        if (this.options.restoreRoom && !options.room) {
            this.restoreRoom(this.options.room);
        } else {
            this.setRoom(this.options.room);
        }


//if the user asked us to initialise when ready lets do that
        if (options.initPanel) this.initPanel();


    }

    /**
     * Set the mode of operation for this bullchat
     * has to do with where the data is stored and how it is encrypted
     * @param mode - the mode you want
     * @param [pair] - if necessary the key pair for encryption
     * @param [world] - if necessary the root of the graph
     * @returns {boolean}
     */
    setMode(mode, pair, world) {
        this._detachRoom();//detach so that no updates can interrupt
        if(!pair) pair = gunAuth.user._.sea;
        if (mode === 'diary') {
            this.setWorld(this.gunAuth.user);
            this.setPair(this.gunAuth.user._.sea);
            //user graph encrypted
            // return true;
        } else if (mode === 'blog') {//draft?
            this.setWorld(this.gunAuth.user);
            this.setPair(false);
            //user graph , not encrypted
            // return true;
        } else if (mode === 'password-blog') {
            this.setWorld(this.gunAuth.user);
            this.setPair(pair)
            //user graph , encrypted with password
            // return true;
        } else if (mode === 'bulletin') {//public
            this.setWorld(this.gun);
            this.setPair(false);
            //public graph ,not encrypted
            // return true;
        } else if (mode === 'password-bulletin') {//encrypted-public / public-secret
            this.setWorld(this.gun);
            this.setPair(pair);
            //public graph ,not encryptes
            // return true;
        } else {
            console.warn("MODE NOT IMPLEMENTED: ", mode)
            // return false;
        }

        this._attachRoom();//reattach the same room;
        return true;
        //public
        //group /collaboration (colab)
        // console.warn("NOT IMPLEMENTED")
        // return false;
    }

    /**
     * Set the root node for this bullchat to exist in aka the world of the bulchat. this is a gun chain node
     * It could be a user for encryption this is at the level above rooms.
     * @param {Gun.Node} place - can be gun root or user depending on how you wish things to operate
     */
    setWorld(place) {
        this._detachRoom()
        this.gun = place;
        this._attachRoom()
    }

    /**
     * Set the pair for bullchat encryption
     * @param pair - the key pair to use for encryption
     */
    setPair(pair) {
        this._pair = pair;
        return this._pair;
    }

    /**
     * Detach event listeners to room
     * @private
     */
    _detachRoom() {
        if (this.room) {//only need to turn it off if we were subscribed
            this.data.get(this.room).off();
        } // todo do we have to remove hooks on destruction
    }

    /**
     * Attache event listeners to room
     * @private
     */
    _attachRoom() {
        let room = this.room;


        this.data.get(room).once(async roomData => {
            // Ok so first we get he room once and set defaults if necessary

            let data, string;
            if (!roomData || !roomData.string) {
                console.log("No Data Found in Room Setting Defaults");
                data = this._mods.map(m => {//map maps an array to another array using provided function
                    return m.getDefault(m.el, this);
                });
            } else {
                // string = roomData.string;
                data = await this._parseData(roomData);
            }

            this._setMods(data);

            for (let i = 0; i < this._mods.length; i++) {
                this._mods[i].setValue(data[i], this._mods[i].el, this);
                // setting value will trigger local change .. whitch might trigger remote change but thats ok since it will be the same and not change again
                // ... i think not 100% positive
            }

        }).on(this._remoteChange); //start listen to the new one
    }

    /**
     * set us up to listen on a room
     * @param room
     */
    setRoom(room) {
        this._detachRoom();

        this.room = room;

        if (this.options.restoreRoom) {//if we are supposed to restore room later we need to save it
            let key = this.id + 'room';
            localStorage.setItem(key, room);
        }

        console.log("BULLCHAT: room changed", this.id);

        this._attachRoom();
    }

    /**
     * Gets the room node of the graph
     * for the room string user this.room
     * @returns {Gun.chain}
     */
    getRoom()
    {
        return this.gun.get(this.room);
    }



    /**
     * restore a room from local storage
     * @param backup - if no room is found use this
     */
    restoreRoom(backup) {
        let key = this.id + 'room';
        let r = localStorage.getItem(key);
        if (!r) r = backup;
        this.setRoom(r);
    }

    /**
     * Might be needed to fix stringified textarea values
     * @param input
     * @returns {*}
     * @private
     */
    _newlineHelper(input) {
        var newline = String.fromCharCode(13, 10);
        return input.replaceAll(/\\+n/g, newline);
    }

    /**
     * Given serialized data from gun parse out the module data array
     * @param string
     * @returns {Array<*>} - should match _mods array
     * @private
     */
    async _parseData(gunData) {
        let data;
        if (this._pair) {
            if (this._pair.pub != gunData.pub) {
                console.warn("Not the right _pair for this data")
            } else {
                data = await SEA.decrypt(gunData.string, this._pair);// also parses whern decrypting
            }
        }
        if (!data) {
            data = JSON.parse(gunData.string);
        }
        if (!Array.isArray(data)) {
            console.error("HMM looks like the data was not how I expected it in the room");
        }

        return data;

    }

    /**
     * this is done in here so that the functions keep a reference to 'this' since they are es6 arrow functions
     * _localChange is what gets sent to the modules;
     * _remoteChange gets called when the data changes remotely ie someone elese sent a message
     */
    initChangeFuncs() {

        /**
         * Local change should be called whenever something is changes by the user
         * its responsible for serializing the modules and saving them into gun
         * @private
         */
        this._localChange = async () => {
            let data = this._getMods();

            console.log("Writing something", data);
            let str = JSON.stringify(data);

            let node = {
                // string: str,
                name: 'todoName'//todo
            }
            if (this._pair) {
                str = await SEA.encrypt(str, this._pair);
                node.pub = this._pair.pub;
            }

            node.string = str;
            this.data.get(this.room).put(node);

        }

        /**
         * This is called whenever gun triggers a change to the data
         * and is responsible for deserializing the modules and restoring the content
         * @param data
         * @param key
         * @private
         */
        this._remoteChange = async (gunData, key, _msg, _ev) => {

            console.log("Remote Change: ", key, gunData, ' By', gunData.name, "etc...", _msg, _ev);
            // let string = gunData.string;
            let data = await this._parseData(gunData);

            this._setMods(data);
        }

    }

    /**
     * Restore module data
     * @param data
     * @private
     */
    _setMods(data) {
        if (this._mods.length != data.length) {
            console.error("Data length does not match module length", this._mods, this.data);
        }

        for (let i = 0; i < this._mods.length; i++) {
            this._mods[i].setValue(data[i], this._mods[i].el, this);
        }

    }

    /**
     * Get module Data
     * @returns {Array}
     * @private
     */
    _getMods() {
        let data = [];

        for (let i = 0; i < this._mods.length; i++) {
            let mod = this._mods[i];
            let el = mod;
            if (mod.el instanceof HTMLElement) el = mod.el;

            console.log("Getting module val", el);
            let val = mod.getValue(el, this);

            data.push(val);
        }

        return data
        // return this._mods.map(mod => mod.getValue(mod.el, this));
    }

    _hookMods() {

        let data = [];

        for (let i = 0; i < this._mods.length; i++) {
            let mod = this._mods[i];
            let el = mod.el;

            if (mod.el instanceof HTMLElement) el = mod.el;

            console.log("Getting module val", el);
            let val = mod.getValue(el, this);

            data.push(val);
        }

        // return data
    }

    /**
     * Can be used to insert chat into dom wherever you want
     * @returns {HTMLElement}
     */
    getContainer() {
        return this.container
    }

    /**
     * get tools for jspanel toolbar
     * @returns {Array}
     * @private
     */
    _getTools() {
        // helper function to se if a webcomponents is registered
        var isRegistered = function (name) {
            return document.createElement(name).constructor !== HTMLElement;
        }

        let tools = [];

        let html = `
            <div style="display: flex;">
                <button title="room" placeholder="Enter A room name" name='roomLabel' style="background: var(--focus); padding: 3px">🂠</button>
                <button title="Go back a room" name="back" style="padding: 3px">«</button>
                <input title="Enter a room name" name="room" type="text" value="${this.room}">
                <button title="Go forward a room" name="forward" style="padding: 3px">»</button>   
            `

        if (this.gunAuth) {
            let alias = 'anon';
            if (this.gunAuth.user.is) alias = this.gunAuth.user.is.alias;
            html += `<button name="login" title="Logged in as ${alias}" style="padding: 3px" >`

            if (bs.isRegistered('gun-avatar')) {
                html += `<gun-avatar title="Logged in as ${this.gunAuth.user.is.alias}"  size=29 round pub=${this.gunAuth.user.is.pub}></gun-avatar>`
            } else {
                html += '😀'
            }
            html += `</button>`
        }
        if (this.options.modes) {
            html += `<button name="mode"  style="padding: 3px" > mode(<span name="mode">${this.mode}</span>) </button>`
        }

        // if (this.gunAuth && isRegistered('gun-avatar')) {
        //     html += `<gun-avatar title="Logged in as ${this.gunAuth.user.is.alias}"  size=39 round pub=${this.gunAuth.user.is.pub}></gun-avatar>`
        //
        // }
        html += `</div>`

        tools.push(html);

        return tools
        // }
    }

    _setupTools(panel) {
        if (this._userToolbar) return;
        let root = panel
        if (!root) root = document
        let back = root.querySelector("button[name='back']")
        let roomLabel = root.querySelector("button[name='roomLabel']")

        this._roomSettings = () => {
            bs.prompt('test', {cat: 'input'});
        }
        roomLabel.addEventListener('click', this._roomSettings)

        let room = root.querySelector("input[name='room']")
        let forward = root.querySelector("button[name='forward']")
        let login = root.querySelector("button[name='login']")
        let avatar = root.querySelector("gun-avatar")
        // if (avatar) login = avatar;
        if (login) {
            login.addEventListener('click', async () => {
                await this.gunAuth.login();
                login.title = "Logged in as user: " + this.gunAuth.user.is.alias;
                console.log("Logged in as user: ", this.gunAuth.user.is.alias);
                if (avatar) {
                    avatar.pub = gunAuth.user.is.pub;
                }
            })
        }
        // if (avatar) {
        //
        // }

        let mode = root.querySelector("button[name='mode']")

        if (!this._roomHist) {
            this._roomHist = [this.room];
            this._roomPos = 0;
            this._roomChangeCount = 0;
            back.disabled = true;
            forward.disabled = true;
        }

        this._backRoom = () => {
            this._roomPos--;
            let r = this._roomHist[this._roomPos]
            this.setRoom(r);
            room.value = r;

            forward.disabled = false;
            if (this._roomPos == 0) {
                back.disabled = true;
            }
        }
        back.addEventListener('click', this._backRoom)

        this._forwardRoom = () => {
            this._roomPos++;
            let r = this._roomHist[this._roomPos]
            this.setRoom(r);
            room.value = r;
            back.disabled = false;
            if (this._roomPos == this._roomHist.length - 1) {
                forward.disabled = true;
            }
        }
        forward.addEventListener('click', this._forwardRoom)
        this._changeRoom = (e) => {
            this._roomChangeCount++;
            let old = this._roomChangeCount;

            setTimeout(() => {
                if (this._roomChangeCount === old) {
                    let val = room.value;

                    this._roomPos++;
                    this._roomHist[this._roomPos] = val;
                    back.disabled = false;
                    let ni = this._roomPos + 1
                    this._roomHist.splice(ni, this._roomHist.length - this._roomPos)//delete remaining
                    forward.disabled = true;
                    this.setRoom(val);
                }
            }, 250)

        }
        room.addEventListener('input', this._changeRoom);

        this._tb = {
            back,
            forward,
            room,
            roomLabel,
            mode,
            login
        }

    }

    initPanel() {
        let opts = this.options.jspanelOptions
        let tools
        if (typeof opts.headerToolbar != "undefined") {
            console.warn("Respecting the user defined header bullhead header will not be added");
            this._userToolbar = true;
        } else {
            // tools =
            opts.headerToolbar =  this._getTools();
        }
        this.options.jspanelOptions.content = this.getContainer();
        if (!this.panel) {
            this.panel = jsPanel.create(this.options.jspanelOptions);
        }
    }

    panelInitialized(panel) {
        this._setupTools(panel)

        this._mods.forEach(m => {
            if (typeof m.hookChange == 'function') {
                m.hookChange(this._localChange, m.el, this);
            }
        })

    }

    getQuote() {
        if (!this.qotd) this.qotd = '"Humanity has the shared goal of life. Let this be enough to overcome malice, motivate altruism, and justify love!" ~ Kier';//not just humanity ... all life
        this.updateQuote();
        return this.qotd;
    }

    async updateQuote() {
        return fetch('https://api.quotable.io/random')
            .then(res => {
                return res.json()
            })
            .then(data => {
                this.qotd = `"${data.content}" ~ ${data.author}`
                return this.qotd;
            })
    }


}


let simpleInputMod = {
    el: document.createElement('input'),//can be any wako dom element
    setValue: (val, el) => {
        el.value = val; // this MUST accept the data state you give out
    },
    getValue: (el) => {
        return el.value; // your responsible for getting the data state out of your module. no cycles
    },
    getDefault: (el, bullchat) => {
        return "QOTD: " + bullchat.getQuote(); // if there is no value what should bulchat set you value to
    },
    /**
     * Gets called on module initialization and is responsible for calling 'fn' any time your modules value is changed
     * It is ytour responsibility to implement this and call the change function when aplicalbe
     * @param fn - the call
     * @param el - your el is passed back for convenience
     * @param bullchat - same with the bullchat instance
     */
    hookChange: (fn, el, bullchat) => {//this asks you to hook bullchats function to your change event(s)
        window._saveChangeFunctionForLater = fn; // :) you can do whatever with it just call this function when the data changed
        el.addEventListener('input', fn);//subscribes the fn to my input elements(el)'s input event
    }
}
// ALL the above is just info on how to implement your own modules
// a = new ArrayInput()


/*
let a = new ArrayInput(['cat', 'dog', 'horse'], {
    type: 'text',
    // data: undefined,
    appendTo: contentDiv,
});
*/


/*
a.div.addEventListener('array-change', (e, r) => {
    console.log("Array Change", e, r);
    localChange()
})


content.appendChild(a);
*/


// let quote = document.getElementById('qotd');
//
// function updateQOTD() {
//     fetch('https://api.quotable.io/random')
//         .then(res => {
//             return res.json()
//         })
//         .then(data => {
//             quote.innerText = `"${data.content}" ~ ${data.author}`
//
//         })
// }
//
// updateQOTD()


// let bc = new BullChat(gun, {
//     id: 'myBullchat',
//     room: 'bullchat3',
//
// })

// if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
//     // <div className="jsPanel-titlebar" style="font-size: 1.05rem; touch-action: none; cursor: move;">
//     //     <div className="jsPanel-title" style="color: rgb(0, 0, 0);">Scratch Room</div>
//     // </div>
//
//     let header = document.createElement('div');
//     header.style.background = 'var(--bullchat-theme)';
//     header.style.padding = '5px';
//     header.innerHTML = `<h2>Scratch Room</h2>`
//     contentDiv.firstElementChild.insertAdjacentElement('beforeBegin', header)
// } else {// init the jspanel if we are on a computer
//
//
//     jsPanel.create(jspanelOptions)
// }


// If environment is not browser, export it (for node compatibility)
// if (typeof window === 'undefined') module.exports = Bullet
