# BullChat

Active development

A simple chat using gun and jspanel4

Also has a nice Array Input Component see js/ArrayInput.js




# Getting started

## As a Library

```shell
# ssh if you do this with privat repo
npm install git+ssh://git@gitlab.com:syonfox/bullchat.git
# otherwise you can use https
npm install git+https://gitlab.com/syonfox/bullchat.git
```

### le code
see the example for full things.. note you will probably 
have to change some src paths to `/node_modules/bullchat/whatever`

```html
<!--css-->
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
<!--<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/water.css@2/out/water.css">-->
<link rel="stylesheet" type="text/css" href="./css/water.css"><!--Or your own but his is nice and simple-->
<link rel="stylesheet" type="text/css" href="./css/animate.min.css"><!--For jspanel animations-->
<link rel="stylesheet" type="text/css" href="./css/InputArray.css">
<link rel="stylesheet" href="./node_modules/jspanel4/dist/jspanel.css">
<!--libs-->
<script src="./node_modules/jquery/dist/jquery.min.js"></script>
<script src="./node_modules/tiny-editor/dist/bundle.js"></script>
<!-- loading jsPanel javascript -->
<script src="./node_modules/jspanel4/dist/jspanel.js"></script>
<!-- optionally load jsPanel extensions -->
<script src="./node_modules/jspanel4/dist/extensions/contextmenu/jspanel.contextmenu.js"></script>
<script src="./node_modules/jspanel4/dist/extensions/hint/jspanel.hint.js"></script>
<script src="./node_modules/jspanel4/dist/extensions/modal/jspanel.modal.js"></script>
<script src="./node_modules/jspanel4/dist/extensions/tooltip/jspanel.tooltip.js"></script>
<script src="./node_modules/jspanel4/dist/extensions/dock/jspanel.dock.js"></script>


<!-- Gun -->
<script src="./node_modules/gun/gun.js"></script>
<script src="./node_modules/gun/sea.js"></script>
<!--bullchat -->
<script src="./js/GunAuth.js"></script>
<script src="./js/bs.js"></script>
<script src="./js/bsMixinGun.js"></script>
<script src="./js/Bullet.js"></script>
<script src="./js/ArrayInput.js"></script>
<script src="./js/BullChat.js"></script>
<script src="./js/BullchatMods.js"></script>

<div id="my_content" style=" background: var(--border); display: grid; padding: 8px;">
    <div id="scratch_content" style=" background: var(--border); display: grid; padding: 8px;"></div>
</div>
```
Then in a script tag or file
```js
//Initialize gun
var gun = new Gun({
    // peers: ['http://bullchat.syon.ca:8585/gun'],
    peers: ['https://bullchat.syon.ca/gun'],
});


window.mobileCheck = function() {
    let check = false;
    (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) check = true;})(navigator.userAgent||navigator.vendor||window.opera);
    return check;
};

let isMobile = mobileCheck();


var gunAuth = new GunAuth(gun);

let bullchat = new BullChat(gun, {
    id: 'scratch_container',
    // container: '.bullchat-rich',
    gunAuth: gunAuth,
    initPanel:!isMobile,
    room: 'bullchat3',
    modules: [
        {
            type: 'textarea'
        },
        tinyEditorFactory(),

        new ArrayInput(),
    ]
})


```



## Hosting It

### to start a bullchat gun server 
```
git clone git@gitlab.com:syonfox/bullchat.git
cd bullchat
npm install
npm start # or node server.js
# then in the browser > http://localhost:8585
```

### Setup Apache Proxy

```
<VirtualHost *:80>
    ServerName bullchat.syon.ca
    ServerAdmin dev@syon.ca
    ProxyRequests Off
    ProxyPreserveHost On
    
    ProxyPass /gun  ws://127.0.0.1:8585/gun
    ProxyPassReverse /gun  ws://127.0.0.1:8585/gun
    ProxyPass / http://127.0.0.1:8585/
    ProxyPassReverse / http://127.0.0.1:8585/
</VirtualHost>

```
```
a2enmod proxy_wstunnel
nano /etc/apache2/sites-available/a-site.conf
#paste in top config without rewrite rule
a2ensite a-site.conf
service apache2 restart
certbot *.mydomain.ca
```


[DEMO](bullchat.syon.ca)

Bullchat api

see /js/BullChat.js - docs in code

if buildt docs are in /docs

`jsdoc js -d docs/bullchat/`


## BullChat Modules
I recommend looking at the code for BullChatMods.js and ArrayInput.js
key thing is there are 5 properties you need to implement for your module 
1. el - the container el or your object can be an instance of HTMLElement 
2. getValue(el, bullchat) - must return the data for you module
3. setValue(val, el, bullchat) - must accept the data you give from git value
4. getDefault(el, bullchat) - must provide a default data accepted by setValue
5. hookChange(fn, el, bullchat) - called on dom add, must set up your module to call fn when the value changes

if you implement these you can then pass your object into the bullchat constuctor.


# VISION

Ok still in the exited stage about the p2p stuff realy it is the future the problem with p2p is 
bootstraps but computers are cheep just not free yet... hope that makes sense.

The main drive for me is privacy we need to build a layer on top of the internet that make encryption not only against 
attackers but against nation states secure. this is the natural progression to sifi worlds.

to do that you need trusted end to end encryption. assuming gun is secure a full stach can posibaly be made

untrusted -> riscV -> linux -> webkit -> gun -> eyeballs
..really security is dead talk in person. But most people are not that smart AND care about you. so being better then 
99% is probably good enough


Traditional Vs Decentralized

T: if you have one trusted source you can spread out from there

D: but I don't rust anyone, so I must be the source of truth!

T: But im the developer, and so I only trust me and not the users browsers etc

D: well im the user, and I don't trust you developer.

T: LOL you run my code you must trust me..., but I suppose I could prove my trust giving you the keys.
    So as long as you trust that I gave you good keys and didn't take a copy you are internally secure. even from me.

D: so you will traditionaly provide me a trusted decentralized platform?

T: yes, and we will make companies pay for it ;)

User(U): but how?

D: well you will encrypt your data with your key

T: and register the key with me for convenience

D: but he cant read any of your data

U; ok cool but I dont want to lose my data.

T: I will provide a stable peer 

D: and if he fails you can fall back on me







## gitlab stuff

## Add your files

- [ ] [Create](https://gitlab.com/-/experiment/new_project_readme_content:c7ab5101eecd757b04a270a2acb3f4ca?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://gitlab.com/-/experiment/new_project_readme_content:c7ab5101eecd757b04a270a2acb3f4ca?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://gitlab.com/-/experiment/new_project_readme_content:c7ab5101eecd757b04a270a2acb3f4ca?https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/syonfox/bullchat.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/-/experiment/new_project_readme_content:c7ab5101eecd757b04a270a2acb3f4ca?https://gitlab.com/syonfox/bullchat/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://gitlab.com/-/experiment/new_project_readme_content:c7ab5101eecd757b04a270a2acb3f4ca?https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://gitlab.com/-/experiment/new_project_readme_content:c7ab5101eecd757b04a270a2acb3f4ca?https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://gitlab.com/-/experiment/new_project_readme_content:c7ab5101eecd757b04a270a2acb3f4ca?https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://gitlab.com/-/experiment/new_project_readme_content:c7ab5101eecd757b04a270a2acb3f4ca?https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://gitlab.com/-/experiment/new_project_readme_content:c7ab5101eecd757b04a270a2acb3f4ca?https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://gitlab.com/-/experiment/new_project_readme_content:c7ab5101eecd757b04a270a2acb3f4ca?https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://gitlab.com/-/experiment/new_project_readme_content:c7ab5101eecd757b04a270a2acb3f4ca?https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://gitlab.com/-/experiment/new_project_readme_content:c7ab5101eecd757b04a270a2acb3f4ca?https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://gitlab.com/-/experiment/new_project_readme_content:c7ab5101eecd757b04a270a2acb3f4ca?https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://gitlab.com/-/experiment/new_project_readme_content:c7ab5101eecd757b04a270a2acb3f4ca?https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!).  Thank you to [makeareadme.com](https://gitlab.com/-/experiment/new_project_readme_content:c7ab5101eecd757b04a270a2acb3f4ca?https://www.makeareadme.com/) for this template.

## Suggestions for a good README
Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.

