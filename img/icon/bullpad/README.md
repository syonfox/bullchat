# Your Favicon Package

This package was generated with [RealFaviconGenerator](https://realfavicongenerator.net/) [v0.16](https://realfavicongenerator.net/change_log#v0.16)

## Install instructions

To install this package:

Extract this package in <code>&lt;web site&gt;/img/icon/bullpad/</code>. If your site is <code>http://www.example.com</code>, you should be able to access a file named <code>http://www.example.com/img/icon/bullpad/favicon.ico</code>.

Insert the following code in the `head` section of your pages:

    <link rel="apple-touch-icon" sizes="180x180" href="/img/icon/bullpad/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/img/icon/bullpad/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/img/icon/bullpad/favicon-16x16.png">
    <link rel="manifest" href="/img/icon/bullpad/site.webmanifest">
    <link rel="mask-icon" href="/img/icon/bullpad/safari-pinned-tab.svg" color="#fae420">
    <link rel="shortcut icon" href="/img/icon/bullpad/favicon.ico">
    <meta name="apple-mobile-web-app-title" content="bullpad">
    <meta name="application-name" content="bullpad">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="msapplication-config" content="/img/icon/bullpad/browserconfig.xml">
    <meta name="theme-color" content="#ffffff">

*Optional* - Check your favicon with the [favicon checker](https://realfavicongenerator.net/favicon_checker)